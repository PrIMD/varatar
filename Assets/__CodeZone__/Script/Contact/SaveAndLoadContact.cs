﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
public class SaveAndLoadContact : MonoBehaviour {

	public  string contactXmlPath;

	public class ContactsSaver
	{
		public Contact [] contactTab;
		public List<Contact> contactList=new List<Contact>();
	}

	public void Awake(){
		Initialize ();
	}

	public Contact [] Load(){

			System.Xml.Serialization.XmlSerializer reader = 
			new System.Xml.Serialization.XmlSerializer(typeof(ContactsSaver));
			System.IO.StreamReader file = new System.IO.StreamReader(contactXmlPath);
			ContactsSaver overview = new ContactsSaver();
			overview = (ContactsSaver) reader.Deserialize(file);
			Debug.Log (overview);

			return overview.contactTab;
	}
	public void Save(Contact [] contact)
	{
		ContactsSaver cs = new ContactsSaver ();
		cs.contactTab = contact;
		foreach (Contact c in contact)
						cs.contactList.Add (c);
		System.Xml.Serialization.XmlSerializer writer = 
			new System.Xml.Serialization.XmlSerializer(typeof(ContactsSaver));

		FileStream file = File.Create (contactXmlPath);
		writer.Serialize(file, cs);
		file.Close ();
	}


	public void Initialize()
	{
		DirectoryInfo directoryInformation = new DirectoryInfo("Contact");
		if(!directoryInformation.Exists)
			directoryInformation.Create();
		
		FileInfo[] fileList = directoryInformation.GetFiles();
		
		foreach (FileInfo file in fileList)
		{
			if (file.Extension == ".xml" && string.Equals( file.Name, "Contact",System.StringComparison.CurrentCultureIgnoreCase))
			{
				Debug.Log("Contact found");
				contactXmlPath = Path.GetFullPath("Contact/Contact.xml");	
			}
		}
		if (string.IsNullOrEmpty(contactXmlPath)) {
			CreateSaveContactFile();
		}
	}


	private void CreateSaveContactFile()
	{
		Debug.Log ("Contact creating");
		string basicXmlStuff = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		StreamWriter file = new StreamWriter ("Contact/Contact.xml");
		file.Write (basicXmlStuff);
		file.Close ();
		contactXmlPath = Path.GetFullPath("Contact/Contact.xml");	
	}
}
