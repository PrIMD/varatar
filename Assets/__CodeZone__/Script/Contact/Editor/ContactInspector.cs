﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ContactMono))]
public class ContactInspector : Editor {

	public override  void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		ContactMono cm = (ContactMono)target;
		EditorGUILayout.LabelField ("Contact information");
		
		EditorGUILayout.LabelField ("Id: "+cm.contact.Id);
		EditorGUILayout.LabelField ("First name: "+cm.contact.FirstName);
		EditorGUILayout.LabelField ("Last name: "+cm.contact.LastName);
		EditorGUILayout.LabelField ("Sex: "+(cm.contact.IsFemale?"Female":"Male"));
		EditorGUILayout.LabelField ("Birth day: "+cm.contact.BirthDay);
		EditorGUILayout.LabelField ("Phone Numbers: ");
		foreach (string s in cm.contact.PhoneNumber) 
			EditorGUILayout.LabelField ("> "+s);

		EditorGUILayout.LabelField ("Mails: ");
		foreach (string s in cm.contact.Mails) 
			EditorGUILayout.LabelField ("> "+s);
		EditorGUILayout.LabelField ("General Description: "+cm.contact.WhoDescription);
		EditorGUILayout.LabelField ("Importance: "+cm.contact.ImportancyDescription);
		EditorGUILayout.LabelField ("Utility: "+cm.contact.UtilityDescription);
		EditorGUILayout.LabelField ("Keywords: ");
		foreach (string s in cm.contact.RelatedKeywords) 
			EditorGUILayout.LabelField ("> "+s);
		EditorGUILayout.LabelField ("Related to: ");
		foreach (int s in cm.contact.RelatedContact) 
			EditorGUILayout.LabelField ("> "+s);
		EditorGUILayout.LabelField ("Image Path: "+cm.contact.ImageRelativePath);


//		public string birthDay;
//		public string [] phoneNumber;
//		public string [] mailNumber;
//		
//		public string imageRelativePath;
//		
//		public List<string> relatedKeywords;
//		public string whoDescription;
//		public string importancyDescription;
//		public string utilityDescription;
//
	}

}
