﻿using UnityEngine;
using System.Collections;

public class ContactMono : MonoBehaviour {

	public readonly Contact contact;
	public readonly static long defaultId=0;
	public readonly static string defaultFirstName="Undefined";
	public readonly static string defaultLastName="Undefined";

	public ContactMono(){
		contact = new Contact (defaultId,defaultFirstName,defaultLastName);

	}

	public ContactMono (Contact contact)
	{
		if (contact == null)
			contact = new Contact (defaultId,defaultFirstName,defaultLastName);


	}
}
