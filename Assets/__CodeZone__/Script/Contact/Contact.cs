﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class Contact  
{	
	private long id;
	private string firstName;
	private string lastName;
	private bool isFemale;
	private string birthDay;
	private string [] phoneNumber;
	private string [] mails;
	
	private string imageRelativePath;
	
	private List<string> relatedKeywords;
	private List<int> relatedContact;
	private string whoDescription;
	private string importancyDescription;
	private string utilityDescription;
	
	public Contact(){
		Init ();
	}
	public Contact( long id ,string firstName, string lastName){

		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		Init (firstName, lastName);

	}
	
	public void Init(
					 string firstName="Undefine",
	                 string lastName="Undefine",
	                 bool isFemale=false,
	                 string birthDay="01/01/1970",
	                 string imageRelativePath= "defaultContact.png",
	                 string whoDescription ="No description",
	                 string importancyDescription="Basic contact",
	                 string utilityDescription="Basic contact"
	                 )
	{
		this.firstName= firstName;
		this.lastName= lastName;
		this.isFemale =isFemale;
		this.birthDay = birthDay;
		this.imageRelativePath = imageRelativePath;
		this.whoDescription = whoDescription;
		this.importancyDescription= importancyDescription;
		this.utilityDescription = utilityDescription;

	}
	
	 
	public string FirstName {
		get {
			return this.firstName;
		}
		set {
			firstName = value;
		}
	}

	public string LastName {
		get {
			return this.lastName;
		}
		set {
			lastName = value;
		}
	}

	public bool IsFemale {
		get {
			return this.isFemale;
		}
		set {
			isFemale = value;
		}
	}

	public string BirthDay {
		get {
			return this.birthDay;
		}
		set {
			birthDay = value;
		}
	}

	public string[] PhoneNumber {
		get {
			
			if(this.phoneNumber==null) this.phoneNumber=new string[]{};
			return this.phoneNumber;
		}
		set {
			phoneNumber = value;
		}
	}

	public string[] Mails {
		get {
			if(this.mails==null) this.mails=new string[]{};
			return this.mails;
		}
		set {
			mails = value;
		}
	}

	public string ImageRelativePath {
		get {
			return this.imageRelativePath;
		}
		set {
			imageRelativePath = value;
		}
	}

	public List<string> RelatedKeywords {
		get {
			
			if(this.relatedKeywords==null) this.relatedKeywords=new List<string>();
			return this.relatedKeywords;
		}
		set {
			relatedKeywords = value;
		}
	}

	public string WhoDescription {
		get {
			return this.whoDescription;
		}
		set {
			whoDescription = value;
		}
	}

	public string ImportancyDescription {
		get {
			return this.importancyDescription;
		}
		set {
			importancyDescription = value;
		}
	}

	public string UtilityDescription {
		get {
			return this.utilityDescription;
		}
		set {
			utilityDescription = value;
		}
	}	
	public long Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public List<int> RelatedContact {
		get {
			if(relatedContact==null)relatedContact= new List<int>();
			return this.relatedContact;
		}
		set {
			relatedContact = value;
		}
	}
}
