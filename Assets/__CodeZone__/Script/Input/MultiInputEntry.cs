﻿using UnityEngine;
using System.Collections;

public abstract class MultiInputEntry : MonoBehaviour {

	public enum SeveralInputBehaviour{ All , One}
	public SeveralInputBehaviour whenSeveral=SeveralInputBehaviour.One;
	public virtual bool IsActive(){
		bool [] values = GetInputsActiveState ();
		if(values==null|| values.Length==0) return false;
		return IsActive (values, whenSeveral);
	}
	public virtual float IsActiveInPourcent()
	{
		
		float [] values = GetInputsPowerState ();
		if(values==null|| values.Length==0) return 0f;
		return IsActiveInPourcent (values, whenSeveral);
	}
	
	public abstract bool [] GetInputsActiveState ();
	public abstract float [] GetInputsPowerState ();

	public static bool IsActive(bool [] isActiveInputValues, SeveralInputBehaviour whenSeveral)
	{	
		if (isActiveInputValues == null)
		return false;
		if (isActiveInputValues.Length == 1)
			return isActiveInputValues [0];
		
		if (whenSeveral == SeveralInputBehaviour.One) {
			foreach(bool isActive in isActiveInputValues)
			{
				if(isActive)return true;
			}
			
			return false;
		}
		else if (whenSeveral == SeveralInputBehaviour.All) {
			foreach(bool isActive in isActiveInputValues)
			{
				if(!isActive)return false;
			}
			return true;
		}
		return false;
		
	}
	
	public static float IsActiveInPourcent(float [] isActiveInputValues, SeveralInputBehaviour whenSeveral)
	{
		if (isActiveInputValues == null)
			return 0f;
		
		float averagePower =0f;
		bool foundOne=false;
		foreach(float isActivePower in isActiveInputValues)
		{
			float power = isActivePower;
			if( power<=0 && whenSeveral == SeveralInputBehaviour.All)return 0f;
			else if( power>0){ 
				if(!foundOne) {
					foundOne=true;
					averagePower=power;
				}
				averagePower= (averagePower+power)/2f;
			}
		}
		return averagePower;
	}
}
