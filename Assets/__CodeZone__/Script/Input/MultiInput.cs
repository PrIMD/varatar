﻿using UnityEngine;

using System.Collections.Generic;
using System;
public class MultiInput : MonoBehaviour, LayoutAffected {

	
	public enum SeveralInputBehaviour{ All , One}
	public SeveralInputBehaviour whenSeveral=SeveralInputBehaviour.One;

	public bool autoAddInputWhenEmpty = true;
	public List<MultiInputEntry> entries = new List<MultiInputEntry>();

	public float minPower = 0.1f;
	public float lastInputValue ;

	public string [] linkedLayout;
	public bool needAllToBeActive;
	public bool layoutValide;

	public void Start()
	{
		if (autoAddInputWhenEmpty) {
				
			MultiInputEntry [] entries = GetComponents<MultiInputEntry> () as MultiInputEntry [];
			foreach (MultiInputEntry mi in entries) {
				this.entries.Add(mi);		
			}
		}
		Layout.OnActivationDetected += OnLayoutActivate;
		Layout.OnDeactivationDetected += OnLayoutDeactivate;
		CheckLayoutValidity ();

	}

	public virtual bool IsActive ()
	{
		if (!layoutValide)
			return false;
		return IsInputsActive ();
	}
	public virtual float IsActiveInPourcent ()
	{
		if (!layoutValide)
						return 0.0f;
		return IsInputsActiveInPourcent ();
	}
	protected float IsInputsActiveInPourcent()
	{
		
		if (entries == null)
			return 0f;

			float averagePower =0f;
			bool foundOne=false;
			foreach(MultiInputEntry entry in entries)
			{
				if(!entry.enabled) continue;
				float power =entry.IsActiveInPourcent();
				if( power<minPower && whenSeveral == SeveralInputBehaviour.All){
				lastInputValue=0f;
				return 0f;
				}
				else if( power>=minPower){ 
					if(!foundOne) {
						foundOne=true;
						averagePower=power;
					}
					averagePower= (averagePower+power)/2f;
				}
			}
			lastInputValue = averagePower;
			return averagePower;
	
	}

	protected bool IsInputsActive()
	{
		
		if (entries == null)
			return false;
		if (entries.Count == 1)
			return entries [0].IsActive ();
		
		if (whenSeveral == SeveralInputBehaviour.One) {
			foreach(MultiInputEntry entry in entries)
			{
				if(!entry.enabled) continue;
				if(entry.IsActive())return true;
			}
			
			return false;
		}
		else if (whenSeveral == SeveralInputBehaviour.All) {
			foreach(MultiInputEntry entry in entries)
			{
				if(!entry.enabled) continue;
				if(! entry.IsActive())return false;
			}
			return true;
		}
		
		return false;
	}




	
	
	protected void OnLayoutDeactivate(object obj, LayoutDeactivateEventArgs args){
		
		CheckLayoutValidity ();
		
	}
	
	protected void OnLayoutActivate(object obj, LayoutActivateEventArgs args){
		CheckLayoutValidity ();
	}
	
	protected void CheckLayoutValidity(){
		if (linkedLayout == null || linkedLayout.Length <= 0) {
			layoutValide=true;
			return;
		}
		bool isAllActive=true;
		bool isOneActive=false;
		foreach (string s in linkedLayout) {
			bool active = Layout.IsActivate(s);
			if(!isOneActive && active) isOneActive=true;
			if(isAllActive && !active) isAllActive=false;
		}
		
		if (needAllToBeActive && isAllActive)
			layoutValide = true;
		else if (!needAllToBeActive && isOneActive)
			layoutValide = true;
		else
			layoutValide = false;
	}
	
	public bool AreLayoutValide ()
	{
		return layoutValide;
	}
	public string[] GetUsedLayoutNames ()
	{
		return linkedLayout;
	}


}
