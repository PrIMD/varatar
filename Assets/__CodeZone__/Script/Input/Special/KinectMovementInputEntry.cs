﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;
public class KinectMovementInputEntry : MultiInputEntry {

	public Movement [] entries;
	public float durationStayActiveWhenEnd=0.1f;
	public float power =0f;
	public bool withSmoothDecrease;

	public  void Start(){
		foreach (Movement m in entries) {
			m.OnEndMoving+=End;		
		}
	}

	public void End (object obj, MouvementEndArgs args){
		power = durationStayActiveWhenEnd;
	}
	public void Update(){
		if (power > 0f)
				power -= Time.deltaTime;
		if (power < 0f)
						power = 0f;

	}
	public override bool[] GetInputsActiveState ()
	{
		bool [] values = new bool[1] ;
		values [0] = power > 0;
		return values;
		
	}
	
	public override float[] GetInputsPowerState ()
	{
		float [] values = new float[1] ;
		if(withSmoothDecrease && durationStayActiveWhenEnd!=0f) values [0] =Mathf.Clamp( power/durationStayActiveWhenEnd,0f,1f) ;
		else values [0] = power > 0f ?1f:0f;
		return values;
	}
	

	

}
