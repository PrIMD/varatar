﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class KinectPositionInputEntry : MultiInputEntry {


	public BodyPosition [] entries;

	public override bool[] GetInputsActiveState ()
	{
		bool [] values = new bool[entries.Length] ;
		int i = 0;
		foreach (BodyPosition entry in entries) {
			if(!entry.enabled) values[i]=false;
			else values[i]= IsActive(entry);
			i++;
		}
		return values;
		
	}
	
	public override float[] GetInputsPowerState ()
	{
		float [] values = new float[entries.Length] ;
		int i = 0;
		foreach (BodyPosition entry in entries) {
			if(!entry.enabled) values[i]=0f;
			else values[i]= IsActive(entry)?1f:0f;
			i++;
		}
		return values;
	}

	
	private  bool IsActive (BodyPosition entryType)
	{
		return entryType.IsActive ();
	}

}
}