﻿using UnityEngine;
using System.Collections;

public class X360InputEntry : MultiInputEntry {

	public enum X360EntryType {A,B,X,Y, BR, BL, TR,TL,Menu,Start, PressLeftJoystick, PressRightJoystick,
		Arrow_Nord , Arrow_East,Arrow_Sud,Arrow_West,
		LeftJoystick_Nord , LeftJoystick_East , LeftJoystick_Sud , LeftJoystick_West ,
		RightJoystick_Nord , RightJoystick_East , RightJoystick_Sud , RightJoystick_West
	}

	public X360EntryType [] entries;
	public float joystickSensibility=0.3f;

	
	public override bool[] GetInputsActiveState ()
	{
		bool [] values = new bool[entries.Length] ;
		int i = 0;
		foreach (X360EntryType entry in entries) {

			values[i]= IsActive(entry);
			i++;
		}
		return values;
		
	}
	
	public override float[] GetInputsPowerState ()
	{
		float [] values = new float[entries.Length] ;
		int i = 0;
		foreach (X360EntryType entry in entries) {
			values[i]= IsActiveInPourcent(entry);
			i++;
		}
		return values;
	}


	
	private  bool IsActive (X360EntryType entryType)
	{
		switch(entryType)
		{
		case X360EntryType.A: return X360.IsA();  
		case X360EntryType.B: return X360.IsB();  
		case X360EntryType.X: return X360.IsX();  
		case X360EntryType.Y: return X360.IsY();  
			
		case X360EntryType. BR: return X360.IsRb();  
		case X360EntryType. BL: return X360.IsLb();  
		case X360EntryType. TR: return X360.IsRt();  
		case X360EntryType. TL: return X360.IsLt();  
			
		case X360EntryType.Menu: return X360.IsMenu();  
		case X360EntryType.Start: return X360.IsStart();  
			
		case X360EntryType.PressLeftJoystick: return X360.IsLeftPull();  
		case X360EntryType.PressRightJoystick: return X360.IsRightPull();  
			
		case X360EntryType.Arrow_Nord: return IsNord(X360.GetArrows());  
		case X360EntryType.Arrow_East: return IsEast(X360.GetArrows());  
		case X360EntryType.Arrow_Sud: return IsSud(X360.GetArrows());  
		case X360EntryType.Arrow_West: return IsWest(X360.GetArrows());  
			
		case X360EntryType.LeftJoystick_Nord: return IsNord(X360.GetLeft());  
		case X360EntryType.LeftJoystick_East: return IsEast(X360.GetLeft());  
		case X360EntryType.LeftJoystick_Sud:  return IsSud(X360.GetLeft());  
		case X360EntryType.LeftJoystick_West: return IsWest(X360.GetLeft());  
			
		case X360EntryType.RightJoystick_Nord: return IsNord(X360.GetRight());  
		case X360EntryType.RightJoystick_East: return IsEast(X360.GetRight());  
		case X360EntryType.RightJoystick_Sud:  return IsSud(X360.GetRight());  
		case X360EntryType.RightJoystick_West: return IsWest(X360.GetRight()); 
		}	
		return false;	
	}

	private  float IsActiveInPourcent (X360EntryType entryType)
	{
		switch(entryType)
		{
		case X360EntryType.A: return X360.IsA()?1f:0f;  
		case X360EntryType.B: return X360.IsB()?1f:0f;  
		case X360EntryType.X: return X360.IsX()?1f:0f;  
		case X360EntryType.Y: return X360.IsY()?1f:0f;  
			
		case X360EntryType. BR: return X360.IsRb()?1f:0f;  
		case X360EntryType. BL: return X360.IsLb()?1f:0f;  
		case X360EntryType. TR: return X360.IsRt()?1f:0f;  
		case X360EntryType. TL: return X360.IsLt()?1f:0f;  
			
		case X360EntryType.Menu: return X360.IsMenu()?1f:0f;  
		case X360EntryType.Start: return X360.IsStart()?1f:0f;  
			
		case X360EntryType.PressLeftJoystick: return X360.IsLeftPull()?1f:0f;  
		case X360EntryType.PressRightJoystick: return X360.IsRightPull()?1f:0f;  

			
		case X360EntryType.Arrow_Nord: return IsNordInPourcent(X360.GetArrows());  
		case X360EntryType.Arrow_East: return IsEastInPourcent(X360.GetArrows());  
		case X360EntryType.Arrow_Sud: return IsSudInPourcent(X360.GetArrows());  
		case X360EntryType.Arrow_West: return IsWestInPourcent(X360.GetArrows());  
			
		case X360EntryType.LeftJoystick_Nord: return IsNordInPourcent(X360.GetLeft());  
		case X360EntryType.LeftJoystick_East: return IsEastInPourcent(X360.GetLeft());  
		case X360EntryType.LeftJoystick_Sud:  return IsSudInPourcent(X360.GetLeft());  
		case X360EntryType.LeftJoystick_West: return IsWestInPourcent(X360.GetLeft());  
			
		case X360EntryType.RightJoystick_Nord: return IsNordInPourcent(X360.GetRight());  
		case X360EntryType.RightJoystick_East: return IsEastInPourcent(X360.GetRight());  
		case X360EntryType.RightJoystick_Sud:  return IsSudInPourcent(X360.GetRight());  
		case X360EntryType.RightJoystick_West: return IsWestInPourcent(X360.GetRight()); 
		}	
		return 0f;	
	}
	
	private bool IsNord(Direction dir)
	{	if (dir == null)
		return false;
		return dir.GetY () > joystickSensibility;
	}
	
	private bool IsSud(Direction dir)
	{	if (dir == null)
		return false;
		return dir.GetY () < -joystickSensibility;
	}
	
	private bool IsWest(Direction dir)
	{	if (dir == null)
		return false;
		return dir.GetX () <- joystickSensibility;
	}
	
	private bool IsEast(Direction dir)
	{	if (dir == null)
		return false;
		return dir.GetX () >joystickSensibility;
	}
	
	private float IsNordInPourcent(Direction dir)
	{	if (dir == null)
		return 0f;
		float val = dir.GetY (); 
		return  val> joystickSensibility ? Mathf.Abs(val):0f;
	}
	
	private float IsSudInPourcent(Direction dir)
	{	if (dir == null)
		return 0f;
		
		float val = dir.GetY (); 
		return  val < - joystickSensibility ? Mathf.Abs (val) : 0f;
	}
	
	private float IsWestInPourcent(Direction dir)
	{	if (dir == null)
		return 0f;
		
		float val = dir.GetX (); 
		return  val < - joystickSensibility ? Mathf.Abs (val) : 0f;
	}
	
	private float IsEastInPourcent(Direction dir)
	{	if (dir == null)
		return 0f;
		float val = dir.GetX (); 
		return  val> joystickSensibility ? Mathf.Abs(val):0f;
	}


}
