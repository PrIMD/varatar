using UnityEngine;
using System.Collections;
using System;
public abstract class ScreenZoneInputEntry : MultiInputEntry {


	public ScreenZoneEntryType [] entries;

	public float pressActiveDuration=0.3f;
	public float whenLastActivePressure;
	public float releaseActiveDuration=0.3f;
	public float whenLastActiveRelease;



	public override bool[] GetInputsActiveState ()
	{
		
		if (IsAlreadyActive ())
			return new bool[]{true};

		bool [] values = new bool[entries.Length] ;
		int i = 0;
		ScreenZoneEntryType.PressureState pressure = GetPressureState();
		float x = GetX ();
		float y = GetY ();
		foreach (ScreenZoneEntryType entry in entries) {
			values[i]= IsActive(entry,ref pressure,ref x,ref y);
			i++;
		}
		return values;
		
	}
	
	public override float[] GetInputsPowerState ()
	{

		if (IsAlreadyActive ())
			return new float[]{1f};

		float [] values = new float[entries.Length] ;
		int i = 0;
		ScreenZoneEntryType.PressureState pressure= GetPressureState();
		float x = GetX ();
		float y = GetY ();
		foreach (ScreenZoneEntryType entry in entries) {
			values[i]= IsActive(entry,ref pressure,ref x,ref y)?1f:0f;
			i++;
		}
		return values;
	}
	

	public bool IsAlreadyActive(){
		float time = GetTime ();
		if (time - whenLastActivePressure < pressActiveDuration)
			return true;
		else if (time - whenLastActiveRelease < releaseActiveDuration)
			return true;
		return false;
	}


	private  bool IsActive (ScreenZoneEntryType entry,ref ScreenZoneEntryType.PressureState pressure, ref float x, ref float y)
	{
		if (entry == null)
			return false;
		bool isActive = entry.IsActive (pressure,x,y);
		if (isActive && pressure == ScreenZoneEntryType.PressureState.Pressed) {
			whenLastActivePressure = GetTime();	
		}
		if (isActive && pressure == ScreenZoneEntryType.PressureState.Release) {
			whenLastActiveRelease = GetTime();	
		}
		return isActive;
		
	}
	public float GetTime(){return Time.realtimeSinceStartup;}
	public bool IsOverZone(float x, float y){

		foreach (ScreenZoneEntryType zoneEntry in entries) {

			if(zoneEntry!=null && zoneEntry.screenZone !=null && zoneEntry.screenZone.IsInZone(x,y)){return true;}
		}
		return false;
	}

	public abstract ScreenZoneEntryType.PressureState GetPressureState();
	public abstract float GetX();
	public abstract float GetY();

	[Serializable]
	public class ScreenZoneEntryType
	{
		public ScreenZone screenZone;
		public enum PressureState{Over,Pressed,Pressing,Release};
		public PressureState [] activeOnPressure;


		public bool IsActive(PressureState state, float x, float y){
			return IsSensitifTo(state) && IsOverZone(x,y);		
		}
		
		public bool IsOverZone(float x, float y){
			
			if (screenZone != null)
				return  screenZone.IsInZone (x, y);
			return false;
		}
		public bool IsSensitifTo(PressureState state)
		{
			foreach(PressureState activeOn in activeOnPressure)
			{
				if(state==activeOn) return true; 
			}
			return false;
		}

//		public enum PresenceState{Out, Enter,In, Exit};
//		public PresenceState [] activeOnPresence;
//		//SHOULD BE PRIVATE
//		public PresenceState lastPresenceState = PresenceState.Out;
//		public PresenceState SetPresenceState(float x, float y){
//			bool isOver = IsOverZone(x, y);
//			PresenceState last = lastPresenceState;	
//			
//			if (last == PresenceState.Out && isOver) {
//				lastPresenceState= PresenceState.Enter;			
//			}
//			else
//			if (last == PresenceState.Enter && isOver) {
//				lastPresenceState= PresenceState.In;			
//			} 
//			if (last == PresenceState.Enter && ! isOver) {
//				lastPresenceState= PresenceState.Out;			
//			} else 
//			if (last == PresenceState.In && ! isOver) {
//				lastPresenceState= PresenceState.Exit;			
//			}else 
//			if (last == PresenceState.Exit && isOver) {
//				lastPresenceState= PresenceState.In;			
//			}  else
//			if (last == PresenceState.Exit && ! isOver) {
//				lastPresenceState= PresenceState.Out;			
//			} 
//			return last;
//		}

	}
}
