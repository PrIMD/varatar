﻿using UnityEngine;
using System.Collections;

public class MouseScreenZoneInputEntry : ScreenZoneInputEntry {




	#region implemented abstract members of ScreenZoneInputEntry

	public override ScreenZoneEntryType.PressureState GetPressureState ()
	{
		if (Input.GetMouseButtonDown (0)||Input.GetMouseButtonDown(1))
			return ScreenZoneEntryType.PressureState.Pressed;
		if (Input.GetMouseButtonUp(0)||Input.GetMouseButtonUp(1))
			return ScreenZoneEntryType.PressureState.Release;
		if (Input.GetMouseButton(0)||Input.GetMouseButton(1))
			return ScreenZoneEntryType.PressureState.Pressing;
		return ScreenZoneEntryType.PressureState.Over;
	}

	public override float GetX ()
	{
		return Input.mousePosition.x;
	}

	public override float GetY ()
	{
		return Screen.height-Input.mousePosition.y;
	}

	#endregion



}
