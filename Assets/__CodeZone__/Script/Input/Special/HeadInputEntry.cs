﻿using UnityEngine;
using System.Collections;

public class HeadInputEntry : MultiInputEntry {

	public enum HeadEntryType {
		Yes,No,
		RotatingUp,RotatingRight,RotatingDown,RotatingLeft,TurningLeft,TurningRight,
		IsRotationUp,IsRotationRight,IsRotationDown,IsRotationLeft,IsTurnAtLeft,IsTurnAtRight
	
		//TODO Oculus Rift V2
		//Up,Right,Down,Left,Front,Back
		//IsMovingUp,IsMovingRight,IsMovingDown,IsMovingLeft,IsMovingFront,IsMovingBack
	}
	//public Head head;
	public HeadEntryType [] entries;
	
	public override bool[] GetInputsActiveState ()
	{
		throw new System.NotImplementedException ();
	}

	public override float[] GetInputsPowerState ()
	{
		throw new System.NotImplementedException ();
	}

	private  bool IsActive (HeadEntryType entryType)
	{
		switch(entryType)
		{
		case HeadEntryType.Yes:  break;
		case HeadEntryType.No:  break;
			//TODO ALL OculusEntryType
		
		}	
		return false;	
	}


}
