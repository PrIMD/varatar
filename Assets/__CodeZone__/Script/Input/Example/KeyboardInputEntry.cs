﻿using UnityEngine;
using System.Collections;
using System;
public class KeyboardInputEntry : MultiInputEntry {
	

	public KeyboardEntryType [] entries;

	public override bool[] GetInputsActiveState ()
	{
		bool [] values = new bool[entries.Length] ;
		int i = 0;
		foreach (KeyboardEntryType entry in entries) {
			values[i]= IsActive(entry);
			i++;
		}
		return values;

	}

	public override float[] GetInputsPowerState ()
	{
		float [] values = new float[entries.Length] ;
		int i = 0;
		foreach (KeyboardEntryType entry in entries) {
			values[i]= IsActive(entry)?1f:0f;
			i++;
		}
		return values;
	}

	private  bool IsActive (KeyboardEntryType entryType)
	{
		if (entryType == null)
						return false;
		bool isActive = false;
		switch (entryType.pession) {
		case KeyboardEntryType.KeyPressType.Press:
			isActive= Input.GetKeyDown(entryType.key);
			break;
		case KeyboardEntryType.KeyPressType.Pressing: 
			isActive= Input.GetKey(entryType.key);
			break;
		case KeyboardEntryType.KeyPressType.Release: 
			isActive= Input.GetKeyUp(entryType.key);
			break;
		}

		if (!isActive)
						return false;

		if (entryType.withCtrl && !(Input.GetKey(KeyCode.LeftControl)||Input.GetKey(KeyCode.RightControl))) { 
			return false;}
		if (entryType.withShift && !(Input.GetKey(KeyCode.LeftShift)||Input.GetKey(KeyCode.RightShift))) { 
			return false;}
		if (entryType.withAlt && !(Input.GetKey(KeyCode.LeftAlt)||Input.GetKey(KeyCode.RightAlt))) { 
			return false;}

		return isActive;

	}
	[Serializable]
	public class KeyboardEntryType
	{
		public KeyCode key;
		public enum KeyPressType{Press,Pressing,Release};
		public KeyPressType pession = KeyPressType.Pressing;
		public bool withCtrl;
		public bool withShift;
		public bool withAlt;

	}
}
