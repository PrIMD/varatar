﻿using UnityEngine;
using System.Collections;

public class MouseInputEntry : MultiInputEntry {


	
	public enum MouseEntryType {
		MovingLeft,MovingRight,MovingTop,MovingDown,
		Left_Click,Right_Click,Middle_Click,Button4_Click ,Button5_Click,
		Left_Pressing,Right_Pressing,Middle_Pressing,Button4_Pressing,Button5_Pressing,
		Left_Release,Right_Release,Middle_Release,Button4_Release,Button5_Release
	}
	public MouseEntryType [] entries;

	public override bool[] GetInputsActiveState ()
	{
		bool [] values = new bool[entries.Length] ;
		int i = 0;
		foreach (MouseEntryType entry in entries) {
			values[i]= IsActive(entry);
			i++;
		}
		return values;
		
	}
	
	public override float[] GetInputsPowerState ()
	{
		float [] values = new float[entries.Length] ;
		int i = 0;
		foreach (MouseEntryType entry in entries) {
			values[i]= IsActiveInPourcent(entry);
			i++;
		}
		return values;
	}
	
	private  bool IsActive (MouseEntryType entryType)
	{
		switch(entryType)
		{
		case MouseEntryType.MovingLeft:
			return Input.GetAxis("Mouse X")<0;
		case MouseEntryType.MovingRight:
			return Input.GetAxis("Mouse X")>0;
		case MouseEntryType.MovingDown:
			return Input.GetAxis("Mouse Y")<0;
		case MouseEntryType.MovingTop:
			return Input.GetAxis("Mouse Y")>0;
			
		case MouseEntryType.Left_Click:
			return Input.GetMouseButtonDown(0);
		case MouseEntryType.Right_Click:
			return Input.GetMouseButtonDown(1);
		case MouseEntryType.Middle_Click:
			return Input.GetMouseButtonDown(2);
			
		case MouseEntryType.Left_Pressing:
			return Input.GetMouseButton(0);
		case MouseEntryType.Right_Pressing:
			return Input.GetMouseButton(1);
		case MouseEntryType.Middle_Pressing:
			return Input.GetMouseButton(2);
			
		case MouseEntryType.Left_Release:
			return Input.GetMouseButtonUp(0);
		case MouseEntryType.Right_Release:
			return Input.GetMouseButtonUp(1);
		case MouseEntryType.Middle_Release:
			return Input.GetMouseButtonUp(2);
			
		case MouseEntryType.Button4_Click:
			return Input.GetMouseButtonDown(4);
		case MouseEntryType.Button4_Pressing:
			return Input.GetMouseButton(4);
		case MouseEntryType.Button4_Release:
			return Input.GetMouseButtonUp(4);
			
		case MouseEntryType.Button5_Click:
			return Input.GetMouseButtonDown(5);
		case MouseEntryType.Button5_Pressing:
			return Input.GetMouseButton(5);
		case MouseEntryType.Button5_Release:
			return Input.GetMouseButtonUp(5);
		}	
		
		return false;	
	}
	private  float IsActiveInPourcent (MouseEntryType entryType)
	{
		switch(entryType)
		{
		case MouseEntryType.MovingLeft:
			return Input.GetAxis("Mouse X")<0?Mathf.Abs(Input.GetAxis("Mouse X")):0f;
		case MouseEntryType.MovingRight:
			return Input.GetAxis("Mouse X")>0?Mathf.Abs(Input.GetAxis("Mouse X")):0f;
		case MouseEntryType.MovingDown:
			return Input.GetAxis("Mouse Y")<0?Mathf.Abs(Input.GetAxis("Mouse Y")):0f;
		case MouseEntryType.MovingTop:
			return Input.GetAxis("Mouse Y")>0?Mathf.Abs(Input.GetAxis("Mouse Y")):0f;
			
		case MouseEntryType.Left_Click:
			return Input.GetMouseButtonDown(0)?1f:0f;
		case MouseEntryType.Right_Click:
			return Input.GetMouseButtonDown(1)?1f:0f;
		case MouseEntryType.Middle_Click:
			return Input.GetMouseButtonDown(2)?1f:0f;
			
		case MouseEntryType.Left_Pressing:
			return Input.GetMouseButton(0)?1f:0f;
		case MouseEntryType.Right_Pressing:
			return Input.GetMouseButton(1)?1f:0f;
		case MouseEntryType.Middle_Pressing:
			return Input.GetMouseButton(2)?1f:0f;
			
		case MouseEntryType.Left_Release:
			return Input.GetMouseButtonUp(0)?1f:0f;
		case MouseEntryType.Right_Release:
			return Input.GetMouseButtonUp(1)?1f:0f;
		case MouseEntryType.Middle_Release:
			return Input.GetMouseButtonUp(2)?1f:0f;
			
		case MouseEntryType.Button4_Click:
			return Input.GetMouseButtonDown(4)?1f:0f;
		case MouseEntryType.Button4_Pressing:
			return Input.GetMouseButton(4)?1f:0f;
		case MouseEntryType.Button4_Release:
			return Input.GetMouseButtonUp(4)?1f:0f;
			
		case MouseEntryType.Button5_Click:
			return Input.GetMouseButtonDown(5)?1f:0f;
		case MouseEntryType.Button5_Pressing:
			return Input.GetMouseButton(5)?1f:0f;
		case MouseEntryType.Button5_Release:
			return Input.GetMouseButtonUp(5)?1f:0f;
		}	
		
		return 0f;	
	}
}
