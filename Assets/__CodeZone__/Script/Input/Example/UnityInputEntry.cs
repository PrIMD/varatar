﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class UnityInputEntry : MultiInputEntry {


	public enum UnityEntryType
	{ 
		JoystickGoingLeft, JoystickGoingRight, JoystickGoingUp,JoystickGoingDown,
		JoystickLeft, JoystickRight, JoystickUp,JoystickDown, 

		MouseGoingLeft, MouseGoingRight, MouseGoingUp,MouseGoingDown,

		MainFire,SecondFire,ThirdFire, Jump, ScrollingDown, ScrollingUp
	}
	
	public float joystickSensibility=0.02f;
	public float joystickBorder= 0.1f;
	public float mouseSensibily=0.01f;


	
	public UnityEntryType [] entries;
	public UnityCustumEntryType [] custumEntries;
	public override bool[] GetInputsActiveState ()
	{
		if (entries == null || custumEntries == null)
						return new bool[0];
		bool [] values = new bool[entries.Length+custumEntries.Length] ;
		int i = 0;
		foreach (UnityEntryType entry in entries) {
			
			values[i]= IsActive(entry);
			i++;
		}
		
		foreach (UnityCustumEntryType entry in custumEntries) {
			
			values[i]= IsActive(entry);
			i++;
		}
		return values;
		
	}
	public override float[] GetInputsPowerState ()
	{
		if (entries == null || custumEntries == null)
			return new float[0];
		float [] values = new float[entries.Length+custumEntries.Length] ;
		int i = 0;
		foreach (UnityEntryType entry in entries) {
			values[i]= IsActiveInPourcent(entry);
			i++;
		}
		foreach (UnityCustumEntryType entry in custumEntries) {
			
			values[i]= IsActiveInPourcent(entry);
			i++;
		}
		return values;
	}
	
	public bool IsActive(UnityEntryType entry){
		return IsActiveInPourcent (entry)>0f;
	}
	
	public bool IsActive(UnityCustumEntryType entry){
		return IsActiveInPourcent (entry) > 0f;
	}
	public float IsActiveInPourcent(UnityCustumEntryType entry){

		if (entry == null)
			return 0f;
		float pct = entry.GetValue ();
		 if (entry.type == UnityCustumEntryType.CustumType.JoystickGoingUp) {
			if(! ( pct>  Mathf.Abs(joystickSensibility) ) )
			{pct=0f;}
		}
		else if (entry.type == UnityCustumEntryType.CustumType.JoystickGoingDown) {
			if(! ( pct< - Mathf.Abs(joystickSensibility) ) )
			{pct=0f;}
		}

		else if (entry.type == UnityCustumEntryType.CustumType.JoystickUp) {
			if(! ( pct> 1f-  Mathf.Abs(joystickBorder) ) )
			{pct=0f;}
		}
		else if (entry.type == UnityCustumEntryType.CustumType.JoystickDown) {
			if(! ( pct< -1f + Mathf.Abs(joystickBorder) ) )
			{pct=0f;}
		}


		return Math.Abs(pct);

	}

	public float IsActiveInPourcent(UnityEntryType entry){
	
			
				float pct = 0f;
				try{
				switch (entry) {
				case UnityEntryType.JoystickGoingLeft:
						if (Input.GetAxis ("Horizontal") < - Mathf.Abs (joystickSensibility)) 
								pct = Mathf.Abs (Input.GetAxis ("Horizontal"));
						break;
				case UnityEntryType.JoystickGoingRight:
						if (Input.GetAxis ("Horizontal") > Mathf.Abs (joystickSensibility))
								pct = Mathf.Abs (Input.GetAxis ("Horizontal"));
						break;
				case UnityEntryType.JoystickGoingUp:
						if (Input.GetAxis ("Vertical") > Mathf.Abs (joystickSensibility))
								pct = Mathf.Abs (Input.GetAxis ("Vertical"));
						break;
				case UnityEntryType.JoystickGoingDown:
						if (Input.GetAxis ("Vertical") < -Mathf.Abs (joystickSensibility))
								pct = Mathf.Abs (Input.GetAxis ("Vertical"));
						break;

				case UnityEntryType.JoystickLeft:
						if (Input.GetAxis ("Horizontal") < -1f + Mathf.Abs (joystickBorder))
								pct = Mathf.Abs (Input.GetAxis ("Horizontal"));
						break;
				case UnityEntryType.JoystickRight:
						if (Input.GetAxis ("Horizontal") > 1f - Mathf.Abs (joystickBorder))
								pct = Mathf.Abs (Input.GetAxis ("Horizontal"));
						break;
				case UnityEntryType.JoystickUp:
						if (Input.GetAxis ("Vertical") > 1f - Mathf.Abs (joystickBorder))
								pct = Mathf.Abs (Input.GetAxis ("Vertical"));
						break;
				case UnityEntryType.JoystickDown:
						if (Input.GetAxis ("Vertical") < -1f + Mathf.Abs (joystickBorder))
								pct = Mathf.Abs (Input.GetAxis ("Vertical"));
						break;

				case UnityEntryType.MouseGoingLeft:
						if (Input.GetAxis ("Mouse X") < - Mathf.Abs (mouseSensibily))
								pct = Mathf.Abs (Input.GetAxis ("Mouse X"));
						break;
				case UnityEntryType.MouseGoingRight:
						if (Input.GetAxis ("Mouse X") > Mathf.Abs (mouseSensibily))
								pct = Mathf.Abs (Input.GetAxis ("Mouse X"));
						break;
				case UnityEntryType.MouseGoingUp:
						if (Input.GetAxis ("Mouse Y") > Mathf.Abs (mouseSensibily))
								pct = Mathf.Abs (Input.GetAxis ("Mouse Y"));
						break;
				case UnityEntryType.MouseGoingDown:
						if (Input.GetAxis ("Mouse Y") < -Mathf.Abs (mouseSensibily))
								pct = Mathf.Abs (Input.GetAxis ("Mouse Y"));
						break;

				case UnityEntryType.MainFire:
						if (Input.GetButton ("Fire1"))
								pct = 1f; 
						break;
				case UnityEntryType.SecondFire:
						if (Input.GetButton ("Fire2"))
								pct = 1f;
						break;
				case UnityEntryType.ThirdFire:
						if (Input.GetButton ("Fire3"))
								pct = 1f;
						break;
				case UnityEntryType.Jump:
						if (Input.GetButton ("Jump"))
								pct = 1f;
						break;
				case UnityEntryType.ScrollingDown:
						if (Input.GetAxis ("Mouse ScrollWheel") < -Mathf.Abs (mouseSensibily))
								pct = 1f;
						break;
				case UnityEntryType.ScrollingUp:
						if (Input.GetAxis ("Mouse ScrollWheel") > Mathf.Abs (mouseSensibily))
								pct = 1f; 
						break;
				}
		}catch(Exception)
		{return 0f;}
		return pct;
	}
}

[Serializable]
public class UnityCustumEntryType
{
	public string labelName;
				public enum CustumType {Button, JoystickDown, JoystickUp, JoystickGoingDown, JoystickGoingUp}
	public CustumType type;

	public float GetValue()
	{
		if (string.IsNullOrEmpty (labelName))
						return 0f;
		try{
		switch (type) {
					case CustumType.Button: return Input.GetButton(labelName)?1f:0f;
					case CustumType.JoystickDown: return Input.GetAxis(labelName);
					case CustumType.JoystickUp: return Input.GetAxis(labelName);
					case CustumType.JoystickGoingDown: return Input.GetAxis(labelName);
					case CustumType.JoystickGoingUp: return Input.GetAxis(labelName);
			}
		}catch(Exception)
		{return 0f;}
		return 0f;
	}
}