﻿using UnityEngine;
using System.Collections;

public class MultiInputWithTimeControl : MultiInput {
	
	public bool activateTimeControl=true;
	public float delayBetween=1.0f;
	public bool isUsableInPause;
	private bool wasActive;
	private float whenLastCheckActive;
	private float whenLastActiveRelease;
	public float lastDuration;

	
	public override bool IsActive ()
	{
		float time = GetTime ();
		bool active = false;		
		if (!activateTimeControl) 
			active = IsInputsActive ();
		
		if (time - whenLastCheckActive > delayBetween) {
			if(activateTimeControl)
				active = IsInputsActive ();
			if(!wasActive && active){
				wasActive=true;
				whenLastCheckActive= time;
			}
		}
		if(!active && wasActive)
		{
			wasActive=false;
			whenLastActiveRelease=time;
		}
		
		lastDuration = GetLastActiveDuration();
		return active;
	}
	
	public float GetTime(){
		return isUsableInPause ? Time.realtimeSinceStartup:Time.timeSinceLevelLoad;}
	
	public float GetLastActiveDuration()
	{
		if (wasActive)
			return  GetTime () - whenLastCheckActive;
		else
			return  whenLastActiveRelease-whenLastCheckActive ;
	}
}
