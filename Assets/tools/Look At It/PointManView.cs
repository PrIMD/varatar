﻿using UnityEngine;
using System.Collections;
using System;
namespace be.primd.toolkit.bodytracker.v1{

public class PointManView : MonoBehaviour {
	
	public Transform rootOrigine;
	public Transform rootViewer;

	public Transform landMarkPoint;
	public float maxDistance=2f;
	public PointMenLink [] points;


	void Update () {
	
		//Vector3 direction = Relocated (landMarkPoint.position, rootOrigine.position, rootOrigine.rotation, false, true,false);
		Vector3 direction = landMarkPoint.position - rootOrigine.position;
		direction.y = 0;
		Vector3 cloneDirection = direction;

		float dist = Vector3.Distance(Vector3.zero, cloneDirection);
		if (dist > maxDistance)
			cloneDirection = cloneDirection.normalized *maxDistance;

		if (points != null && points.Length > 0) {

			foreach(PointMenLink pt in points)
			{
				if(pt!=null && pt.copie!=null && pt.origine!=null)
				{
					pt.copie.position= pt.origine.position+cloneDirection-direction;
				}
			}
		}
	}

	public static Vector3 Relocated(Vector3 point,Vector3 rootPosition ,Quaternion rootRotation, bool withIgnoreX=false, bool withIgnoreY=false, bool withIgnoreZ=false){
		//Recenter the point on cartesions axes
		Vector3 p = point- rootPosition;
		
		//Rotate points, based on the root rotation, in aim to be front of cartesians axes
		//No need to be rotate if the root is already in front of cartesians axes
		if (rootRotation.eulerAngles != Vector3.zero) {
			Quaternion rootRotationInverse = Quaternion.Inverse (rootRotation);
			p = RotateAroundPoint (p, Vector3.zero, rootRotationInverse);
		}
		if (withIgnoreX)
			p.x = 0;
		if (withIgnoreY)
			p.y = 0;
		if (withIgnoreZ)
			p.z = 0;
		
		//Debug.Log (p);
		//Debug.DrawLine (rootPosition, rootPosition+p ,  Color.red);
		return p;
	}
	
	public static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}

	[Serializable]
	public class PointMenLink
	{
		public BodyPart bodyPart;
		public Transform origine, copie;
	}
}
}