﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Renderer))]
public class GhostScript : MonoBehaviour {

	public bool turnInvisible;
	public float invisibilityInPourcent=0f;
	public float apparitionSpeed=0.05f;
	public float disparitionSpeed=0.3f;


	
	public float existingDuration=5f;
	public float lifeTime=0f;
	public bool deactivateWhenFinish;
	public bool destroyWhenFinish;

	public Renderer ghostRenderer;

	public void Awake(){
		
		if (ghostRenderer == null)
			ghostRenderer = GetComponent<Renderer> () as Renderer;

		if (ghostRenderer != null && ghostRenderer.material != null) {
			Color col = ghostRenderer.material.color;
			col.a=invisibilityInPourcent;
			ghostRenderer.material.color = col;
		}

	}

	public void Start()
	{
		lifeTime = existingDuration;
	}

	public void Update()
	{

		if (lifeTime > 0f) {
			lifeTime-= Time.deltaTime;
			if(lifeTime<=0){
				turnInvisible=true;
				lifeTime=0;
			}
		}
		
		if (turnInvisible && invisibilityInPourcent > 0f) {
			invisibilityInPourcent = Mathf.Clamp(invisibilityInPourcent-disparitionSpeed * Time.deltaTime, 0f,1f);
		}
		if (!turnInvisible && invisibilityInPourcent < 1f) {
			invisibilityInPourcent= Mathf.Clamp(invisibilityInPourcent + apparitionSpeed * Time.deltaTime, 0f,1f);
		}


		if (ghostRenderer!=null & ghostRenderer.material!=null ) {
			Color col = ghostRenderer.material.color;
			col.a= invisibilityInPourcent;
			ghostRenderer.material.color=col;
			
			if(col.a<=0f)ghostRenderer.enabled=false;
			if(col.a>0f)ghostRenderer.enabled=true;
			
			if(deactivateWhenFinish &&  col.a<=0 && lifeTime<=0) this.gameObject.SetActive(false);
			if(destroyWhenFinish &&  col.a<=0 && lifeTime<=0) Destroy(this.gameObject); 
		}

	
	}



}
