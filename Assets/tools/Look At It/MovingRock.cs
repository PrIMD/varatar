﻿using UnityEngine;
using System.Collections;

public class MovingRock : MonoBehaviour {

	public Transform rock;
	public float speed =0.2f;
	public int cursor;
	public Transform [] movePoints;
	public enum MoveType{Linera, Circle, Rand}
	public MoveType moveType; 
	public float nextToDistance=0.2f;

	public Transform lastTarget;
	public  Transform nextTarget;

	public bool up=true;


	public void Start(){

		nextTarget = Next (moveType);

	}


	public void Update(){

		if (nextTarget != null && rock!=null) {
			Vector3 direction= nextTarget.position- rock.position;
			rock.position+=direction.normalized*Time.deltaTime*speed;

			if(Vector3.Distance(rock.position, nextTarget.position)<nextToDistance)
			{
				Transform next =Next(moveType);
				if(next!=null)
				{
					lastTarget= nextTarget;
					nextTarget= next;
				}
			}

		
		}

	}


	public Transform Next(MoveType moveType)
	{
		Transform nextTarget=null;

		switch (moveType) {
		case MoveType.Circle:	nextTarget = NextCircle();break;
		case MoveType.Linera:	nextTarget = NextLinear();break;
		case MoveType.Rand	:	nextTarget = NextRand();break;		
		}
		if (up)
						cursor++;
				else
						cursor--;
		return nextTarget;
	}

	
	Transform NextCircle ()
	{
		if (movePoints != null && movePoints.Length > 0) {
			if(cursor>= movePoints.Length){ cursor=0;}
			else if(cursor<0) cursor = movePoints.Length-1;
			return movePoints[cursor];		
		}
		return null;
	}

	Transform NextLinear ()
	{
		if (movePoints != null && movePoints.Length > 0) {
			if(cursor>=movePoints.Length){ up=false;cursor=movePoints.Length-1;}
			else if(cursor<0){up=true;cursor=0;}
			return movePoints[cursor];		
		}
		return null;
	}


	 Transform NextRand ()
	{
				if (movePoints == null || movePoints.Length == 0)
						return null;
				if (cursor < 0)
						cursor = 0;
				else if (cursor > movePoints.Length)
						cursor = movePoints.Length - 1;

				Transform transf = null;
				int maxTry = 10, didTry = 0;
				if (movePoints.Length == 1) {
						return movePoints [0];
				} else {
					bool ok = false;
						while ( !ok && didTry<maxTry) {
								transf = movePoints [Random.Range (0, movePoints.Length)];
							ok = transf!=null && transf != lastTarget ;
						}
					
					if(!ok) return NextCircle();
					

		}return transf;
		}
}
