﻿using UnityEngine;
using System.Collections;

public class GravityCircle : MonoBehaviour {

	public Transform gravitationCenter;
	public Vector3 positionAdjustement;
	public float gravitySpeed=2f;
	
	public float gravityRadius =3f;
	public float gravityRadiusVariation =1f;
	public float gravityHeight =1f;
	public float gravityHeightVariation =3f;
	public bool  up;

	public float degree;
	private float degreePerSecond=90f;

	public float height;
	public float heightPerSecond=0.5f;


	public float x=1f, y=0f;
	public bool xUp=false, yUp=true;

	public void Start()
	{
		x = Random.Range (-1f, 1f);
		y = Random.Range (-1f, 1f);
		height = Random.Range (gravityHeight - gravityHeightVariation, gravityHeight + gravityHeightVariation);

	}
	void Update () {


		float dt = Time.deltaTime;
		x +=dt * (xUp ? 1f : -1f);
		if (x > 1f) xUp = false;
		else if (x < -1f) xUp = true;

		
		y += dt * (yUp ? 1f : -1f);
		if (y > 1f) yUp = false;
		else if (y < -1f) yUp = true;


		if (up) {
			height+=heightPerSecond * Time.deltaTime;
			if(height > gravityHeight + gravityHeightVariation)
				up=false;
		}
		else 
		{
			height-=heightPerSecond * Time.deltaTime;
			if(height < gravityHeight - gravityHeightVariation)
				up=true;
		}

		degree += degreePerSecond * Time.deltaTime;
	
		Vector3 attractionPosition = GetGravityPosition ();
		attractionPosition.y += height;
		attractionPosition.x += x * gravityRadius;
		attractionPosition.z += y * gravityRadius;



		if (gravitationCenter != null) {
			transform.position = Vector3.Lerp(transform.position,attractionPosition,Time.deltaTime*gravitySpeed);		
		}
	}
	
	public Vector3 GetGravityPosition(){
		if (gravitationCenter == null)
			return transform.position+ positionAdjustement;
		return gravitationCenter.position + positionAdjustement;
	}

	public void SetGravityCenter (Transform gravitationCenter, Vector3 adjustement)
	{
		this.gravitationCenter = gravitationCenter;
		this.positionAdjustement = adjustement;
	}

	public void SetSpeed (float gravitySpeed)
	{
		this.gravitySpeed = gravitySpeed;
	}
	public void SetRadius(float radius, float variation)
	{
		this.gravityRadius = radius;
		this.gravityRadiusVariation = variation;
	}
	public void SetHeight(float height, float variation)
	{
		this.gravityHeight = height;
		this.gravityHeightVariation = variation;
	}
}
