﻿using UnityEngine;
using System.Collections;

public class FloatingRock : MonoBehaviour {

	public float rotationPerSecond=0f;
	public float heightVariation =10f;
	public float heightVariationSpeed =0.3f;
	private float height;
	private bool up;
	//private float initialWorldHeight;
	public bool isInitialize;
	public void Start(){
		height = 0f;
	//	initialWorldHeight = transform.position.y;
		
		up= Random.Range(0,2)==1;

	}
	public void Update(){
		float dt = Time.deltaTime;
		height +=dt * heightVariationSpeed * (up ? 1f : -1f);
		if ( height > heightVariation/2f) up = false;
		else if ( height < -heightVariation/2f) up = true;

		if( rotationPerSecond>0f || rotationPerSecond<0f )
		transform.Rotate (Vector3.up * rotationPerSecond, Space.Self);
		Vector3 pos = transform.position;
		pos.y += Time.deltaTime*heightVariationSpeed * ( height>0?1f:-1f);
		transform.position = pos;
	}

}
