﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice,
 * you can do whatever you want with this code. 
 * If you think this stuff is worth it, you can buy me a beer in return.
 * At least, if you use it in a commercial or in a big project, 
 * let's me know it ;)
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;
using System;
public class GhostPopper : MonoBehaviour {

	public float maxLoopTime = 20;
	public float time;
	public GhostPopperData [] ghostsAppartion;


	// Use this for initialization
	void Start () {
		ResetLoop ();
	}


	void Update () {
		time += Time.deltaTime;
		if (time >= maxLoopTime){
			ResetLoop();
		}

		foreach (GhostPopperData ghost in ghostsAppartion) {
			if( ghost!=null && ghost.what!=null && ghost.where!=null  && !ghost.IsApperead && ghost.when<= time)
			{
			 GameObject gamo =	GameObject.Instantiate(ghost.what, ghost.where.position,ghost.where.rotation) as GameObject;
				gamo.SetActive(true);
				ghost.IsApperead=true;
			}		
		}

	}
	
	public void ResetLoop()
	{	
		time = 0;
		foreach (GhostPopperData g in ghostsAppartion) {
			g.IsApperead=false;
		}
	}
}

[Serializable]
public class GhostPopperData
{
	public float when;
	public GameObject what;
	public Transform where;
	
	private bool isApperead;

	public bool IsApperead {
		get {
			return this.isApperead;
		}
		set {
			isApperead = value;
		}
	}

}