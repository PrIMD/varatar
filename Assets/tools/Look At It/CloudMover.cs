﻿using UnityEngine;
using System.Collections;

public class CloudMover : MonoBehaviour {

	public Transform cloudRoot;
	public Rigidbody cloudGravity;
	public CharacterController character;
	public ForceMode walkForceType;
	public ForceMode pushForceType;
	
	public float globalSpeedRegulation =1f;
	public float leftSpeed=2f;
	public float rightSpeed=2f;
	public float frontSpeed=4f;
	public float backSpeed=1.5f;
	public float downSpeed=1f;
	public float topSpeed=1f;

	public float leftRotateSpeed	=90f;
	public float rightRotateSpeed	=90f;

	public float globalPushRegulation =1f;
	public float leftPushPower=20f;
	public float rightPushPower=20f;
	public float frontPushPower=40f;
	public float backPushPower=15f;
	public float downPushPower=10f;
	public float topPushPower=10f;
	
	public float leftRotatePushPower	=10f;
	public float rightRotatePushPower	=10f;


	public float friction= 0.1f;



	public void Update(){
			
		if(cloudGravity!=null)
		{
			cloudGravity.velocity *= Mathf.Pow(friction,Time.deltaTime); 
		}
	}
	
	private void Push(float power, Vector3 direction){
		
	
		if (cloudGravity != null) {
			cloudGravity.AddForce(transform.rotation*direction*(power*globalPushRegulation), pushForceType);
		}
	}

	
	private void GoToward(float power, Vector3 direction){

		if(cloudRoot!=null)
			cloudRoot.position += (cloudRoot.rotation*direction * power*globalSpeedRegulation);

	}
	
	public void RotateToward(float power, Vector3 direction){


		cloudRoot.Rotate (direction * power);

	}

	public void PushLeft(float power)
	{	
		Push (power*leftPushPower, Vector3.left);
	}
	public void GoLeft(float pct, float deltaTime){
		pct = Mathf.Clamp (pct,0f, 1f);
		GoToward (pct * deltaTime*leftSpeed, Vector3.left);}
	
	public void PushRight(float power){

		Push (power*rightPushPower, Vector3.right);
	}
	public void GoRight(float pct, float deltaTime){
		pct = Mathf.Clamp (pct,0f, 1f);
		GoToward (pct * deltaTime*rightSpeed, Vector3.right);}

	public void PushUp(float power)
	{
		
		Push (power*topPushPower, Vector3.up);
	}
	public void GoUp(float pct, float deltaTime){
		pct = Mathf.Clamp (pct,0f, 1f);
		GoToward (pct * deltaTime*topSpeed, Vector3.up);}

	public void PushDown(float power){

		Push (power*downPushPower, Vector3.down);
	}
	public void GoDown(float pct, float deltaTime)
	{
		pct = Mathf.Clamp (pct,0f, 1f);
		GoToward (pct * deltaTime*downSpeed, Vector3.down);
	}

	public void PushBack(float power){
		
		Push (power*backPushPower, Vector3.back);
	}
	public void GoBack(float pct, float deltaTime){
		pct = Mathf.Clamp (pct,0f, 1f);
		GoToward (pct * deltaTime*backSpeed, Vector3.back);}

	public void PushFront(float power){
		
		Push (power*frontPushPower, Vector3.forward);
	}
	public void GoFront(float pct, float deltaTime)
	{	pct = Mathf.Clamp (pct,0f, 1f);
		GoToward (pct * deltaTime*frontSpeed, Vector3.forward);
	}

	public void TurnLeft(float pct, float deltaTime)
	{
		pct = Mathf.Clamp (pct,0f, 1f);	
		RotateToward (pct * deltaTime*leftRotateSpeed, Vector3.down);
	}

	public void TurnRight(float pct, float deltaTime)
	{
		pct = Mathf.Clamp (pct,0f, 1f);
		RotateToward (pct * deltaTime*rightRotateSpeed, Vector3.up);
	}

}
