/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */



public enum PositionType  {

	TopLeftCorner, TopRightCorner, BotLeftCorner, BotRightCorner, Center,Top, Bot, Left, Right
}

public enum MesureType
{

	PX, PCT
}
public enum FormType {Rectangle, Circle}