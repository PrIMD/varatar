/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using System;
		public interface I_ZoneElement
		{
			/**Do the x, y coord are in the zone reserved by this object*/
			bool IsInZone(float x, float y);

			/**Do the object accept to share the zone it need*/
			//bool IsZoneSharable();
			
			/** Display in the scene view the rectangle of the zone used*/
			void DisplayDebugZone();
		}


