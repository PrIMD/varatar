﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScreenZone : MonoBehaviour {

	public bool positionRefresh =false;
	public float refresh=0.5f;
	private float lastRefresh;
	public PositionType  positionType = PositionType.Center;
	public MesureType mesure  = MesureType.PX;

	/** OnWidth, the user ask to affect the zone by the width of the screen.
	 OnHeight, the user ask to affect the zone by the height of the screen.	
		None, the user ask the zone to be deformed by the size of the screen*/
	public enum MenuResizeType {OnWidth, OnHeight,None}
	/**Do the element is affected by the width of the screen or the height*/
	public MenuResizeType  resizeOn = MenuResizeType.OnWidth;

	/**Do the player want to center the object instead of the left corner position*/
	public bool centered = true;
	/**Please do not modify, the localisation the player ask.*/
	public Rect localisation  = new Rect (0,0,50,50);

	public FormType form;
	/**The localisation the player ask, but translate by the params request*/
	protected Rect finalLocalisation ; 

	public bool displayDebugZone =true;


	protected virtual void Awake ()
	{
		RefreshPositionAndSize();
	}
	protected virtual void Start ()
	{
		RefreshPositionAndSize();
	}

	protected virtual void Update()
	{
		if (positionRefresh){
		float time = Time.realtimeSinceStartup;
			if (time - lastRefresh > refresh) {
				lastRefresh = time;	
				RefreshPositionAndSize ();
			}	
		}
		
		if(displayDebugZone)
			DisplayDebugZone();
	}
	public virtual void RefreshPositionAndSize()
	{


		finalLocalisation = ScreenPositionTool.GetScreenLocalisationOf(positionType, mesure, localisation , centered,IsSizeOnWidth(), IsSizeOnHeight());

	}

	public bool IsSizeOnWidth()
	{
		return MenuResizeType.OnWidth.Equals (resizeOn) ;
	}
	public bool IsSizeOnHeight()
	{
		
		return MenuResizeType.OnHeight.Equals (resizeOn) ;
	}

	public Rect GetFinalPosition() { return finalLocalisation;}


	public bool IsInZone (float x, float y)
	{
		switch(form){
		case FormType.Rectangle: return ScreenPositionTool.IsPointInRectangleZone(x,y, finalLocalisation);
		case FormType.Circle:
			float radius = GetCircleRadius ();
			float centerX = finalLocalisation.x + finalLocalisation.width/2f;
			float centerY = finalLocalisation.y + finalLocalisation.height/2f;
			return ScreenPositionTool.IsPointInCircleZone(x,y, centerX,centerY,radius);
		}
		return false;
	}

	private float GetCircleRadius(){return finalLocalisation.width<finalLocalisation.height?finalLocalisation.width:finalLocalisation.height;}
	
	public void DisplayDebugZone ()
	{
		float x =finalLocalisation.x;
		float y =finalLocalisation.y;
		//if (form.Equals (FormType.Rectangle)) {
						float width = finalLocalisation.width;
						float height = finalLocalisation.height;
			
						Vector2 topLeft = new Vector2 (x, -y);
						Vector2 topRight = new Vector2 (x + width, -y);
						Vector2 botLeft = new Vector2 (x, -(y + height));
						Vector2 botRight = new Vector2 (x + width, -(y + height));
			
						Debug.DrawLine (topLeft, topRight, Color.green);
						Debug.DrawLine (topLeft, botLeft, Color.green);
			
						Debug.DrawLine (botRight, topRight, Color.green);
						Debug.DrawLine (botRight, botLeft, Color.green);
			
			
			
						x = 0;
						y = 0;
						width = Screen.width;
						height = Screen.height;
			
						topLeft = new Vector2 (x, -y);
						topRight = new Vector2 (x + width, -y);
						botLeft = new Vector2 (x, -(y + height));
						botRight = new Vector2 (x + width, -(y + height));
			
						Debug.DrawLine (topLeft, topRight, Color.yellow);
						Debug.DrawLine (topLeft, botLeft, Color.yellow);
			
						Debug.DrawLine (botRight, topRight, Color.yellow);
						Debug.DrawLine (botRight, botLeft, Color.yellow);
			//	} else if (form.Equals (FormType.Circle)) {}
	}
}
