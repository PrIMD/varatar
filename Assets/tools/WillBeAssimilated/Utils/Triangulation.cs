﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Triangulation : MonoBehaviour {
	
	public Transform a;
	public Transform b;
	public Transform c;
	
	public Transform direction;
	public Transform point;

	
	public Transform compareStart;
	public Transform compareEnd;

	public bool drawDebug=true;
	
	public virtual void Update()
	{
		if(drawDebug)
		{
			if(a!=null && b!=null && c!=null ){
			Debug.DrawLine (a.position,b.position,Color.magenta);
			Debug.DrawLine (c.position,b.position,Color.magenta);
			Debug.DrawLine (c.position,a.position,Color.magenta);
			}
			if(a!=null && b!=null && c!=null && point!=null){
				
				Vector3 center = (a.position+b.position+2f*c.position)/4f;

			Debug.DrawLine (a.position,point.position,Color.white);
			Debug.DrawLine (c.position,point.position,Color.white);
			Debug.DrawLine (b.position,point.position,Color.white);

			Debug.DrawLine( center, direction.position, Color.green);
			}
		}
	}


}
