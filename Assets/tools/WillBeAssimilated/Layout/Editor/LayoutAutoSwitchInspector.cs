﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LayoutAutoSwitch))]
public class LayoutAutoSwitchInspector : Editor {


	public override void OnInspectorGUI()
	{
		if (! Application.isPlaying) {
			DrawDefaultInspector ();
		} else {
			LayoutAutoSwitch layoutSwitch = (LayoutAutoSwitch)target ;
			EditorGUILayout.LabelField("Layout > Mode > Targeted layouts");
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			foreach( AutoSwitch asw in layoutSwitch.autoSwitch)
			{
				string print=" ";
				print+=asw.layout+" > "+asw.GetMode()+" >  ";
				foreach(string s in asw.layoutsTargeted)
				{
					print+= " "+s;
				}
				EditorGUILayout.LabelField(print);

			}
			
			EditorGUILayout.Space();
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Friend: targets -> follow -> layout state");
			EditorGUILayout.LabelField("Enemy: layout -> deactivates -> targets when on");
			EditorGUILayout.LabelField("Antipode: targets is -> inverse -> of the layout");
			EditorGUILayout.LabelField("Hidden: targets -> appear -> when layout is off");

		}

	}
}