﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Layout))]
public class LayoutInspector : Editor {


	public override void OnInspectorGUI()
	{
		EditorGUILayout.LabelField("Layout Names:");


		foreach (string s in Layout.GetLayoutNames()) {
			EditorGUILayout.BeginHorizontal();
			bool value=Layout.IsActivate(s);
			GUILayout.Label("> "+s+": ");
			if(GUILayout.Button(""+value))
			{
				if(value)Layout.Deactivate(s);
				else Layout.Activate(s);
			}
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button ("Activate All")) {
			Layout.ActivateAll();
		}
		if (GUILayout.Button ("Deactivate All")) {
			Layout.DeactivateAll();
		}
		EditorGUILayout.EndHorizontal();

	}
}
