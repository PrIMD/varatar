﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class Layout : MonoBehaviour  {




	private static Dictionary<string, bool> layouts = new Dictionary<string,bool>();
	public static EventHandler<LayoutActivateEventArgs> OnActivationDetected;
	public static EventHandler<LayoutDeactivateEventArgs> OnDeactivationDetected;
	public static EventHandler<LayoutDeactivateAllEventArgs> OnDeactivateAll;





	public static bool CheckAndCreateLayout (string layout)
	{
		//IS LAYOUT VALIDE
		if (string.IsNullOrEmpty (layout) || layout.Length <= 0)
			return false;
		//VERIFY THAT IT IS OR CAN BE IN LAYOUTS
		if (!layouts.ContainsKey (layout)) {
			if (!AddLayout (layout, false))
				return false;
		}
		return true;
	}

	public static bool AddLayout(string name, bool value){
		if (! string.IsNullOrEmpty (name) && name.Length > 0) {
						layouts.Add (name, value);
			return true;
				}
		return false;
	}

	public static string [] GetLayoutNames(){
		string [] layoutNames = new string[layouts.Count];
		layouts.Keys.CopyTo (layoutNames, 0);
		return layoutNames;
	}

	private static bool IsValide(string layoutName){
		
		if (string.IsNullOrEmpty (layoutName) || layoutName.Length<=0)
			return false;
		if (! layouts.ContainsKey (layoutName)) {
			AddLayout (layoutName,false);
			return true;
		}
		return true;
	}

	public static void DeactivateAll()
	{
		string [] keys = GetLayoutNames ();
		if (keys != null && keys.Length > 0) {
			foreach(string s in keys)
				Deactivate(s,false);
		
			OnDeactivateAll( null, new LayoutDeactivateAllEventArgs(GetLayoutNames()));
		}
	}
	public static void ActivateAll()
	{
		string [] keys = GetLayoutNames ();
		if (keys != null && keys.Length > 0) {
			foreach(string s in keys)
				Activate(s);
		}
	}
	public static void Activate(string layoutName)
	{
		if (!IsValide (layoutName))
			return;
		layouts [layoutName] = true;
		if (OnActivationDetected != null)
			OnActivationDetected (layoutName, new LayoutActivateEventArgs (layoutName));



	}
	public static void Deactivate(string layoutName, bool withEventSend =true)
	{
		
		if (!IsValide (layoutName))
			return;
		layouts [layoutName] = false;
		if (withEventSend &&OnDeactivationDetected != null)
			OnDeactivationDetected (layoutName, new LayoutDeactivateEventArgs (layoutName));
		

	}
	
	public static void Activate(string [] layoutNames )
	{
		if (layoutNames == null)
			return;
		foreach (string s in layoutNames)
			Activate (s);
	}
	public static void Deactivate(string [] layoutNames){
		
		if (layoutNames == null)
			return;
		foreach (string s in layoutNames)
			Deactivate (s);
	}

	
	public static bool IsActivate(string layoutName){
		if (!IsValide (layoutName))
						return false;
		bool isActive = false;
		try{
			layouts.TryGetValue (layoutName,out isActive);
		}catch(Exception){}
		return isActive;}

	public static bool AreActivate(string [] layoutNames){
		if (layoutNames == null)
						return false;
		foreach (string s in layoutNames)
			if (! IsActivate (s))
				return false;
		return true;
	}


}
public class LayoutActivateEventArgs :EventArgs
{
	public string layoutName;
	public LayoutActivateEventArgs (string layoutName)
	{
		this.layoutName = layoutName;
	}
	
}
public class LayoutDeactivateEventArgs :EventArgs
{
	
	public string layoutName;
	public LayoutDeactivateEventArgs (string layoutName)
	{
		this.layoutName = layoutName;
	}
}

public class LayoutDeactivateAllEventArgs :EventArgs
{
	
	public string [] layouts;
	public LayoutDeactivateAllEventArgs (string [] layouts)
	{
			this.layouts = layouts;
	}
}

