
public interface LayoutAffected  {
	
	bool AreLayoutValide();
	string [] GetUsedLayoutNames();
}