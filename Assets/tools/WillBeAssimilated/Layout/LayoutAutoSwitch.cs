﻿using UnityEngine;
using System.Collections.Generic;
using System;
[RequireComponent(typeof(Layout))]
public class LayoutAutoSwitch : MonoBehaviour {

	public string[] layoutStartActivate;
	public AutoSwitch []   autoSwitch;


	public HashSet<string> alreadySwitch = new HashSet<string>();

	
	/**In unity you can setup before the call start some enemy layout that will deactivate all his enemy layout when it is activate*/
	private static Dictionary<string ,HashSet<String>> enemyLayouts = new Dictionary<string, HashSet<string>> ();
	/**In unity you can setup before the call start some friend layout that will activate all his allies layout when it is activate*/
	private static Dictionary<string ,HashSet<String>> friendLayouts = new Dictionary<string, HashSet<string>> ();
	/**In unity you can setup before the call start some friend layout that will activate all his allies layout when it is activate*/
	private static Dictionary<string ,HashSet<String>> antipodeLayouts = new Dictionary<string, HashSet<string>> ();
	/**In unity you can setup before the call start some friend layout that will activate all his allies layout when it is activate*/
	private static Dictionary<string ,HashSet<String>> hiddenLayouts = new Dictionary<string, HashSet<string>> ();

	
	void Start () {
		Layout.OnActivationDetected += ActivationDetected;
		Layout.OnDeactivationDetected += DeactivationDetected;

		ResetWithValue ();
		Layout.Activate(layoutStartActivate);
	
	}

	void Update(){
		alreadySwitch.Clear();
	}

	private void OnOff(bool onOff,string layoutName, Dictionary<string ,HashSet<String>> dictonary){
		if (dictonary.ContainsKey (layoutName)) {
			HashSet<string> list = null;
			if (dictonary.TryGetValue (layoutName, out list)) {
				foreach (string s in list) 
				{
					if(onOff) Layout.Activate (s);
					else	Layout.Deactivate (s);	   
				}     
			}
		}
	}
	
	public void ActivationDetected(object obj, LayoutActivateEventArgs args)
	{
		if (alreadySwitch.Add (args.layoutName)) {

			OnOff(false, args.layoutName, enemyLayouts);
			OnOff(true, args.layoutName, friendLayouts);
			OnOff(false, args.layoutName, antipodeLayouts);

			}
		
	}

	public void DeactivationDetected(object obj, LayoutDeactivateEventArgs args)
	{
		
		if (alreadySwitch.Add (args.layoutName)) {
			OnOff(false, args.layoutName, friendLayouts);
			OnOff(true, args.layoutName, hiddenLayouts);
			OnOff(true, args.layoutName, antipodeLayouts);
		}
	}



	
	private static void Add(string layout, string [] layoutToLink, Dictionary<string ,HashSet<String>> targetDictionary){
		//IS LAYOUT VALIDE
		if (Layout.CheckAndCreateLayout (layout)) {
			//GREAT WE CAN ADD IT TO FRIENDS LIST
			
			//CHECK IF IT IS NOT IN FRIEND LIST
			if (! targetDictionary.ContainsKey (layout)) {
				targetDictionary.Add(layout, new HashSet<string>());		
			}
			
			//GET THE LIST FOR LAYOUT FRIEND
			HashSet<string> targetList =null;
			try{
				targetDictionary.TryGetValue(layout,out targetList);
			}catch(Exception exc)
			{Debug.Log(exc); return;}
			
			//ADD FRIEND TO THE LIST
			if(targetList!=null)
			{
				foreach(string s in layoutToLink)
				{
					if(Layout.CheckAndCreateLayout(s))
						targetList.Add(s);
				}
			}
		}
	}


	public void ResetWithValue()
	{
		friendLayouts.Clear ();
		enemyLayouts.Clear ();
		antipodeLayouts.Clear ();
		hiddenLayouts.Clear ();


		foreach (AutoSwitch _autoSwitch in autoSwitch) {
			if(_autoSwitch!=null  ){
				Dictionary<string ,HashSet<String>>  dico = null;
				switch(_autoSwitch.GetMode())
				{
					case AutoSwitch.SwitchMode.Friend:
						dico = friendLayouts;break;
					case AutoSwitch.SwitchMode.Enemy:
						dico = enemyLayouts;break;
					case AutoSwitch.SwitchMode.Antipode:
						dico = antipodeLayouts;break;
					case AutoSwitch.SwitchMode.Hidden:
						dico = hiddenLayouts;break;
				}
				Add (_autoSwitch.layout,_autoSwitch.layoutsTargeted, dico);
			}
		}

	}
}
[Serializable]
public class AutoSwitch
{
	public string layout;
	public enum Switch {ActivateTargets, DeactivateTargets,DoNothing}
	public enum SwitchMode {Friend,Enemy, Antipode, Hidden,Unmanaged}
	public Switch whenLayoutOn  = Switch.ActivateTargets;
	public Switch whenLayoutOff = Switch.DeactivateTargets;
	public string  [] layoutsTargeted;

	public SwitchMode GetMode()
	{
		if(whenLayoutOn== Switch.ActivateTargets && whenLayoutOff== Switch.DeactivateTargets)
			return SwitchMode.Friend;
		
		if(whenLayoutOn== Switch.DeactivateTargets && whenLayoutOff== Switch.DoNothing)
			return SwitchMode.Enemy;
		
		if(whenLayoutOn== Switch.DeactivateTargets && whenLayoutOff== Switch.ActivateTargets)
			return SwitchMode.Antipode;
		
		if(whenLayoutOn== Switch.DoNothing && whenLayoutOff== Switch.ActivateTargets)
			return SwitchMode.Hidden;
		return SwitchMode.Unmanaged;
	}
	
}
