using UnityEngine;
public interface IScalable  {
	
	///*
	//Ask to move the object  in that direction
	void ScaleAdd ( Vector3 scale   );
	//Askt to move at this position
	void ScaleTo (Vector3 scale);
	
	// Ask to move of x pourcentage depending on what is the object that inherit
	void Horizontal (float percentage);
	void Vertical (float percentage);
	void Deepness (float percentage);
	
	//Ask to activate code corresponding to move in this direction
	void Horizontal ();
	void Vertical ();
	void Deepness ();
	
	//To be able to move the user must say it before asking to move  in aim to be able to cancel the move
	bool StartScaling();
	// Ask to be permanent move (reset data)
	Vector3 Validate();
	// Ask to cancel the move and to give the old position
	Vector3 Cancel();
	
	// Give a clue to the inheritant of about what it have to move

	void SetRelative(Transform relativeTo);
	//void SetMoveRelativity(Space relativeTo); 
	Vector3 GetInitialScale();
	 //*/
}