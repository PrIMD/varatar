﻿using UnityEngine;
using System.Collections;

public class DefaultMovable : Movable {

	public Vector3 initialPosition;
	public float velocity =1f;
	protected Space relativeTo = Space.World;

	
	public override Vector3 GetInitialPosition ()
	{return initialPosition;}

	public override void Move (Vector3 direction)
	{

		this.transform.Translate (direction*velocity,relativeTo);
	}
	public override void MoveAt (Vector3 position)
	{
	if(Space.World.Equals(relativeTo))
		this.transform.position = position;
	if(Space.Self.Equals(relativeTo))
		this.transform.localPosition = position;
	}

	public override bool StartMoving ()
	{
		bool isOverriding = initialPosition != Vector3.zero;
		initialPosition = this.transform.position;

		return isOverriding;
	}

	public override Vector3 Validate ()
	{
		initialPosition = Vector3.zero;
		return transform.position;
	}

	public override Vector3 Cancel ()
	{
		this.transform.position = initialPosition;
		Vector3 initialPosTMP = initialPosition;
		initialPosition = Vector3.zero;
		return initialPosTMP;

	}


	public override void Left (float percentage)
	{
		transform.Translate (Vector3.left*percentage*velocity,relativeTo);
	
	}

	public override void Right (float percentage)
	{
		transform.Translate (Vector3.right*percentage*velocity,relativeTo);
	}

	public override void Up (float percentage)
	{
		transform.Translate (Vector3.up*percentage*velocity,relativeTo);
	}

	public override void Down (float percentage)
	{
		transform.Translate (Vector3.down*percentage*velocity,relativeTo);
	}

	public override void Front (float percentage)
	{
		transform.Translate (Vector3.forward*percentage*velocity,relativeTo);
	}

	public override void Back (float percentage)
	{
		transform.Translate (Vector3.back*percentage*velocity,relativeTo);
	}

	public override void SetRelative (Space relativeTo)
	{
		this.relativeTo = relativeTo;
	}

}
