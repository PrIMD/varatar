﻿using UnityEngine;
using System.Collections;

public abstract class Rotable : MonoBehaviour, IRotable{
	


	public abstract void Rotate (Vector3 direction);

	public virtual void RotateTo (Quaternion rotation){}
	public virtual void RotateTo (Vector3 eulerRotation){}
	public virtual void RotateAroundX (float percentage){}
	public virtual void RotateAroundY (float percentage){}
	public virtual void RotateAroundZ (float percentage){}

	public virtual void Left (){}

	public virtual void Right (){}

	public virtual void Up (){}
	public virtual void Down (){}

	public virtual void HorizontalLeft (){}
	public virtual void HorizontalRight (){}

	public abstract bool StartRotating ();
	public abstract Quaternion Validate ();
	public abstract Quaternion Cancel ();
	public virtual void SetRelative (Space relativeTo){}

	public virtual void SetRelative (Transform relativeTo){}
	public abstract Quaternion GetInitialRotation ();
}
