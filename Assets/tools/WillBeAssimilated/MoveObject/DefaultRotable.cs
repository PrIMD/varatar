﻿using UnityEngine;
using System.Collections;

public class DefaultRotable : Rotable {

	
	public Quaternion initialRotation;
	public Transform compareTo;
	protected Space relativeTo = Space.World;

	public override void Rotate (Vector3 direction)
	{
		if (compareTo == null) {
			this.transform.Rotate (direction,relativeTo);
			
		} else
		{this.transform.RotateAround (compareTo.position,direction, 1);}
	}


	public override void RotateTo (Quaternion rotation)
	{
			if (Space.World.Equals (relativeTo))
				this.transform.rotation = rotation;
			if (Space.Self.Equals (relativeTo))
				this.transform.localRotation = rotation;
				
	
	}

	public override void RotateTo (Vector3 eulerRotation){

			if (Space.World.Equals (relativeTo))
				this.transform.eulerAngles = eulerRotation;
			if (Space.Self.Equals (relativeTo))
				this.transform.localEulerAngles = eulerRotation;

	}

	public override void RotateAroundX (float percentage)
	{
		Vector3 v = new Vector3 (percentage, 0f, 0f);
		Rotate( v );
	}

	public override void RotateAroundY (float percentage)
	{
		
		Vector3 v = new Vector3 (0f, percentage,0f);
		Rotate( v );
	}

	public override void RotateAroundZ (float percentage)
	{
		
		Vector3 v = new Vector3 (0f,0f,percentage);
		Rotate( v );
	}


	public override bool StartRotating ()
	{
		bool isOverriding = initialRotation.eulerAngles != Vector3.zero ; 
		initialRotation = this.transform.rotation;
		return isOverriding;
	}

	public override Quaternion Validate ()
	{	
		initialRotation.eulerAngles = Vector3.zero;
		return transform.rotation;
	}

	public override Quaternion Cancel ()
	{
		
		this.transform.rotation = initialRotation;
		Quaternion initialRotTMP = initialRotation;
		initialRotation.eulerAngles = Vector3.zero;
		return initialRotTMP;
	}

	public override void SetRelative (Space relativeTo)
	{
		
		this.relativeTo = relativeTo;
	}

	public override void SetRelative (Transform relativeTo)
	{
		this.compareTo = relativeTo;
	}

	public override Quaternion GetInitialRotation ()
	{
		return initialRotation;
	}
}
