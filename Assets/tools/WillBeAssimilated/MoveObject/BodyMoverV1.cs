﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;

public class BodyMoverV1 : TwinCursorsBehavior {


	public Mover mover;
	public bool withLandMark = true;
	public IMovable  movable;
	public GameObject target;
	public GameObject follow;
	
	public BodyPosition left;
	public BodyPosition up;
	public BodyPosition right;
	public BodyPosition down;
	public BodyPosition front;
	public BodyPosition back;
	public BodyPosition cancel;
	public BodyPosition validate;

	public bool moving;
	public float refresh =0.1f;
	public float velocity =2f;
	public float realVelocity =2f;

	
	private float lastTime;
	void Update () {
		if (mover == null || target==null || follow ==null)
			return;

		float t = Time.time;
		if ( t - lastTime > refresh) {
			lastTime=t;


			if (X360.IsA ()) {
				moving=true;
				mover.StartMove (target.transform, follow.transform, withLandMark);

			}
		
			if (X360.IsRb () && mover.IsMoving()) 
			{
				moving=false;

				target.transform.position = mover.Validate (withLandMark);
			}
			
			if (X360.IsLb ()  && mover.IsMoving()) 
			{
				moving=false;
				
				target.transform.position = mover.Cancel ();
			}
			
		}

		
		if( mover.IsMoving()){
			realVelocity = GetRealVelocity();
			if(realVelocity>0.2f){
				float delta = Time.deltaTime* realVelocity*100f;
				if(left!=null && left.IsActive())
				{
					if(movable!=null){movable.Left(0.1f);} else
					mover.AddMove(Vector3.left *delta);
					
				}
				
				if(right!=null && right.IsActive())
				{
					
					if(movable!=null){movable.Right(0.1f);} else
					mover.AddMove(Vector3.right *delta );
					
				}

				
				if(down!=null && down.IsActive())
				{
					
					if(movable!=null){movable.Down(0.1f);} else
					mover.AddMove(Vector3.down *delta );
					
				}
				if(up!=null && up.IsActive())
				{
					if(movable!=null){movable.Up(0.1f);} else
					mover.AddMove(Vector3.up *delta  );
					
				}
				
				if(front!=null && front.IsActive())
				{
					
					if(movable!=null){movable.Front(0.1f);} else
					mover.AddMove(Vector3.forward *delta );
					
				}
				if(back!=null && back.IsActive())
				{
					if(movable!=null){movable.Back(0.1f);} else
					mover.AddMove(Vector3.back *delta );
					
				}
			}
			if(validate!=null && validate.IsActive())
			{
				target.transform.position = mover.Validate (withLandMark);
				Reset();
			}
			
			if(cancel!=null && cancel.IsActive())
			{

				Reset();
			}
		}

		if(moving && mover.IsMoving())
			target .transform.position = mover.GetPosition (true,withLandMark);


	}

	public float GetRealVelocity()
	{

		float v =velocity;
		Body b = Body.Instance;
		if(b !=null && b.IsInitialize())
		{
			v*= b.GetDistanceRatioBetween(BodyPart.HandLeft, BodyPart.HandRight, BodyMajorBone.Arm);
		}
		return v;
	}

	public void Reset(){
	
		if (mover.IsMoving()) 
		{
			
			target.transform.position = mover.Cancel ();
		}
		
		moving=false;
		mover.Reset ();
		movable = null;
		target = null;
		cursorOne.SetCursorToStopUsing (false);
		cursorTwo.SetCursorToStopUsing (false);
		
		cursorOne.SetCursorDisplayTo (false);
		cursorTwo.SetCursorDisplayTo (false);
	}



	protected override void OnDoubleSelection (GameObject selected)
	{

			if(target!=null || movable!=null)
			{
				Reset();
			}
			movable = selected.GetComponent (typeof(IMovable)) as IMovable;;
			this.target = selected;
			moving=true;
		mover.StartMove (target.transform, follow.transform, withLandMark);
		cursorOne.SetCursorToStopUsing (true);
		cursorTwo.SetCursorToStopUsing (true);
		
		cursorOne.SetCursorDisplayTo (true);
		cursorTwo.SetCursorDisplayTo (true);
		
	}

	protected override void OnTransferSelection (GameObject cursorOneSelection, GameObject cursorTwoSelection)
	{
		//throw new System.NotImplementedException ();
	}

}
