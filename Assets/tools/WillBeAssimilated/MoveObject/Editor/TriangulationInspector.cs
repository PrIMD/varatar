﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Triangulation))]
public class TriangulationInspector : Editor {


	public override void OnInspectorGUI ()
	{
		Triangulation tri = (Triangulation)target;
		base.OnInspectorGUI ();

		float compareDistance = 1;
		if(tri.compareEnd!=null && tri.compareStart!=null )
			compareDistance= Vector3.Distance (tri.compareEnd.position, tri.compareStart.position);
		if (compareDistance == 0)
						compareDistance = 1;


		if (tri.a != null && tri.b != null && tri.c != null && tri.point!=null ) {
			EditorGUILayout.LabelField (tri.a.name + " :  " + Vector3.Distance(tri.a.position, tri.point.position)/compareDistance);
			EditorGUILayout.LabelField (tri.b.name + " :  " + Vector3.Distance(tri.b.position, tri.point.position)/compareDistance);
			EditorGUILayout.LabelField (tri.c.name + " :  " + Vector3.Distance(tri.c.position, tri.point.position)/compareDistance);

		}
	}
}
