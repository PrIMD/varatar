﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	public Transform target;
	public Transform landMark;
	public bool withLandMarkFollow;
	public float landMarkMultiplicator=3f;

	public Vector3 targetInitialPosition;
	public Vector3 landMarkInitialPosition;

	public Vector3 moveCount;

	public void StartMove(Transform target, Transform landMark , bool withLandMark = true)
	{
		Reset ();
		this.target = target;
		this.landMark = landMark;

		targetInitialPosition = target.position;
		landMarkInitialPosition = landMark.position;
	}


	public bool IsMoving(){
		return target != null && landMark != null;
	}
	public Vector3 Validate(bool withLandMark)
	{	Vector3 result = GetPosition (true, withLandMark);
		Reset ();
		return result;
	}
	
	public Vector3 Cancel()
	{
		Vector3 startPosition = targetInitialPosition;
		Reset ();
		return startPosition;
	}


	public void Reset()
	{
		target = null;
		landMark = null;
		targetInitialPosition = Vector3.zero;
		landMarkInitialPosition = Vector3.zero;
		moveCount = Vector3.zero;
	}

	/**return at how many distance of the land mark the move is*/
	public Vector3 GetLandMarkMove(){
		return (landMark.position - landMarkInitialPosition  )*landMarkMultiplicator;
	}
	/**Retrun the total of moved asked by the user*/
	public Vector3 GetMovedRequested(){
		return moveCount;
	}
	/**Return the total of the moves asked since the start recording*/
	public Vector3 GetMoved(bool withMoved=true, bool withLandMark= false){
		if (withMoved && withLandMark)
			return GetMovedRequested () + GetLandMarkMove ();
		if (withMoved)
			return GetMovedRequested ();
		if (withLandMark)
			return GetLandMarkMove ();
		return Vector3.zero;
	}
	/**Return the finals position of the target*/
	public Vector3 GetPosition(bool withMoved=true, bool withLandMark= true){
		return targetInitialPosition + GetMoved (withMoved, withLandMark) ;
	}

	public void AddMove(Vector3 direciton)
	{
		moveCount += direciton;

	}

}
