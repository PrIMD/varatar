﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;
public class BodyMoverV2 : TwinCursorsBehavior {
	

	public IMovable  movable;
	public IRotable rotable;
	public IScalable scalable;

	public BodyPosition left;
	public BodyPosition up;
	public BodyPosition right;
	public BodyPosition down;
	public BodyPosition front;
	public BodyPosition back;
	public Space moveRelativeTo = Space.World;
	public Transform moveRelatToObj ;
	
	public BodyPosition rotateLeft;
	public BodyPosition rotateRight;
	public bool inverseLeftRight;
	public BodyPosition rotateHorizontalLeft;
	public BodyPosition rotateHorizontalRight;
	public bool inverseHorizontal;
	public Space rotationRelativeTo = Space.Self;
	public Transform rotationRelatToObj ;
	
	public BodyPosition scaleMore;
	public BodyPosition scaleLess;
	public float minimumScale=0.2f;
	//public Transform scaleComparator;

//	public Space scaleRelativeTo = Space.Self;


	public BodyPosition cancel;
	public BodyPosition validate;


	
	public bool moving;
	public float refresh =0.1f;
	public float velocity =2f;
	public float realVelocity =2f;
	



	private float lastTime;
	void Update () {
		if (movable == null || ! moving)
			return;
		
		
		
		
		realVelocity = GetRealVelocity();
		if(realVelocity>0.2f){
			float delta = Time.deltaTime* realVelocity*100;
			if(movable!=null){
				if(left!=null && left.IsActive())
				{
					movable.Left(delta);
				}
				if(right!=null && right.IsActive())
				{
					movable.Right(delta);	
				}
				if(down!=null && down.IsActive())
				{	
					movable.Down(delta);
				}
				if(up!=null && up.IsActive())
				{
					movable.Up(delta);	
				}
				if(front!=null && front.IsActive())
				{
					movable.Front(delta);	
				}
				if(back!=null && back.IsActive())
				{
					movable.Back(delta);
					
				}
			}
			if(rotable!=null)
			{
				float inverseLR = inverseLeftRight?-1:1;
				float inverseH = inverseHorizontal?-1:1;
				if(rotateLeft!=null && rotateLeft.IsActive())
				{
					rotable.RotateAroundZ(-delta*inverseLR);
				}
				
				if(rotateRight!=null && rotateRight.IsActive())
				{
					rotable.RotateAroundZ(delta*inverseLR);
				}
				
				if(rotateHorizontalLeft !=null && rotateHorizontalLeft.IsActive())
				{
					rotable.RotateAroundY(-delta*inverseH);
				}
				
				if(rotateHorizontalRight!=null && rotateHorizontalRight.IsActive())
				{
					rotable.RotateAroundY(delta*inverseH);
				}


			}
			if(scalable!=null){
				Vector3 vs = Vector3.one*delta/10f;

				if(scaleMore!=null  && scaleMore.IsActive() )
				{
					scalable.ScaleAdd(vs);
				}
				
				else if(scaleLess!=null && scaleLess.IsActive())
				{

					scalable.ScaleAdd(-vs);

				}
				Vector3 actualScal = this.transform.localScale;
				if(actualScal.x<minimumScale || actualScal.y<minimumScale || actualScal.z<minimumScale  )
				{
					if(actualScal.x<minimumScale) actualScal.x = minimumScale;
					if(actualScal.y<minimumScale) actualScal.y = minimumScale;
					if(actualScal.z<minimumScale) actualScal.z = minimumScale;

					scalable.ScaleTo(actualScal);
				}

			}
		}
		if(validate!=null && validate.IsActive())
		{
			if(movable!=null)
			movable.Validate ();
			
			if(rotable!=null)
			rotable.Validate();
			
			if(scalable!=null)
				scalable.Validate();
			Reset();
		}
		
		if(cancel!=null && cancel.IsActive())
		{
			if(movable!=null)
				movable.Cancel();
			if(rotable!=null)
				rotable.Cancel();
			if(scalable!=null)
				scalable.Cancel();
			Reset();
		}
		
		
	}
	
	public float GetRealVelocity()
	{
		
		float v =velocity;
		Body b = Body.Instance;
		if(b !=null && b.IsInitialize())
		{
			v*= b.GetDistanceRatioBetween(BodyPart.HandLeft, BodyPart.HandRight, BodyMajorBone.Arm);
		}
		return v;
	}
	
	public void Reset(){
		
		
		if(movable!=null)
			movable.SetRelative(null);
		if(rotable!=null)
			rotable.SetRelative(null);
		if(scalable!=null)
			scalable.SetRelative(null);

		moving=false;
		movable = null;

		cursorOne.SetCursorToStopUsing (false);
		cursorTwo.SetCursorToStopUsing (false);

	}
	
	
	
	protected override void OnDoubleSelection (GameObject selected)
	{
		
		IMovable movable = selected.GetComponent (typeof(IMovable)) as IMovable;
		IRotable rotate = selected.GetComponent (typeof(IRotable)) as IRotable;
		IScalable scale= selected.GetComponent (typeof(IScalable)) as IScalable;

		if (movable != null || rotate!=null || scale!=null ) {
			if ( this.movable != null || this.rotable!=null || this.scalable!=null) {
				Reset ();
			}
			this.movable= movable;
			this.rotable=rotate;
			this.scalable = scale;
			moving = true;
			
			if(this.movable!=null){
				this.movable.StartMoving();
				this.movable.SetRelative(moveRelativeTo);}
			if(this.rotable!=null){
				this.rotable.StartRotating();
				this.rotable.SetRelative(rotationRelativeTo);}
			if(this.scalable!=null){
				this.scalable.StartScaling();
			}


//			Body body = Body.Instance;
//			if(body!=null)
//			{
//				GameObject root = Body.Instance.Get(BodyPart.Root);
//				if(root!=null){
//					if(movable!=null)
//						movable.SetRelative(root.transform);
//					if(rotable!=null)
//						rotable.SetRelative(root.transform);
//				}
//			}

			
			cursorOne.SetCursorToStopUsing (true);
			cursorTwo.SetCursorToStopUsing (true);
			
		}
	}
	
	protected override void OnTransferSelection (GameObject cursorOneSelection, GameObject cursorTwoSelection)
	{
		//throw new System.NotImplementedException ();
	}
	
}
