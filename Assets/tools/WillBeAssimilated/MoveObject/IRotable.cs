using UnityEngine;
public interface IRotable {

	//Ask to rotate the object  in that direction
	void Rotate ( Vector3 direction  );
	//Askt to rotate at this rotation
	void RotateTo (Quaternion rotation);
	void RotateTo (Vector3 eulerRotation);
	
	// Ask to rotate of x pourcentage depending on what is the object that inherit
	void RotateAroundX (float percentage);
	void RotateAroundY (float percentage);
	void RotateAroundZ (float percentage);

	//Ask to activate code corresponding to rotate in this direction
	void Left ();
	void Right ();
	void Up ();
	void Down ();
	void HorizontalLeft();
	void HorizontalRight();
	
	//To be able to move the user must say it before asking to move  in aim to be able to cancel the move
	bool StartRotating();
	// Ask to be permanent move (reset data)
	Quaternion Validate();
	// Ask to cancel the move and to give the old position
	Quaternion Cancel();
	
	// Give a clue to the inheritant of about what it have to move
	void SetRelative(Space relativeTo);
	void SetRelative(Transform relativeTo);
	//void SetMoveRelativity(Space relativeTo); 
	Quaternion GetInitialRotation();

}