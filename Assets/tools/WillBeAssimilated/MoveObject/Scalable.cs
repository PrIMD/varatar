﻿using UnityEngine;
using System.Collections;

public abstract class Scalable : MonoBehaviour,  IScalable {

	public abstract void ScaleAdd (Vector3 scale);

	public virtual void ScaleTo (Vector3 scale){}
	public virtual void Horizontal (float percentage){}
	public virtual  void Vertical (float percentage){}
	public virtual  void Horizontal (){}
	public virtual  void Vertical (){}
	public virtual  void Deepness (float percentage){}
	public virtual void Deepness (){}
	public abstract  bool StartScaling ();
	public abstract  Vector3 Validate ();
	public abstract  Vector3 Cancel ();
	public virtual  void SetRelative (Transform relativeTo){}
	public abstract  Vector3 GetInitialScale ();
}
