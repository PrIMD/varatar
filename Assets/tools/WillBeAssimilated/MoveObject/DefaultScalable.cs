﻿using UnityEngine;
using System.Collections;
using System;
public class DefaultScalable : Scalable {

	public Vector3 initialScale;
	public Transform compareTo;

	public override void ScaleAdd (Vector3 scale)
	{
		if (compareTo == null) {
		
				this.transform.localScale += scale;


		} else
		{
			//throw new NotImplementedException();
		}

	}

	public override void ScaleTo (Vector3 scale)
	{	
			this.transform.localScale = scale;

	}

	public override void Horizontal (float percentage)
	{
		Vector3 v = new Vector3 (percentage, 0f, 0f);
		ScaleAdd( v );
	}

	public override void Vertical (float percentage)
	{
		
		Vector3 v = new Vector3 (0f,percentage,  0f);
		ScaleAdd( v );
	}

	
	public override void Deepness (float percentage)
	{
		
		Vector3 v = new Vector3 (0f,0f,percentage);
		ScaleAdd( v );
	}


	public override bool StartScaling ()
	{
		bool isOverriding = initialScale != Vector3.zero ; 
		initialScale = this.transform.localScale;
		return isOverriding;
	}

	public override Vector3 Validate ()
	{
		initialScale = Vector3.zero;
		return transform.localScale;
	}

	public override Vector3 Cancel ()
	{
		this.transform.localScale = initialScale;
		Vector3 initialScaleTMP = initialScale;
		initialScale = Vector3.zero;
		return initialScaleTMP;
	}



	public override void SetRelative (Transform relativeTo)
	{
		base.SetRelative (relativeTo);
	}

	public override Vector3 GetInitialScale ()
	{
		return initialScale;
	}
}
