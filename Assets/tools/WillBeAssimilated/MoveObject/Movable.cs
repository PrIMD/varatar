﻿using UnityEngine;
using System.Collections;

public abstract class Movable : MonoBehaviour,IMovable {


	public abstract void Move (Vector3 direction);
	public virtual void MoveAt (Vector3 position)
	{}
	public virtual  void Left (float percentage)
	{}
	public virtual  void Right (float percentage)
	{}
	public virtual  void Up (float percentage)
	{}
	public virtual  void Down (float percentage)
	{}
	public virtual  void Front (float percentage)
	{}
	public virtual  void Back (float percentage)
	{}


	public virtual void SetRelative (Space relativeTo)
	{}
	public virtual void SetRelative (Transform relativeTo)
	{}

	public abstract Vector3 GetInitialPosition ();

	/*Return true if it was already moving and erase the old data*/
	public abstract bool StartMoving ();

	public abstract Vector3 Validate ();
	public abstract Vector3 Cancel ();


	public virtual void Left ()
	{}

	public virtual  void Right ()
	{}

	public virtual  void Up ()
	{}

	public virtual  void Down ()
	{}

	public virtual  void Front ()
	{}

	public virtual void Back ()
	{}

}
