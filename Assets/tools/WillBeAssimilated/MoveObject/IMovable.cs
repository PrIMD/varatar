using UnityEngine;
public interface IMovable {


	//Ask to move the object  in that direction
	void Move ( Vector3 direction  );
	//Askt to move at this position
	void MoveAt (Vector3 position);

	// Ask to move of x pourcentage depending on what is the object that inherit
	void Left (float percentage);
	void Right (float percentage);
	void Up (float percentage);
	void Down (float percentage);
	void Front (float percentage);
	void Back (float percentage);

	//Ask to activate code corresponding to move in this direction
	void Left ();
	void Right ();
	void Up ();
	void Down ();
	void Front ();
	void Back ();

	//To be able to move the user must say it before asking to move  in aim to be able to cancel the move
	bool StartMoving();
	// Ask to be permanent move (reset data)
	Vector3 Validate();
	// Ask to cancel the move and to give the old position
	Vector3 Cancel();

	// Give a clue to the inheritant of about what it have to move
	void SetRelative(Space relativeTo);
	void SetRelative(Transform relativeTo);
	//void SetMoveRelativity(Space relativeTo); 
	Vector3 GetInitialPosition();
}