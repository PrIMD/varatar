﻿using UnityEngine;
using System.Collections;

public class WolfenFollowCam : MonoBehaviour {

	public Transform playerView ;
	public bool linkCamIfNull=true;
	public bool freezeVerticalRotation =true;

	public Vector3 rotateAdjustement = new Vector3 (90, 90, 90) ;

	public void Start()
	{
	}

	void Update () {
		if (playerView != null) {
			Vector3 playerPtAdjusted = playerView.position;
			if(freezeVerticalRotation)
			playerPtAdjusted.y = this.transform.position.y; 


			transform.LookAt(playerPtAdjusted);
			transform.Rotate(rotateAdjustement);
		}
		else{
			GameObject gamo =null ;
			if(linkCamIfNull)
			{
				if(Camera.main!=null)
				gamo= Camera.main.gameObject;
			}

			if(gamo==null){
				gamo = GameObject.FindWithTag("Player");
			
			}
			if(gamo==null)
			{
				Debug.Log("Looking target is not define : "+this.gameObject);
			}
			else 
				playerView = gamo.transform;
		}

	
	}
}
