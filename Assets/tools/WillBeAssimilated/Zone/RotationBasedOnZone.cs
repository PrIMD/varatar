﻿using UnityEngine;
using System.Collections;

public class RotationBasedOnZone : MonoBehaviour , RotationZoneListner{

	public RotationZoneActivator rotationZoneObsever;
	public Transform objToRotate;
	
	public float minRotationDegree=10;
	public float maxRotationDegree=100;

	public bool inverse;

	public void Start(){
		if (!IsValide())
						Destroy (this);
		if (IsValide()) {
		
			rotationZoneObsever.listners.AddListner(this);

		}
	}

	public bool IsValide(){
		return objToRotate != null && rotationZoneObsever != null;
	}

	public void OnRotateLeft (float pourcent, float deltaTime, bool isOutOfZone = false)
	{
		if(IsValide())
		{
			float angle = minRotationDegree+ (maxRotationDegree-minRotationDegree)*pourcent;
			angle*= deltaTime;
			angle*= inverse?-1:1;
			objToRotate.Rotate(0, angle, 0, Space.World);
		}


	}

	public void OnRotateRight (float pourcent, float deltaTime, bool isOutOfZone = false)
	{
		
		if(IsValide())
		{
			float angle = maxRotationDegree-minRotationDegree*pourcent;
			angle*= deltaTime;
			angle*= inverse?-1:1;
			objToRotate.Rotate(0, - angle, 0, Space.World);
		}
	}
	public void OnRotateTop (float pourcent, float deltaTime, bool isOutOfZone = false)
	{
	//	throw new System.NotImplementedException ();
	}

	public void OnRotateDown (float pourcent, float deltaTime, bool isOutOfZone = false)
	{
	//	throw new System.NotImplementedException ();
	}
}
