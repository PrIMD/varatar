﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class RotationZoneActivator : MonoBehaviour {

	public Transform direction;
	public Transform root;
	public Vector3 directionAdjustment;
	public bool isActive;
	public float lastAngle;
	
	public float lastHorizontalAngle;
	public float lastVerticalAngle;


	public bool observeHorizontal = true;
	public float minHorizontalAngle=50;
	public float maxHorizontalAngle=100;
	

	
	public bool observeVertical=false;
	public float minVerticalAngle=50;
	public float maxVerticalAngle=100;

	
	public bool enterLeft;
	public bool enterRight;
	public bool enterTop;
	public bool enterDown;

	public bool isRotating;
	
	public bool withEndAutoRedirection;
	public float timeToRedirection = 1.2f;

	public bool isOutOfVerticalZone;
	public bool isOutOfHorizontalZone;
	public bool isReactingWhenOut;

	public bool withDebugDraw=true;

	public bool withListner=true;
	public readonly Listner  listners;
	public bool withEvents = true;
	public readonly Events  events;

	
	public bool withSimulationData;
	public Vector3 simulationData;


	public void SetActive(bool onOff)
	{
		isActive = onOff;
	}
	public bool IsActive(){
		return isActive;
	}

//	private float startRedirection;
	public RotationZoneActivator()
	{
		listners = new Listner ();
		events = new Events (this);
	}

	protected void Start()
	{
		bool Ok = root != null && direction != null; 
		if (!Ok)
			Destroy (this);
		if (Ok) {
				
		}
}


	protected void Update()
	{

		if(isActive && (withListner || withEvents))
		{
			CheckData();
		}

	}

	public bool IsRegularyUpdated(){return withListner || withEvents;}
	public void CheckData()
	{
//		float time = GetTime();
		
		Vector3 position = direction.position;
		

		//The root has a point that I will use to rotate the direction
		Vector3 rootForward = Utility.BasedOnRotation.GetForwardPoint (Vector3.zero, root.rotation,10);
		//I rotate the point of the root to give the user the posibility to adjuste the root direction à it will
		//Plus the root is based on on the center of a cartesian map
		Vector3 rootForwardAdjusted = Utility.BasedOnPosition.RotateAroundPoint (rootForward, Vector3.zero, Quaternion.Euler (directionAdjustment));
		//A take the root direction

		Quaternion rootDirectionAdjusted = Quaternion.LookRotation (rootForwardAdjusted );


		//Direction point and relocated on the cartesian axe at zero
		Vector3 directionForward = Utility.BasedOnRotation.GetForwardPoint (Vector3.zero, direction.rotation,10);

		Vector3 horizontalDirection = Utility.BasedOnPosition.Relocated (directionForward, Vector3.zero, rootDirectionAdjusted, false, true, false);
		Vector3 verticalDirection = Utility.BasedOnPosition.Relocated (directionForward, Vector3.zero, rootDirectionAdjusted, true, false, false);

	//	Vector3 horizontalDirectionWithRotation = Utility.BasedOnPosition.RotateAroundPoint (horizontalDirection, Vector3.zero, (rootDirectionAdjusted));
	//	Vector3 verticalDirectionWithRotation = Utility.BasedOnPosition.RotateAroundPoint (verticalDirection, Vector3.zero, (rootDirectionAdjusted));
		
		bool isAtLeft = horizontalDirection.x 	< 0f;
		bool isAtRight = horizontalDirection.x	>0f;
		bool isAtTop = verticalDirection.y 		> 0f;
		bool isAtDown = verticalDirection .y	<0f;

		
		if (withDebugDraw) {
			
			
			Debug.DrawLine (position, position+rootForwardAdjusted, Color.cyan);	
			Debug.DrawLine (position, position+new Vector3(0f,0f,10f), Color.blue);	
			Debug.DrawLine (position, position+horizontalDirection, Color.green);
			Debug.DrawLine (position, position+verticalDirection, Color.yellow);
			
			//Debug.DrawLine (position, position+horizontalDirectionWithRotation, Color.white);
			//Debug.DrawLine (position, position+verticalDirectionWithRotation, Color.grey);

				//		if(isAtLeft)
				//			Debug.Log("Left");
				//		if(isAtRight)
				//			Debug.Log("Right");

		}


		
		float horizontalAngle = 0f;
		if(observeHorizontal) horizontalAngle = Utility.BasedOnPosition.GetAngleBetween (horizontalDirection, Vector3.zero ,Vector3.forward);
		float verticalAngle = 0f;
		if(observeVertical)  verticalAngle = Utility.BasedOnPosition.GetAngleBetween (verticalDirection, Vector3.zero ,Vector3.forward);
		
		
		lastHorizontalAngle = horizontalAngle;
		lastVerticalAngle 	= verticalAngle;

		if (horizontalAngle > minHorizontalAngle || verticalAngle > minVerticalAngle ) {
			
			
			// notify that the view is in the rotation zone and start rotating
			if(!isRotating)
			{	
//				Debug.Log("START ------------->");
				events.NotifyStartRotating(GetTime());
				isRotating = true;
			}
			if (observeHorizontal && horizontalAngle > minHorizontalAngle){
				
				float pourcent = (Mathf.Abs(horizontalAngle)-minHorizontalAngle) / (maxHorizontalAngle -minHorizontalAngle);
				if(pourcent>1f){ 
					pourcent=1f;
					isOutOfHorizontalZone =true;
				}
				else isOutOfHorizontalZone =false;
				

				if(isAtLeft)
				{
				//	Debug.Log("Left");
					if(! isOutOfHorizontalZone || (isOutOfHorizontalZone && isReactingWhenOut))
					listners.OnLeft(pourcent, Time.deltaTime,isOutOfHorizontalZone);
				}
				if(isAtRight)
				{
					
					//	Debug.Log("Right");
					if(! isOutOfHorizontalZone || (isOutOfHorizontalZone && isReactingWhenOut))
						listners.OnRight(pourcent, Time.deltaTime,isOutOfHorizontalZone);
				}
				
				///notify left and handle
				if(enterLeft==false && isAtLeft)
				{
					events.NotifyLeft(GetTime());
					enterLeft=true;
				}
				if(isAtLeft==false){enterLeft=false;}
				
				///notify right and handle
				if(enterRight==false && isAtRight)
				{
					events.NotifyRight(GetTime());
					enterRight=true;
				}
				if(isAtRight==false){enterRight=false;}
				
			}
			
			
			if (observeVertical && verticalAngle > minVerticalAngle){
				
				float pourcent = (Mathf.Abs(verticalAngle)-minVerticalAngle) / (maxVerticalAngle -minVerticalAngle);
				if(pourcent>1f){ 
					pourcent=1f;
					isOutOfVerticalZone =true;
				}
				else isOutOfVerticalZone =false;
				

				
				if(isAtTop  )
				{
				//	Debug.Log("Top");
					if(! isOutOfVerticalZone || (isOutOfVerticalZone && isReactingWhenOut))
						listners.OnTop(pourcent, Time.deltaTime, isOutOfVerticalZone);
				}
				if(isAtDown)
				{
					
				//	Debug.Log("Down");
					if(! isOutOfVerticalZone || (isOutOfVerticalZone && isReactingWhenOut))
						listners.OnDown(pourcent, Time.deltaTime, isOutOfVerticalZone);
				}
				
				///notify top and handle
				if(enterTop==false && isAtTop)
				{
					events.NotifyTop(GetTime());
					enterTop=true;
				}
				if(isAtTop==false){enterTop=false;}
				
				///notify down and handle
				if(enterDown==false && isAtDown)
				{
					events.NotifyDown(GetTime());
					enterDown=true;
				}
				if(isAtDown==false){enterDown=false;}
				
			}
			
			
			
		} else{
			
			isOutOfVerticalZone =false;
			isOutOfHorizontalZone =false;
			enterDown=false;
			enterTop=false;
			enterRight=false;
			enterLeft=false;
			// notify that the view is in the good zone and stop rotating
			if(isRotating)
			{	
				//Debug.Log("<-------------------  END");
				events.NotifyEndRotating(GetTime());
				isRotating = false;
			}
//			startRedirection = time;

		}

/*
		if (!isRotating && withEndAutoRedirection && timeToRedirection!=0f) {
			Debug.Log ("A");
			float timer = time-startRedirection;
			if(timer<timeToRedirection)
			{
				Debug.Log("Redirection: "+directionWithAdj);
				rootDirectionRef.rotation =  Quaternion.Slerp(rootDirectionRef.rotation,directionWithAdj,timer/timeToRedirection);
				
				
				
			}
		}
*/

	}

	public float GetTime(){return Time.timeSinceLevelLoad;}

	public class Listner
	{

		public List<RotationZoneListner> listners = new List<RotationZoneListner>();

		public void AddListner(RotationZoneListner listner){
			if(listner!=null) listners.Add (listner);		
		}

		public void Clear(){
			listners.Clear ();
		}

		public void OnLeft(float pourcent, float deltaTime, bool isOutOfZone=false){
			foreach (RotationZoneListner l in listners) {
				l.OnRotateLeft(pourcent, deltaTime,isOutOfZone);			
			}
		}
		
		public void OnRight(float pourcent, float deltaTime, bool isOutOfZone=false){
			foreach (RotationZoneListner l in listners) {
				l.OnRotateRight(pourcent, deltaTime,isOutOfZone);			
			}
		}
		
		public void OnTop(float pourcent, float deltaTime, bool isOutOfZone=false){
			foreach (RotationZoneListner l in listners) {
				l.OnRotateTop(pourcent, deltaTime,isOutOfZone);			
			}
		}
		
		public void OnDown(float pourcent, float deltaTime, bool isOutOfZone=false){
			foreach (RotationZoneListner l in listners) {
				l.OnRotateDown(pourcent, deltaTime,isOutOfZone);			
			}
		}


		
	}

	public class Events
	{


		public EventHandler<StartRotatingArgs> onStartRotation;
		public EventHandler<EndRotationArgs> onEndRotation;

		public EventHandler<StartRotatingLeftArgs> onLeft;
		public EventHandler<StartRotatingRightArgs> onRight;
		public EventHandler<StartRotatingTopArgs> onTop;
		public EventHandler<StartRotatingDownArgs> onDown;


		RotationZoneActivator parent;

		public Events (RotationZoneActivator parent)
		{
			this.parent = parent;
		}

		
		public void NotifyStartRotating(float time){
			if (onStartRotation!=null) {
				onStartRotation(parent, new StartRotatingArgs(time, parent));
			}
		}
		
		public void NotifyEndRotating(float time){
			if (onEndRotation!=null) {
				onEndRotation(parent, new EndRotationArgs(time, parent));
			}
			
		}


		public void NotifyLeft(float time){
			if (onLeft!=null) {
				onLeft(parent, new StartRotatingLeftArgs(time, parent));
			}
			
		}

		public void NotifyRight(float time){
			if (onRight!=null) {
				onRight(parent, new StartRotatingRightArgs(time, parent));
			}

		}
		
		public void NotifyTop(float time){
			if (onTop!=null) {
				onTop(parent, new StartRotatingTopArgs(time, parent));
			}

		}
		
		public void NotifyDown(float time){
			if (onDown!=null) {
				onDown(parent, new StartRotatingDownArgs(time, parent));
			}

		}


	}


}
public interface RotationZoneListner
{
	void OnRotateLeft (float pourcent, float deltaTime, bool isOutOfZone=false);
	void OnRotateRight (float pourcent, float deltaTime, bool isOutOfZone=false);
	void OnRotateTop (float pourcent, float deltaTime, bool isOutOfZone=false);
	void OnRotateDown (float pourcent, float deltaTime, bool isOutOfZone=false);
}
public class RotationZoneEventArgs  : EventArgs
{
	public float when;
	public RotationZoneActivator rotationZone;

	public RotationZoneEventArgs (float when, RotationZoneActivator rotationZone)
	{
		this.when = when;
		this.rotationZone = rotationZone;
	}
	

}

public class StartRotatingArgs : RotationZoneEventArgs
{
	public StartRotatingArgs (float when, RotationZoneActivator rotationZone):  base(when, rotationZone){}
}

public class EndRotationArgs : RotationZoneEventArgs
{
	public EndRotationArgs (float when, RotationZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartRotatingLeftArgs : RotationZoneEventArgs
{
	public StartRotatingLeftArgs (float when, RotationZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartRotatingRightArgs : RotationZoneEventArgs
{
	public StartRotatingRightArgs (float when, RotationZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartRotatingTopArgs : RotationZoneEventArgs
{
	public StartRotatingTopArgs (float when, RotationZoneActivator rotationZone):  base(when, rotationZone){}}

public class StartRotatingDownArgs : RotationZoneEventArgs
{
	public StartRotatingDownArgs (float when, RotationZoneActivator rotationZone):  base(when, rotationZone){}}
