﻿using UnityEngine;
using System.Collections;

public  class PlayerMove: MonoBehaviour {


	public Transform player;
	public CharacterController playerCharactMove;

	private bool isFlying;
	public float jumpForce;

	
	public float minSpeed = 2;
	public float maxSpeed = 12;

	
	public float minRotationDegree=10;
	public float maxRotationDegree=100;
	
	
	public bool inverseHorizontalRotation;
	public bool inverseVerticalRotation;
	
	public float lookForwardVelocity=1f;
	public float rotationVelocity=2.5f;
	public float rotationOutVelocity=2.5f;

	public void Start(){

		if (player != null)
		playerCharactMove = player.GetComponent<CharacterController> () as CharacterController;
	}

	public bool IsValide(){
		return player != null ;
	}

	
	public virtual void MoveForward (float pourcent, float deltaTime){
	
		MoveFront (pourcent, deltaTime);
	}

	public virtual void MoveLeft(float pourcent, float deltaTime)
	{
		
		if (IsValide()) {
			float speed =  minSpeed+ (maxSpeed-minSpeed) *pourcent;
			
			Move (Vector3.left, deltaTime, speed);
		}
	}

	
	public virtual  void MoveRight(float pourcent, float deltaTime)
	{
		
		if (IsValide()) {
			float speed =  minSpeed+ (maxSpeed-minSpeed) *pourcent;
			
			Move (Vector3.right, deltaTime, speed);
		}
	}
	
	public virtual void MoveFront(float pourcent, float deltaTime)
	{
		if (IsValide()) {
			float speed =  minSpeed+ (maxSpeed-minSpeed) *pourcent;
			
			Move (Vector3.forward, deltaTime, speed);
		}
	}

	protected void Move(Vector3 direction, float deltaTime, float speed){
		if(playerCharactMove!=null)
		{
			Vector3 move = direction*(deltaTime*speed);
			move = player.TransformDirection(move);
			playerCharactMove.Move(move);
		}			else
			player.Translate(direction *(deltaTime*speed));


	}
	public virtual void MoveBack(float pourcent, float deltaTime)
	{
		if (IsValide()) {
			float speed =  minSpeed +(maxSpeed-minSpeed) * pourcent;

			Move (Vector3.back, deltaTime, speed);
		}
	}


	public virtual  void MoveUp(float pourcent, float deltaTime)
	{
		if (IsValide()) {
			float speed =  minSpeed+ (maxSpeed-minSpeed) *pourcent;
			
			Move (Vector3.up, deltaTime, speed);
		}

	}
	
	public virtual  void MoveDown(float pourcent, float deltaTime)
	{
		if (IsValide()) {
			float speed =  minSpeed+ (maxSpeed-minSpeed) *pourcent;
			
			Move (Vector3.down, deltaTime, speed);
		}
		
	}
	
	public virtual  void Jump(float pourcent, float deltaTime)
	{
		if(IsValide())
		if (transform.rigidbody != null) {
			Vector3 velocity = transform.rigidbody.velocity;
			velocity.y= 0f;
			player.rigidbody.velocity = velocity;
			player.rigidbody.AddForce (Vector3.up*jumpForce, ForceMode.Impulse);
		}
	}


	public virtual  void RotateLeft(float pourcent, float deltaTime, bool isOutOfZone)
	{
		if(IsValide())
		{
			float angle = minRotationDegree+( maxRotationDegree-minRotationDegree)*pourcent;
			angle*= deltaTime ;
			angle*= isOutOfZone?rotationOutVelocity:1f;
			angle*= -1 *(inverseHorizontalRotation?-1:1);
			//Debug.Log("R L  "+ angle);
			player.Rotate(0, angle, 0, Space.Self);
		}
	}

	public virtual  void RotateRight(float pourcent, float deltaTime, bool isOutOfZone)
	{
		if(IsValide())
		{
			float angle = minRotationDegree+(maxRotationDegree-minRotationDegree)*pourcent;
			angle*= deltaTime;
			angle*= isOutOfZone?rotationOutVelocity:1f;
			angle*= 1 *(inverseHorizontalRotation?-1:1);
			player.Rotate(0, angle, 0, Space.Self);
		}
	}



	public void RotateTo (Vector3 point)
	{		
				if (IsValide ()) {

						Debug.DrawLine(player.position, point);
						point.y = player.position.y;

						point = point + (point-player.position)*1000f;
						Quaternion q = player.rotation;
						q.SetLookRotation(point);
						q= Quaternion.Lerp(player.rotation, q, Time.deltaTime*lookForwardVelocity);
						player.rotation=q;
				}

	}


}
