﻿using UnityEngine;
using System.Collections;

public class InitiatePlayerWhenStart : MonoBehaviour {

	public MoveZoneActivator circleMoveZone;
	public RotationZoneActivator shoulderRotationZone;
	public RotationZoneActivator headRotationZone;
	// Use this for initialization
	void Start () {
		if (circleMoveZone == null) {
						Destroy (this);
						return;
				}
			LevelManager.OnLevelStart+= InitPlayer;		

	}
	private void InitPlayer(object obj, StartLevelEventArgs startArgs)
	{

		if (circleMoveZone != null) {
			circleMoveZone.RootAutoAdjustement ();
			circleMoveZone.isActive=true;		
		}

		if (shoulderRotationZone != null) {
			shoulderRotationZone.isActive=true;		
		}

		if (headRotationZone != null) {
			headRotationZone.isActive=true;		
		}

	}
}
