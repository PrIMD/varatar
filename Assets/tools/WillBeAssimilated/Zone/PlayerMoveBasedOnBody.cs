﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;

public class PlayerMoveBasedOnBody : MonoBehaviour,   RotationZoneListner, MoveZoneListner{


	public PlayerMove player;
	public RotationZoneActivator rotation;
	public MoveZoneActivator move;
	public Cursor view;
	public BodyPosition fly;

	public Movement jump;


	public void Start(){
		if (rotation != null)
			rotation.listners.AddListner (this);

		if (move != null)
			move.listners.AddListner (this);

	}

	protected void Update () {


		if (rotation != null && player!=null && view!=null && ! rotation.isRotating) {
			
			player.RotateTo(view.GetCursorPosition());
		}
		   
	}

	public void OnRotateLeft (float pourcent, float deltaTime, bool isOutOfZone=false)
	{
		if(player !=null)
			player.RotateLeft (pourcent, deltaTime,isOutOfZone);
	}

	public void OnRotateRight (float pourcent, float deltaTime, bool isOutOfZone=false)
	{
		if(player !=null)
			player.RotateRight (pourcent, deltaTime,isOutOfZone);
	}

	public void OnRotateTop (float pourcent, float deltaTime, bool isOutOfZone=false)
	{
		//if (fly != null  && player!=null && fly.IsActive()) {
		//		player.MoveUp (pourcent, deltaTime,isOutOfZone);
		//	}

	}

	public void OnRotateDown (float pourcent, float deltaTime, bool isOutOfZone=false)
	{}


	public void OnMoveLeft (float pourcent, float deltaTime)
	{
		if(player !=null)
			player.MoveLeft (pourcent, deltaTime);
	}

	public void OnMoveRight (float pourcent, float deltaTime)
	{
		if(player !=null)
			player.MoveRight (pourcent, deltaTime);
	}

	public void OnMoveFront (float pourcent, float deltaTime)
	{
		if(player !=null)
			player.MoveFront (pourcent, deltaTime);
	}

	public void OnMoveBack (float pourcent, float deltaTime)
	{	if(player !=null)
			player.MoveBack (pourcent, deltaTime);
	}
}
