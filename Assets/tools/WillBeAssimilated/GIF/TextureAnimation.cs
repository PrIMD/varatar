﻿using UnityEngine;
using System.Collections;

public class TextureAnimation : MonoBehaviour {

	public string purpose="Unammed";
	public Texture2D [] textureAnim;

	
	public Texture2D GetFirst ()
	{
		
		if (textureAnim.Length == 0)
			return null;
		return textureAnim [0];
	}
	
	public Texture2D GetLast ()
	{
		if (textureAnim.Length == 0)
						return null;
		return textureAnim [textureAnim.Length - 1];
	}
	/*0.01 to 1.0 pourcent and return the texture that correspond the best to the anim*/
	public Texture2D GetTexture(float pourcent)
	{
		float pct = Mathf.Clamp(pourcent, 0.0f, 1.0f);
		pct *= 100f;
		float pctValueByImage = 100f / (float)textureAnim.Length;

		int cursor = Mathf.RoundToInt(pct/pctValueByImage) -1;
		cursor =Mathf.Clamp (cursor, 0, textureAnim.Length-1);

		return textureAnim [cursor];

	}

}
