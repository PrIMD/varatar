﻿using UnityEngine;
using System.Collections;

public class TextureToApply : MonoBehaviour {

	public GameObject target;
	public Shader shader;
	public Material material;
	public Texture2D texture;

	// Use this for initialization
	void Start () {
	
		if (target == null|| target.renderer == null ) {
			Destroy(this);
			return;
		}

		if (shader == null) {
			shader = Shader.Find ("Transparent/Bumped Diffuse");
		}
		material = new Material (shader);
		material.mainTexture = texture;

		target.renderer.material = material;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (texture !=null && material.mainTexture != texture) {
			material.mainTexture = texture;


		}
	}
}
