﻿using UnityEngine;
using System.Collections;

public class MovableCube : DefaultMovable {


	public override bool StartMoving ()
	{
		Rigidbody rig = GetComponent<Rigidbody> () as Rigidbody;
		if (rig != null) rig.useGravity = false;
		return base.StartMoving ();
	}

	public override Vector3 Validate ()
	{

		Rigidbody rig = GetComponent<Rigidbody> () as Rigidbody;
		if (rig != null) rig.useGravity = true;
		return base.Validate ();

	}

	public override Vector3 Cancel ()
	{
		Rigidbody rig = GetComponent<Rigidbody> () as Rigidbody;
		if (rig != null) rig.useGravity = true;
		return base.Validate ();

	}
}
