﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;

[CustomEditor(typeof(MovementsAndPositions))]
public class MovAndPosInspector : Editor {

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		
		string [] movements = Movement.GetKeywordAvailable ();
		string [] positions = BodyPosition.GetKeywordAvailable ();
				
		DisplayThoseString ("Movements", movements);
		DisplayThoseString ("Positions", positions);
		
		

	}

	static void DisplayThoseString (string title, string[] values)
	{
		EditorGUILayout.LabelField (title+":");
		if (values == null || values.Length == 0) {
			EditorGUILayout.LabelField ("None");
		}
		else {
			string keywords = "";
			bool first = true;
			foreach (string s in values) {
				keywords += (first ? " " : ", ") + s;
				first = false;
			}
			EditorGUILayout.LabelField (keywords);
		}
	}
}
