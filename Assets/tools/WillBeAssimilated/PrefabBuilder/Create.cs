﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
public class Create  {

	public static Dictionary<string,ExtensionBuilder> builderByKeyword = new Dictionary<string, ExtensionBuilder> ();
	public static Dictionary<string,List<ExtensionBuilder> > builderByExtension = new Dictionary<string, List<ExtensionBuilder>> ();

	public static string[] GetKeywordAvailable ()
	{
		string [] keys = new string[builderByKeyword.Count]; 
		builderByKeyword.Keys.CopyTo (keys, 0);
		return keys;
	}

	public static string[] GetExtensionAvailable ()
	{
		string [] exts = new string[builderByExtension.Count]; 
		builderByExtension.Keys.CopyTo (exts, 0);
		return exts;
	}

	public static void AddFileBuilder(ExtensionBuilder builder){
		if (builder == null )
						return;
		AddKeyWordBuilder (builder.GetKeywordID (), builder);
		AddExtensionBuilder (builder.GetExtensionsHandled (), builder);

	}

	static void AddKeyWordBuilder (string keyword, ExtensionBuilder builder)
	{
		if (builder==null || keyword == null || keyword.Length <= 0)
						return ;
		builderByKeyword.Remove(keyword);		
		builderByKeyword.Add (keyword, builder);
	}

	static void AddExtensionBuilder (string[] extensions, ExtensionBuilder builder)
	{
		if (builder == null || extensions == null || extensions.Length <= 0)
						return;
		//FOR EACH EXTENSION
		foreach (string extension in extensions) {
			//IF VALIDE
			if ( extension == null || extension.Length <= 0)
				continue;
			// WE ADD THE BUILDER IN THE CORRESPONDING LIST
			List<ExtensionBuilder> buildersForExt;
			if(! builderByExtension.TryGetValue(extension, out buildersForExt) || buildersForExt==null)
			{
				//IF IT DOES NOT EXISTE, WE CREATE IT
				buildersForExt = new List<ExtensionBuilder>();
				builderByExtension.Add (extension,buildersForExt);
			}
				buildersForExt.Add(builder);
			
		}
	}

	public static GameObject File(string path, string withKeyword=""){
		ExtensionBuilder builder = null;
		if (withKeyword != null && withKeyword.Length > 0) {
			builder = GetBuilderBasedOnKeyword(withKeyword);
		}	

		if (builder == null) {
			string ext = Path.GetExtension(path);

			builder = GetBuilderBasedOnExtension(ext);
		}
		if(builder!=null)
		return builder.GetInstanceOf(path);
		return null;
		
	}

	static ExtensionBuilder GetBuilderBasedOnKeyword (string keyword)
	{
		ExtensionBuilder builder = null;
		builderByKeyword.TryGetValue (keyword, out builder);
		return builder;
	}

	static ExtensionBuilder GetBuilderBasedOnExtension (string extension)
	{
		ExtensionBuilder builder = null;
		List<ExtensionBuilder> buildersStored = null;
		builderByExtension.TryGetValue (extension, out buildersStored);
		if (buildersStored != null && buildersStored.Count > 0)
			foreach (ExtensionBuilder eb in buildersStored)
				if (eb != null) {
					builder = eb;
					break;
				}
		return builder;
	}


	/** The class that inherite from this one know the detail of how to build a game object for this type of extension*/
	public abstract class ExtensionBuilderAbst : MonoBehaviour, ExtensionBuilder {
		
		public string keywordID ="";
		public string [] extensionsHandled =new string[]{".txt"};


		public void Start(){
			AddToCreateAsBuilder (keywordID,extensionsHandled);		
		}

		public void AddToCreateAsBuilder(string keywordID, string [] extensions){
		
			bool keywordValide = keywordID!=null && keywordID.Length>0;
			bool extensionValide = extensions!=null && extensions.Length>0;

			if(! extensionValide) Debug.LogWarning("Extension Builder created with no extension handle",this.gameObject);

			if(keywordValide || extensionValide)
			{
				Create.AddFileBuilder(this);
			}
		

		
		}
		public abstract GameObject GetInstanceOf (string path);

		public string [] GetExtensionsHandled(){
			return extensionsHandled;
		}
		public string GetKeywordID(){
						return keywordID;
				}
	}
	
	public interface ExtensionBuilder
	{
		string [] GetExtensionsHandled();
		string  GetKeywordID();
		GameObject GetInstanceOf(string path);
	}

}
