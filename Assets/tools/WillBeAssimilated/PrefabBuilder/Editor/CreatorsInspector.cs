﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Creators))]
public class CreatorsInspector : Editor {

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		
		string [] keywords = Create.GetKeywordAvailable ();
		string [] extensions = Create.GetExtensionAvailable ();
		
		DisplayThoseString ("Builders with ID", keywords);
		DisplayThoseString ("Extensions supported", extensions);
		
		
		
	}
	
	static void DisplayThoseString (string title, string[] values)
	{
		EditorGUILayout.LabelField (title+":");
		if (values == null || values.Length == 0) {
			EditorGUILayout.LabelField ("None");
		}
		else {
			string keywords = "";
			bool first = true;
			foreach (string s in values) {
				keywords += (first ? " " : ", ") + s;
				first = false;
			}
			EditorGUILayout.LabelField (keywords);
		}
	}
}
