﻿using UnityEngine;
using System.Collections;

public class CreateUseTest : MonoBehaviour {
	
	public string pathCS = "C:/BlaBlabla.cs";
	public string pathTXT = "C:/BLOBLO.txt";

	void Update () {
	
		if(Input.GetKeyDown(KeyCode.C))
		{
			
			GameObject obj = Create.File (pathCS);
			Debug.Log("Created CS: "+obj);
		}
		
		if(Input.GetKeyDown(KeyCode.T))
		{
			
			GameObject obj = Create.File (pathTXT);
			
			Debug.Log("Created TXT: "+obj);
		}
	}
}
