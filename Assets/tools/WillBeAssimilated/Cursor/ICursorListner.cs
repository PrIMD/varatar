﻿using UnityEngine;
public interface ICursorListner  {
	
	void OnCursorIsSelecting(float time, GameObject selected);
	void OnCursorIsFocusing(float time, GameObject selected);


}
