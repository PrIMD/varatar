using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Cursor : MonoBehaviour {

	//Cursors
	public static Dictionary <string,Cursor> cursors = new Dictionary<string, Cursor> ();
	public static bool AddCursor(string name, Cursor cursor){
		if (name != null && cursor != null) {
						if (! cursors.ContainsKey (name)){

								cursors.Add (name, cursor);
				return true;}
		}
		return false;
	}
	public static int AddAllSceneCursor()
	{
		Cursor [] curs = GameObject.FindObjectsOfType<Cursor>() as Cursor [];
		int i = 0;
		foreach (Cursor c in curs) {
				if(AddCursor (c.purpose, c))i++;
		}
		return i;
	}
	public static Cursor GetCursorByPurpose(string purpose){
		Cursor cursor=null; 
		cursors.TryGetValue (purpose, out cursor);
		return cursor;
	}

	//Cursor

	public bool isActive= true;
	public string purpose ="";
	public LayerMask layersUse;

	public bool withSmooth=true;
	public float smoothVeclotiy=0.02f;
	private bool withZoneEffect=true;
	public float zoneEffectRadius=5f;
	public bool onlyBehindImpact=false;

	public Transform origine;
	public Transform direction;
	public Vector3 lastCursorPosition;

	public float minDistance=1f;
	public float maxDistance=20f;
	public bool withScrollDistance;
	public float scrollDistance=10f;
	
	public bool isCursorDisplay=true;
	public bool onlyDisplayWhenHit=false;
	public GameObject cursorDisplay;
	public Vector3 cursorDisplayRotAdjust;
	public Vector3 cursorDisplayPosAdjust;
	public Transform cursorLookAt;
	public float cursorStartSize=0.5f;
	public Material defaultMaterial;
	public Material custumMaterial;


	public float focusMinTime =2f;

	public bool drawDebugRay;
	public bool drawLineInGame;
	public LineRenderer line;
	public Transform lineStartPosition;
	public Color lineDefaultColor=Color.green;
	
	public float refresh = 0.05f;
	private float lastTimeRefresh;

	public bool eventsInteraction=true;
	public Cursor.Events events ;


	//Listner
	public bool listnersInteraction;
	public Cursor.Listners listners ;
	
	public float listnerRefresh =0.1f;
	private float lastListenerRefresh;

	//temp variable
	protected Vector3 lastDirection = new Vector3 ();

	public float lastTimeHit;
	public float lastTimeStartHit;


	//user of cursor class request storage variable
	private  bool stopDisplayingCursorAndWait;
	private  bool stopUsingCursorAndWait;
	private 	bool stopNotifyingAndWait;

	private bool isMaxRefreshAsked ;
	private float maxRefreshTime;

    private bool lastHasHitten;


	protected void Awake(){
		
		events = new Cursor.Events(this);
		listners = new Cursor.Listners(this);
	}

	//Code
	protected void Start(){


		if (cursorDisplay == null) {
			cursorDisplay = GameObject.CreatePrimitive(PrimitiveType.Plane);
		

			cursorDisplay.transform.localScale = Vector3.one*cursorStartSize;
			cursorDisplay.name= "Cursor Image";
			cursorDisplay.transform.parent=origine.transform;
			cursorDisplay.renderer.enabled=false;
			cursorDisplay.transform.LookAt (origine);
			cursorDisplay.transform.Rotate(cursorDisplayRotAdjust);
		}
		if(custumMaterial==null)
			cursorDisplay.renderer.material= defaultMaterial;
		else 
			cursorDisplay.renderer.material= custumMaterial;
		
		cursorDisplay.transform.LookAt (origine);

	}
	protected void Update(){
		if (isMaxRefreshAsked && maxRefreshTime>0) {
			maxRefreshTime-=Time.deltaTime;
			if(maxRefreshTime<=0) isMaxRefreshAsked=false;
		}

		if (stopUsingCursorAndWait) {
			if(cursorDisplay!=null)
			cursorDisplay.renderer.enabled=false;
			if(line!=null)
			line.renderer.enabled=false;		
		}

		if (isActive&& IsValide() && !stopUsingCursorAndWait) {

			float t = GetTime();

			if(isMaxRefreshAsked || t-lastTimeRefresh>refresh){
				lastTimeRefresh=t;
			
			SetNewDirection();

			Vector3 pos;
			GameObject hit;
			GameObject [] objInZone;
			bool hasHit = CheckRayCollision(out pos, out  hit, out objInZone,zoneEffectRadius,onlyBehindImpact);
            lastHasHitten = hasHit;
			SetLastPosition(pos);
				//tmp vairable
				if(hasHit)
				{
					lastTimeHit=t;
					if(lastTimeStartHit==0f)
						lastTimeStartHit=t;

				}
				else 
				{
					lastTimeHit=0f;
					lastTimeStartHit=0f;
				}

			//Handle events
			if(!stopNotifyingAndWait){
				if(hasHit && eventsInteraction)
				{
					if(eventsInteraction && events.sendNotificationEnter==false)	
						events.NotifyCurserEnterInCollisionWith(hit, objInZone);
					else if( eventsInteraction && events.sendNotificationEnter &&  events.sendNotificationFocus==false && (GetTime() - events.lastTimeEnter >focusMinTime ) )
						events.NotifyCurserFocusInCollisionWith(hit, objInZone);
				}
				else
				{
					if(eventsInteraction && events.sendNotificationEnter  &&  events.sendNotificationExit==false)
						events.NotifyCurserExitInCollisionWith(events.lastGameObjectEntered, events.lastGameObjectImpacts);

				}

				//Handle Listner
					if(listnersInteraction && t -lastListenerRefresh>listnerRefresh){
						lastListenerRefresh=t;
						if(hasHit)	listners.NotifyInteracting(t,hit);
						else 		listners.NotifyStopInteracting(t);

					}
			}

			//Draw  debug
			if(drawDebugRay)
			{

					Debug.DrawLine(origine.position ,  origine.position + lastDirection*maxDistance  ,		Color.green,refresh);

					Debug.DrawLine(origine.position , lastCursorPosition,			Color.yellow,refresh);
				Debug.DrawLine(origine.position , origine.position+ lastDirection*minDistance,			Color.red,refresh);
			}

			//Draw cursor icon

				//TO verifiy, the cursor alway have to be orianted thorward the player
				if(cursorDisplay!=null){
					if(   cursorLookAt==null){
						cursorDisplay.transform.LookAt (origine);
					}
					else 
					{
						cursorDisplay.transform.LookAt (cursorLookAt);
					}
					
					cursorDisplay.transform.Rotate(cursorDisplayRotAdjust);
					cursorDisplay.transform.Translate(cursorDisplayPosAdjust);
				}

				if(stopDisplayingCursorAndWait){
					cursorDisplay.renderer.enabled=false; 
					line.enabled= false;}
				if(!stopDisplayingCursorAndWait){
					if(cursorDisplay!=null ){
					bool isDisplayable=isCursorDisplay && (!onlyDisplayWhenHit || (onlyDisplayWhenHit&&hasHit));
					cursorDisplay.renderer.enabled= isCursorDisplay;
					cursorDisplay.transform.position= GetNextAsLastPosition(withSmooth);

						if(isDisplayable)
						{
							if(cursorLookAt==null){
								cursorDisplay.transform.LookAt (origine);
							}
							else 
							{
								cursorDisplay.transform.LookAt (cursorLookAt);
							}
							cursorDisplay.transform.Rotate(cursorDisplayRotAdjust);
							cursorDisplay.transform.Translate(cursorDisplayPosAdjust);
						}
					}
					//Draw line
					if(line!=null){
						
						if (drawLineInGame) {
							if(lineStartPosition==null)line.SetPosition(0, direction.position);
							else line.SetPosition(0, lineStartPosition.position);
							line.SetPosition(1, cursorDisplay.transform.position);
						}
						line.enabled= drawLineInGame;

					}
				}
			}
		}
	}

	public Vector3 GetLastDirection (){return lastDirection;}
	public Vector3 GetLastPosition (){return lastCursorPosition;}
	public Vector3 GetOrigine () {
				return origine.position;
		}

	public Vector3 GetCursorDirection(){
		if (lastHasHitten) {
			return (GetLastPosition()-origine.position).normalized;		
		}
		return (direction.position  - origine.position).normalized;
	}
	public Vector3 GetCursorPosition(){
		return cursorDisplay.transform.position;
	}

	protected void SetNewDirection(){
		lastDirection = ((this.direction.position-this.origine.position).normalized);
	}
	protected void SetNewDirection(Vector3 direction){
		lastDirection = direction;
	}
	protected void SetLastPosition(Vector3 newPosition){
			lastCursorPosition = newPosition;
		

	}
	protected Vector3 GetNextAsLastPosition(bool withSmooth){
		if (!withSmooth) {
			return lastCursorPosition;
		} else {
				return cursorDisplay.transform.position - ( cursorDisplay.transform.position- lastCursorPosition)*(refresh* smoothVeclotiy);
		}
	}
	
	protected bool CheckRayCollision(out Vector3 position, out GameObject hit, out GameObject [] objInZone, float distance = 5f, bool onlyBehindImpact=false)
	{
		hit = null;
		objInZone = null;
		RaycastHit rayHit;
		bool hasHit = Physics.Raycast (origine.position, lastDirection, out rayHit, maxDistance,layersUse);//TODO add layer filter later
		if (hasHit) {
			position = rayHit.point;

			//mini distance
			if(Vector3.Distance(origine.position, position)< minDistance)
			{
				position = origine.position + lastDirection*minDistance;
				hasHit=false;
			}
			//Hits
			if(hasHit){
				hit= rayHit.collider.gameObject;
				if(withZoneEffect)
				objInZone= GetGameObjectInZone(position,distance,onlyBehindImpact);

			}

		}  else {

			if(withScrollDistance)
				position = origine.position + lastDirection*scrollDistance;
			else
				position = origine.position + lastDirection*minDistance;
			
		}
		return hasHit;
	}
	protected GameObject  [ ] GetGameObjectInZone( Vector3 zone, float distance, bool onlyBehindImpact = false){
		Vector3 sphereStart = zone;
		if (!onlyBehindImpact)
						sphereStart -= lastDirection * (distance / 2f);
		RaycastHit [] hits = Physics.SphereCastAll ( sphereStart, distance, lastDirection, distance, layersUse);//TODO add layer filter later
		GameObject [] objs = new GameObject[hits.Length];
		for (int i =0; i<hits.Length; i++)
		{
			if (drawDebugRay) {
				Debug.DrawLine( hits[i].point, zone,Color.magenta,refresh);
			}
			objs[i] = hits[i].collider.gameObject;
		}		
		return objs;
	}



	public bool IsValide(){

		return origine!=null && direction!=null && purpose!=null && purpose.Length>0 && listners!=null && events!=null;
	}

	protected float GetTime(){
		return Time.time;}

	//public methode for user

	public float GetPourcentageFocused(){
		return (lastTimeHit - lastTimeStartHit) / focusMinTime;
	}
	public void SetCursorDisplayTo(bool stopDisplay){
		stopDisplayingCursorAndWait = stopDisplay;
	}
	public void SetCursorNotificationTo(bool disable){
		stopNotifyingAndWait = disable;
	}
	public void SetCursorToStopUsing(bool disableCursor){
		stopUsingCursorAndWait = disableCursor;
	}
	public void SetCursorToMaximumPrecision(bool maxRefresh, float timeToMaxRefresh){
		isMaxRefreshAsked = maxRefresh;
		maxRefreshTime = timeToMaxRefresh;
	}



	public GameObject GetSelected()
	{
		if (listnersInteraction && listners != null) {
			return listners.GetSelected();		
		}
		if (eventsInteraction && events != null) {
			return events.GetSelected();		
		}
		return null;
	}

	//Listner

	public class Listners
	{
		private Cursor cursor;
		
		public Listners(Cursor cursor)
		{this.cursor = cursor;}

		private List<ICursorListner> listners = new List<ICursorListner> ();
		
		private GameObject lastSelection;

		private bool hasBeenReset=true;
		private float startInteracting;
		private float lastInteraction;

		public void Reset(){
			hasBeenReset = true;
			lastSelection = null;
			startInteracting = 0;
			lastInteraction = 0;		
		}

		public GameObject GetSelected(){
			return lastSelection;}

		public void AddListner(GameObject listner){
			ICursorListner [] lists = listner.GetComponents (typeof(ICursorListner)) as ICursorListner[]; 
			foreach (ICursorListner l in lists) {
				AddListner(l);		
			}
			
		}
		public void AddListner(ICursorListner listner)
		{
			if (listner != null) {
				listners.Add(listner);		
			}
		}
		
		public void ClearListner(){
			listners.Clear ();
		}


		public void NotifyInteracting(float t, GameObject selected){
			if (hasBeenReset) {
				startInteracting=t;
				hasBeenReset=false;

			}
			if (lastSelection != selected) {
				lastSelection=selected;			
			}

			lastInteraction = t;
			if (lastInteraction - startInteracting < cursor.focusMinTime) {
				NotifyIsSelecting (t, lastSelection);
			} else
				NotifyIsFocusing (t, lastSelection);
				
		}
		public void NotifyStopInteracting(float t){
			if (hasBeenReset == false)
								Reset ();
		}

		private void NotifyIsSelecting (float t, GameObject selected)
		{
			foreach (ICursorListner cl in listners) {
				cl.OnCursorIsSelecting(t,selected);			
			}

		}
		private void NotifyIsFocusing(float t, GameObject selected)
		{
			foreach (ICursorListner cl in listners) {
				cl.OnCursorIsFocusing(t,selected);			
			}				
		}

	}


	//Events

public class Events{
		private Cursor cursor;
		
		public Events(Cursor cursor)
		{
			this.cursor = cursor;

		}
	public event EventHandler <CursorEnterArgs> OnCursorEnterDetected;
	public event EventHandler <CursorFocusArgs> OnCursorFocusedDetected;
	public event EventHandler <CursorExitArgs> OnCursorExitDetected;
	

	public GameObject lastGameObjectEntered;
	public GameObject [] lastGameObjectImpacts;
	public float lastTimeEnter;
	public float lastTimeFocused;
	public float lastTimeExit;

				public bool sendNotificationEnter;
				public bool sendNotificationFocus;
				public bool sendNotificationExit;

	private void Reset(){
		
		lastGameObjectEntered = null;
		lastGameObjectImpacts = null;
		lastTimeEnter = 0f;lastTimeFocused = 0f;lastTimeExit = 0f;
		sendNotificationEnter = false;
		sendNotificationFocus = false;
		sendNotificationExit = false;
	}

		public GameObject GetSelected(){
			return lastGameObjectEntered;}

	public void NotifyCurserEnterInCollisionWith(GameObject selected , GameObject [] inTheImpactZone)
	{
		//if there is not ask of using event, it it worst less to do anything
		if (cursor.eventsInteraction && OnCursorEnterDetected != null && OnCursorExitDetected!=null) {
			lastTimeEnter = cursor.GetTime ();
			lastGameObjectEntered = selected;
			lastGameObjectImpacts = inTheImpactZone;
			sendNotificationEnter = true;
			//Debug.Log ("Enter");	
				OnCursorEnterDetected (cursor, new CursorEnterArgs (cursor, lastTimeEnter, selected, inTheImpactZone));
		}
	}
	
	public void NotifyCurserFocusInCollisionWith(GameObject selected , GameObject [] inTheImpactZone)
	{ 
		
		//if there is not ask of using event, it it worst less to do anything
			if (cursor.eventsInteraction && OnCursorEnterDetected != null && OnCursorExitDetected != null) {
			if(sendNotificationExit==false){
		
				//assertion
				if (lastGameObjectEntered == null) { throw new Exception ("Last Game Object Enter should not be null");	}
				if (lastGameObjectEntered != null && selected != lastGameObjectEntered) { throw new Exception ("Focus selected game object should be same as in the enter notification");	}

				lastTimeFocused = cursor.GetTime ();
				lastGameObjectImpacts = inTheImpactZone;
				sendNotificationFocus = true;
			//	Debug.Log ("Focus");
				if(OnCursorFocusedDetected!=null)
						OnCursorFocusedDetected (cursor , new CursorFocusArgs (cursor, lastTimeEnter, lastTimeFocused, selected, inTheImpactZone));
			}
		}
	}
	
	public void NotifyCurserExitInCollisionWith(GameObject selected , GameObject [] inTheImpactZone)
	{
		//if there is not ask of using event, it it worst less to do anything
			if (cursor.eventsInteraction && OnCursorEnterDetected != null && OnCursorExitDetected != null) {

			//assertion
				if (lastGameObjectEntered == null) { Reset();throw new Exception ("Last Game Object Enter should not be null");	}
			if (lastGameObjectEntered != null && selected != lastGameObjectEntered) { throw new Exception ("Focus selected game object should be same as in the enter notification");	}

				lastTimeExit = cursor.GetTime ();
			sendNotificationExit = true;
				OnCursorExitDetected (cursor, new CursorExitArgs (cursor, lastTimeEnter, lastTimeFocused, lastTimeExit, selected, inTheImpactZone));
			
			//Debug.Log ("Exit");	
			Reset ();
		}
		}
	}

public bool IsHitting()
{
    return lastHasHitten;
}




}

public class CursorArgs : EventArgs
{
	public Cursor cursor;
	public float time; 
	public GameObject selected;
	public GameObject [] inTheImpactZone;

	public CursorArgs(Cursor cursor, float time, GameObject selected , GameObject [] inTheImpactZone){
		this.cursor = cursor;
		this.time = time;
		this.selected = selected;
		this.inTheImpactZone = inTheImpactZone;
	}
}

public class CursorEnterArgs : CursorArgs
{
	public CursorEnterArgs(Cursor cursor, float time, GameObject selected , GameObject [] inTheImpactZoneOfEnter) : base (cursor,time,selected, inTheImpactZoneOfEnter){}
}
public class CursorFocusArgs : CursorArgs
{
	public float timeWhenFocus;
	public CursorFocusArgs(Cursor cursor, float time,float focusTime, GameObject selected , GameObject [] inTheImpactZoneOfFocus) : base (cursor,time,selected, inTheImpactZoneOfFocus)
	{
		timeWhenFocus = focusTime;
	}

}
public class CursorExitArgs : CursorArgs
{
	public float timeWhenFocus;
	public float timeWhenExit;
	public CursorExitArgs(Cursor cursor, float time,float focusTime, float exitTime, GameObject selected , GameObject [] inTheImpactZoneOfFocus) : base (cursor,time,selected, inTheImpactZoneOfFocus)
	{
		timeWhenFocus = focusTime;
		timeWhenExit = exitTime;
	}

}