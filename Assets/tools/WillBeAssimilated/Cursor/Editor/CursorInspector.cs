﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Diagnostics;
[CustomEditor(typeof(Cursor))]
public class CursorInspector : Editor {


	public static bool helperOn=true;
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();

		helperOn = EditorGUILayout.Foldout (helperOn, "Help");
			if (helperOn) {

				EditorGUILayout.BeginHorizontal();
					if (GUILayout.Button ("Video")) {
							Process p = new Process ();
							p.StartInfo.FileName = "https://www.youtube.com/playlist?list=PLpEIkmL95a3rpJmKSWx6Cpshwu8-XtPwl";
									p.Start ();
						}

			if (GUILayout.Button ("Wiki")) {
				Process p = new Process ();
				p.StartInfo.FileName = "https://bitbucket.org/PrIMD/toolbox/wiki/Home";
				p.Start ();
			}EditorGUILayout.EndVertical();
			}
		}


}
