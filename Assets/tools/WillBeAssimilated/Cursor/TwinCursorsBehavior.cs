﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class TwinCursorsBehavior : MonoBehaviour {
	
	public Cursor cursorOne;
	private GameObject selectionCursorOne;

	public Cursor cursorTwo; 
	private GameObject selectionCursorTwo;
	
	public EventHandler<TransferSelectionArgs> onTransfertSelection;
	public EventHandler<DoubleSelectionArgs> onDoubleSelection;

	protected void Start()
		{
			if (cursorOne != null && cursorTwo!=null) {
				
				cursorOne.events.OnCursorEnterDetected += 	FirstEnterEvents;
				cursorOne.events.OnCursorExitDetected += 	FirstExitEvents;		
				
				cursorTwo.events.OnCursorEnterDetected += 	SecondEnterEvents;
				cursorTwo.events.OnCursorExitDetected += 	SecondExitEvents;		
			}

		}


	protected bool CheckIfDoubleSelection(out GameObject gamoSelected){

		if (cursorOne != null && cursorTwo != null && selectionCursorOne != null && selectionCursorTwo != null) {
			if(selectionCursorOne == selectionCursorTwo){gamoSelected = selectionCursorOne; return true;}
		}
		gamoSelected = null;
		return false;
	}
	protected bool CheckIfTransferSelection(out GameObject gamoOne,out GameObject gamoTwo){

		
		if (cursorOne != null && cursorTwo != null && selectionCursorOne != null && selectionCursorTwo != null) {
			if(selectionCursorOne != selectionCursorTwo){
				gamoOne = selectionCursorOne;
				gamoTwo = selectionCursorTwo;
				return true;}
		}
		gamoOne = null; gamoTwo = null;
		return false;
	}

	protected void CheckEventualityOfEvents(){
		GameObject gamoSelected;
		GameObject gamoOne;
		GameObject gamoTwo;
		
		
		if(CheckIfDoubleSelection (out gamoSelected))
			DoubleSelection(cursorOne, cursorTwo, gamoSelected);
		
		else if(CheckIfTransferSelection (out gamoOne, out gamoTwo))
			TransferSelection(cursorOne, cursorTwo, gamoOne, gamoTwo);
	}

	public void FirstEnterEvents(object obj, CursorEnterArgs args){
		selectionCursorOne = args.selected;
		CheckEventualityOfEvents ();
	}
	public void FirstExitEvents(object c, CursorExitArgs args){
		
		selectionCursorOne = null;
	}



	public void SecondEnterEvents(object obj, CursorEnterArgs args){
		selectionCursorTwo = args.selected;
		CheckEventualityOfEvents ();
	}
	public void SecondExitEvents(object c, CursorExitArgs args){
		selectionCursorTwo = null;
	}

	
	
	protected abstract void OnDoubleSelection( GameObject selected);
	private void DoubleSelection(Cursor one,	Cursor two, GameObject selected)
	{
		if (one != null && two != null && selected != null) {
			DoubleSelectionArgs args = new DoubleSelectionArgs (selected);
			args.Add (		one  	);
			args.Add (		two		);
			if(onDoubleSelection!=null) onDoubleSelection(this, args);
			OnDoubleSelection( selected);
		}
	}


	protected abstract void OnTransferSelection (GameObject cursorOneSelection, GameObject cursorTwoSelection);
	private void TransferSelection(Cursor one,Cursor two, GameObject cursorOneSelection, GameObject cursorTwoSelection)
	{
		
		if (one != null && two != null && cursorOneSelection!=null && cursorTwoSelection!=null ) {
			TransferSelectionArgs args = new TransferSelectionArgs (one,two,cursorOneSelection,cursorTwoSelection);

			if(onTransfertSelection!=null) onTransfertSelection(this, args);
			OnTransferSelection(cursorOneSelection,cursorTwoSelection);
		}


	}

	}
public class DoubleSelectionArgs : EventArgs
{
	private readonly GameObject selected;
	private Dictionary< string, Cursor> selectors =  new Dictionary<string,Cursor>();
	
	public DoubleSelectionArgs( GameObject gamo)
	{
		if(gamo==null) throw new NullReferenceException();
		selected = gamo;
	}
	
	public bool Add(Cursor cursor)	
	{
		if (selectors.ContainsKey (cursor.name))
			return false;
		selectors.Add(cursor.name, cursor);
		return true;
		
	}
	
	public GameObject GetSelected(){return selected;}
	public Cursor [] GetSelectors(){
		Cursor [] objs = new Cursor[selectors.Count];
		selectors.Values.CopyTo (objs, 0);
		return objs;}
	
}




public class TransferSelectionArgs : EventArgs
{
	public readonly Cursor cursorOne;
	public readonly GameObject cursorOneSelection;
	
	public readonly Cursor cursorTwo; 
	public readonly GameObject cursorTwoSelection;
	
	public TransferSelectionArgs( Cursor cursorOne,Cursor cursorTwo,GameObject cursorOneSelection, GameObject cursorTwoSelection )
	{
		if(cursorOne==null || cursorTwo==null || cursorOneSelection==null || cursorTwoSelection==null ) throw new NullReferenceException();
		
		this.cursorOne =cursorOne;
		this.cursorOneSelection  =cursorOneSelection;
		
		this.cursorTwo= cursorTwo; 
		this.cursorTwoSelection =cursorTwoSelection;
	}
	
	
}
