﻿using UnityEngine;
using System.Collections;

public class TestCursorListner : MonoBehaviour, ICursorListner {

	public Cursor cursor;

	public void Start()
	{
		if (cursor != null) {
			cursor.events.OnCursorEnterDetected	+=EnterEvents;
			cursor.events.OnCursorFocusedDetected	+=FocusEvents;
			cursor.events.OnCursorExitDetected		+=ExitEvents;		
		}

		//cursor.listners.AddListner (this);
	

	}
	
	public void EnterEvents(object c, CursorEnterArgs args){Debug.Log ("Bonjour");}
	public void FocusEvents(object c, CursorFocusArgs args){Debug.Log ("Ok Au travail");}
	public void ExitEvents(object c, CursorExitArgs args){Debug.Log ("A plus tard");}


	public void OnCursorIsSelecting (float time, GameObject selected)
	{
		
		print ("......Selecting");
	}

	public void OnCursorIsFocusing (float time, GameObject selected)
	{
		print ("......Focusing");
	}



}
