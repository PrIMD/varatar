﻿using UnityEngine;
using System.Collections;

public abstract class MoveCursorsBehavior : TwinCursorsBehavior {
	//this version is base on a comparation point
	//the next is going to be based on body move
	public Transform comparePoint;
	public bool usingSelections=false;
	public float multiplicator =3f;

	public GameObject selected ;
	public Vector3 initialSelectedPosition;
	public Vector3 initialComparePosition;


	protected void Update(){

		if (usingSelections == false) {
			if(selected!=null && LockCondition() )
			{
				usingSelections=true;
				initialSelectedPosition= selected.transform.position;
				initialComparePosition= comparePoint.position;
			}
		}
		if (usingSelections) {
			Vector3 compMoveOf = (initialComparePosition - comparePoint.position) * multiplicator;
			selected.transform.position= 	initialSelectedPosition-compMoveOf;

			
			if(CancelCondition())
			{
				selected.transform.position = initialSelectedPosition;
				Reset();
			}

			if(ValidateCondition())
			{
				Reset ();

			}
		}
	}

	public void Reset(){
			usingSelections = false;
		  selected  =null;
		  initialSelectedPosition=Vector3.zero;
		  initialComparePosition=Vector3.zero;
	}

	protected override void OnDoubleSelection ( GameObject selected)
	{
		Debug.Log ("Double selection on: " + selected+ " (by: "+cursorOne.purpose+","+cursorTwo.purpose+")");
		this.selected = selected;
	}

	protected override void OnTransferSelection ( GameObject cursorOneSelection, GameObject cursorTwoSelection)
	{//		Debug.Log ("Transfer selection on: " + cursorOneSelection+ " <-> "+cursorTwoSelection+" (by: "+cursorOne.purpose+","+cursorTwo.purpose+")");
	}
	
	public abstract bool LockCondition ();
	public abstract bool CancelCondition ();
	public abstract bool ValidateCondition ();
	/*
	public void SetLock(bool onOff)
	{
		usingSelections = true;
	}
	 */

}
