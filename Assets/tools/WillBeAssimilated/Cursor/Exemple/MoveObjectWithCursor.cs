﻿using UnityEngine;
using System.Collections;

public class MoveObjectWithCursor : MoveCursorsBehavior {


	public override bool LockCondition ()
	{
		return X360.IsA ();
	}
	public override bool CancelCondition ()
	{
		return X360.IsLb ();
	}

	public override bool ValidateCondition ()
	{
		return X360.IsRb ();

	}
}
