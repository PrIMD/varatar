﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
[CustomEditor(typeof(QuickSoundAvailable))]
public class QuickSoundInpsector : Editor {
	
	public static bool plus =false;
	public static bool showSoundAvailable =true;
	public static HashSet<string> keywords = new HashSet<string>();

	public override void OnInspectorGUI()
	{

		RefreshAllKeywordAvailaible ();

		////DISPLAY KEYWORD
		EditorGUILayout.LabelField("KeyWords :");

		bool first=true;
		string keyWord = "";
		if (keywords.Count == 0)
						keyWord += "None";
		foreach (string name  in keywords) {
			keyWord+= (first?" ":", ")+name;
			first=false;
		}
		
		EditorGUILayout.TextField(keyWord);

		first = true;
		string  []  soundNotFound = Sound.GetSoundNotFound ();
		//keywords.CopyTo(keywordsTab);
		if (soundNotFound.Length > 0){
				string keyNotFound = "Not Found:";

			foreach (string name  in soundNotFound) {
				keyNotFound+= (first?" ":", ")+name;
				first=false;
			}
			
			EditorGUILayout.TextField(keyNotFound);
		}


		EditorGUILayout.Space ();EditorGUILayout.Space ();

		////DISPLAY INSPECTOR
		base.DrawDefaultInspector ();

		///MORE INFORMATION ON THE SOUND
		EditorGUILayout.Space ();EditorGUILayout.Space ();
		plus = EditorGUILayout.Foldout (plus, "Plus");

				if (plus && Application.isPlaying) {
					
						showSoundAvailable = EditorGUILayout.Foldout (showSoundAvailable, "Sound Available");
						if (showSoundAvailable) {
								string [] soundName = Sound.GetSoundStoredNames ();
								if (soundName.Length == 0)
										EditorGUILayout.LabelField ("None");
								foreach (string name  in soundName) {
										AudioClip [] sounds = Sound.GetSoundsForName (name);
										EditorGUILayout.LabelField (name + " (" + (sounds.Length) + ")");
								}
		
		
						}
				}
		if (plus &&!Application.isPlaying) {
				EditorGUILayout.LabelField ("Data only on play mode");
			}
	}
	

	private void RefreshAllKeywordAvailaible ()
	{
		keywords.Clear ();
		QuickSoundAvailable [] allSound = GameObject.FindObjectsOfType<QuickSoundAvailable> () as QuickSoundAvailable [];
		if (allSound != null)
						foreach (QuickSoundAvailable quickSound in allSound) {
								quickSound.AddSound();
								foreach (SoundData data in quickSound.sound) {
										if (data.keyword != null && data.keyword.Length > 0 && data.audio != null)
												keywords.Add (data.keyword);
								}
						}
	}
}

//[CustomPropertyDrawer(typeof(SoundData))]
//public class SoundDataDrawer : PropertyDrawer {


//}
