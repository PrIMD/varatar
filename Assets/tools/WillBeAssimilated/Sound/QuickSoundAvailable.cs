﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

using UnityEngine;
using System.Collections;
using System;

public class QuickSoundAvailable : MonoBehaviour {
	
	public SoundData[] sound;
	public string storageName="";

	// Use this for initialization
	void Start () {
		Sound.SetStorageZoneForSound (storageName);
		AddSound();
	}
	

	public void AddSound ()
	{
		foreach (SoundData d in sound) {
			if (d.keyword != null && d.keyword.Length > 0 && d.audio != null)
				Sound.AddSound (d.keyword, d.audio);
			
		}
	}
}

[Serializable]
public class SoundData 
{
	public string keyword;
	public AudioClip audio;
	
}