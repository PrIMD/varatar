﻿using UnityEngine;
using System.Collections;
using System;
namespace be.primd.toolkit.bodytracker{
[Serializable]
public class StepObserver {


	private readonly Foot foot;
	public float whenStart;
	public float maxHeight;
	
	public static readonly float minValideDurationDefault= 0.1f;
	public static readonly float maxValideDurationDefault= 2f;
	public float minValideDuration= minValideDurationDefault;
	public float maxValideDuration= maxValideDurationDefault;
	public bool inTheAir;
	public bool hasBeenStartInitialize;
	/**Do the air in control by exteneral*/
	public bool inAirControlByExtenalSource =false;
	
	public bool sendInvalideStepEvent = false;
	public bool sendOutOfTimeStepEvent = false;

	public EventHandler<StepEventArgs> OnStep;
	public EventHandler<StepStartEventArgs> OnStartStep;

	public StepObserver (Foot foot)
	{
		this.foot = foot;
		Reset ();
	}
	
	
	public void StartStep(float time, float maxValideDuration =0f , float minValideDuration =0f)
	{
		if (maxValideDuration <= 0)
			maxValideDuration = maxValideDurationDefault;
			this.maxValideDuration = maxValideDuration;
		if (minValideDuration <= 0)
			minValideDuration = minValideDurationDefault;
			this.minValideDuration = minValideDuration;

		whenStart = time;
		if (! inAirControlByExtenalSource)
						inTheAir = true;
		if(OnStartStep!=null) 
			OnStartStep(null,new StepStartEventArgs(foot,whenStart,minValideDuration,maxValideDuration));
		hasBeenStartInitialize = true;

	}
	
	public void SetLastHeight(float height){
		if (height > maxHeight)
			maxHeight = height;
	}

	public void SetInAir(bool value=true){

		inTheAir = value;
		if (! inAirControlByExtenalSource)
						DeactivateInAirAutoManage ();
	}
	public void SetOnGround(bool value=true){
		inTheAir = !value;
		if (! inAirControlByExtenalSource)
			DeactivateInAirAutoManage ();
	}
	private void DeactivateInAirAutoManage(){
		if (! inAirControlByExtenalSource) {
			Debug.Log("IsInAir value in no manually controled");
			inAirControlByExtenalSource = true;
		}
	}

	public void EndStep(float time)
	{
		if (hasBeenStartInitialize) {
						float whenEnd = time;
						float duration = whenEnd - whenStart;
						float heightOfStep = maxHeight;
						bool outOfTime = duration > maxValideDuration;
						bool valide = duration > minValideDuration;
						
						if (OnStep != null )
						{
							if(valide || (!valide && sendInvalideStepEvent))
								if( ! outOfTime || (outOfTime && sendOutOfTimeStepEvent))
					   				OnStep (null, new StepEventArgs (foot, whenStart, whenEnd, duration, heightOfStep, outOfTime, ! valide));
						}	
			if(valide)
						Reset ();

				}
	}

	public void Reset(){
		hasBeenStartInitialize = false;
		whenStart = 0;
		maxHeight = 0;
		if(!inAirControlByExtenalSource)
			inTheAir = false;
		minValideDuration=minValideDurationDefault;
		maxValideDuration = maxValideDurationDefault;
	}

	public bool IsInTheAir ()
	{
		return inTheAir;
	}
	public bool IsOnGround ()
	{
		return ! inTheAir;
	}

	public float GetMaxDurationTime ()
	{
		return maxValideDuration;
	}

	public float GetDuration (float time)
	{
		float d = time - whenStart;
		if (d <= 0) d = 0;
		return d;
	}
}


public class StepEventArgs :EventArgs
{
	public readonly Foot foot;
	public readonly float whenStart;
	public readonly float whenFinish;
	public readonly float duration;
	public readonly float height;
	public readonly bool outOfTime;
	public readonly bool invalide;
	
	public StepEventArgs (Foot foot, float whenStart, float whenFinish, float duration, float height, bool outOfTime, bool invalide)
	{
		this.foot = foot;
		this.whenStart = whenStart;
		this.whenFinish = whenFinish;
		this.duration = duration;
		this.height = height;
		this.invalide = invalide;
		this.outOfTime = outOfTime;
	}

	public float GetFrequency ()
	{
		return 1f / duration;
	}

	public override string ToString ()
	{
		string state = "";
		if (invalide) state = "INVALIDE -- ";
		if (outOfTime) state = "OUT OF TIME -- ";
		return string.Format (state + "END  --  [StepEventArgs: foot={0}, whenStart={1}, whenFinish={2}, duration={3}, height={4}, outOfTime={5}, invalide={6}]", foot, whenStart, whenFinish, duration, height, outOfTime, invalide);
	}
	
	
}
public class StepStartEventArgs :EventArgs
{
	public readonly Foot foot;
	public readonly float whenStart;
	public readonly float maxValideDuration;
	public readonly float minValideDuration;
	
	public StepStartEventArgs (Foot foot, float whenStart, float  minValideDuration,float  maxValideDuration)
	{
		this.foot = foot;
		this.whenStart = whenStart;
		this.maxValideDuration = maxValideDuration;
		this.minValideDuration = minValideDuration;
	}
	
	public override string ToString ()
	{
		return string.Format ("START  --  [StepStartEventArgs: foot={0}, whenStart={1}, maxValideDuration={2}, minValideDuration={3}]", foot, whenStart, maxValideDuration, minValideDuration);
	}
	
	}
}