﻿using UnityEngine;
using System.Collections;

namespace be.primd.toolkit.bodytracker{
public class Feet : MonoBehaviour {

	public Transform rotationRef;
	public Transform groundPointRef;
	public bool autoAdjusteGround=true;

	public Transform leftFoot;
	public Vector3 leftPosition;

	public Transform rightFoot;
	public Vector3 rightPosition;
	protected float timeStableBeforeAdjustement=2f;
	public float adjustementZoneDistance=0.3f;
	public float oneFootDistance=0.7f;

	
	public  StepObserver left = new StepObserver(Foot.Left);
	public  StepObserver right = new StepObserver(Foot.Right);

	private float whenLastStep;

	protected  void Start(){
		if (leftFoot == null) {
			Debug.LogWarning("Foot Left is not define",this.gameObject);
		}
		if (rightFoot == null) {
			Debug.LogWarning("Foot right is not define",this.gameObject);		
		}
		if (leftFoot != null && rightFoot != null && leftFoot == rightFoot) {
			Debug.LogWarning("Foot left is the foot right, it is invalide",this.gameObject);
			Debug.Break();
		}
		left.OnStep 		+= OnStep;
		right.OnStep 		+= OnStep;
	}
	private void OnStep(object obj, StepEventArgs step){

		whenLastStep = Time.realtimeSinceStartup;
	}

	public void Update(){
		float time = Time.realtimeSinceStartup;
		leftPosition 	= Relocated (leftFoot.position, groundPointRef.position, rotationRef.rotation);
		rightPosition 	= Relocated (rightFoot.position, groundPointRef.position,rotationRef.rotation);



		float leftHeight = GetHeight (Foot.Left);
		float rightHeight = GetHeight (Foot.Right);
		bool leftInAir = leftHeight > oneFootDistance;
		bool rightInAir = rightHeight > oneFootDistance;


		//START AND END STEP DETECTION
		if (! left.IsInTheAir () && leftInAir) {
			
			left.SetInAir (true);
			left.StartStep(time,2f);
		}
		if ( left.IsInTheAir () && !leftInAir) {
			
			left.SetInAir (false);
			left.EndStep(time);
		}
		if (! right.IsInTheAir () && rightInAir) {
			right.SetInAir (true);
			right.StartStep(time,2f);
		}
		if ( right.IsInTheAir () && !rightInAir) {
			
			right.SetInAir (false);
			right.EndStep(time);
		}
		//REFRESH DATA
		left.SetLastHeight (leftHeight);
		right.SetLastHeight (rightHeight);


		// CHECK LIMIT
		if(left.GetDuration(time)> left.GetMaxDurationTime())
		{
			left.EndStep(time);
		}

		float lastTimeFeetDuration = time - whenLastStep;
		if (lastTimeFeetDuration>timeStableBeforeAdjustement && autoAdjusteGround && GetFeetGap () < adjustementZoneDistance) {
			groundPointRef.position= (leftFoot.position+ rightFoot.position)/2f;		
		}
	
	}

	public Foot GetAboveFoot()
	{
		if (leftPosition.y < rightPosition.y)
			return Foot.Right;
		return Foot.Left;
	}
	
	public float GetGroundToFeetGap ()
	{
		float distLeft =Mathf.Abs(leftPosition.y);
		float distRight=Mathf.Abs(rightPosition.y);
		return distLeft<distRight ? distLeft: distRight;
	}
	
	public float GetFeetGap(){
		return Mathf.Abs(leftPosition.y-rightPosition.y);
	}
	
	public bool IsOnFeet ()
	{
		return !left.IsInTheAir() && !right.IsInTheAir ();
	}
	public bool IsOnLeftFoot ()
	{
		return left.IsOnGround ();
	}
	public bool IsOnRightFoot ()
	{
		return right.IsOnGround ();
	}
	public bool IsOnOneFoot(){
		return (left.IsOnGround() && right.IsInTheAir()) || (left.IsInTheAir() && right.IsOnGround()) ;
	}

	public bool IsJumping ()
	{
		return left.IsInTheAir() && right.IsInTheAir ();
	}

	public float GetHeight (Foot whichFoot)
	{
		if (whichFoot == Foot.Left) {
			return leftPosition.y;
		} else {
			return rightPosition.y;
		}
	}

	public static Vector3 Relocated(Vector3 point,Vector3 rootPosition ,Quaternion rootRotation, bool withIgnoreX=false, bool withIgnoreY=false, bool withIgnoreZ=false){
		Vector3 p = point- rootPosition;
		if (rootRotation.eulerAngles != Vector3.zero) {
			Quaternion rootRotationInverse = Quaternion.Inverse (rootRotation);
			p = RotateAroundPoint (p, Vector3.zero, rootRotationInverse);
		}
		if (withIgnoreX)
			p.x = 0;
		if (withIgnoreY)
			p.y = 0;
		if (withIgnoreZ)
			p.z = 0;
		return p;
	}
	
	public static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}
	
}
public enum Foot {Left, Right}
}
