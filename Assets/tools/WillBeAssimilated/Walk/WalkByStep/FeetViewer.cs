using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker;

public class FeetViewer : MonoBehaviour {
	public Transform leftFoot;
	public Transform rightFoot;
	
	public Transform leftFootHeight;
	public Transform rightFootHeight;
	private Vector3 leftFootInitPosition;
	private Vector3 rightFootInitPosition;
	public float heightMultiplicator;
	public TextMesh textInfo;
	public TextMesh textInfoPlus;
	public Feet feet ;
	
	
	void Start()
	{
		leftFootInitPosition = leftFootHeight.position;
		rightFootInitPosition = rightFootHeight.position;
		
	}
	
	
	// Update is called once per frame
	void Update () {
		
		if (leftFoot == null || rightFoot == null|| leftFootHeight == null || rightFootHeight == null ) {
			Debug.Log("You forget to link asset required",this.gameObject);
			Debug.Break();
			Destroy(this);
		}
		
		if (feet.IsOnFeet()) {
			leftFoot.renderer.material.color =  Color.green;
			rightFoot.renderer.material.color = Color.green;
		} else if (feet.IsJumping ()) {
			leftFoot.renderer.material.color =   Color.red;
			rightFoot.renderer.material.color = Color.red;		
			
		}else {
			bool isOnLeft = feet.IsOnLeftFoot ();
			leftFoot.renderer.material.color =  isOnLeft? Color.green : Color.red;
			rightFoot.renderer.material.color = isOnLeft ? Color.red : Color.green;		
		}
		leftFootHeight.position = leftFootInitPosition + new Vector3 (0, feet.GetHeight(Foot.Left), 0);
		rightFootHeight.position = rightFootInitPosition + new Vector3 (0, feet.GetHeight(Foot.Right), 0);
		
		//if (textInfo != null && textInfoPlus!=null && walk !=null  ) {
		//	if(walk.IsWalking())
		//		textInfo.text+= "  Walking"  ;
		//}
	}
}
