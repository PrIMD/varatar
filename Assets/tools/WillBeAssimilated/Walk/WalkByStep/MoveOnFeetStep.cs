﻿using UnityEngine;
using System.Collections;

namespace be.primd.toolkit.bodytracker{
public class MoveOnFeetStep : MonoBehaviour {

	
	public Feet feet;
	private float forwardPower;
	public float stepPowerValue = 0.5f;
	public float speed=1f;
	public float averageFrequency=1f;
	void Start () {
		if (feet != null) {
			feet.left.OnStep+=MoveOfOnStep;
			feet.right.OnStep+=MoveOfOnStep;
				} else
						Destroy (this);
	}

	public void Update(){
		if (forwardPower > 0f) {
			transform.Translate(Vector3.forward*speed*averageFrequency);
			forwardPower= Mathf.Lerp(forwardPower,-0.5f, Time.deltaTime);		
		}

	}
	public void MoveOfOnStep(object obj, StepEventArgs step)
	{
		if (step.outOfTime || step.invalide)
						return ;
		averageFrequency = (averageFrequency + step.GetFrequency ()) / 2f;
		forwardPower = step.duration+stepPowerValue;

	}
	}}
