﻿using UnityEngine;
using System.Collections.Generic;

namespace be.primd.toolkit.bodytracker{
public class WalkStepPattern : MonoBehaviour {

	public Feet feet;
	public enum StepsDuo:int{OnGround=1, OnRight=2,OnLeft=3, InAir=4}
	public List<int> walkStepsRecord = new List<int>();
	
	public bool isLeftOnGround=true;
	public bool isRightOnGround=true;

	void Start () {
		if (feet != null) {
			feet.left.OnStartStep +=StepStart;		
			feet.left.OnStep +=StepEnd;		
			feet.right.OnStartStep +=StepStart;		
			feet.right.OnStep +=StepEnd;		
				} else
						Destroy (this);
	}
	public void StepStart(object obj, StepStartEventArgs stepStart){

		
		if(SetStepPositionTo (false,stepStart.foot))
		AddToRecord ();
	}
	
	public void StepEnd(object obj, StepEventArgs stepEnd){

		if (stepEnd.invalide || stepEnd.outOfTime)
						return ;

		if(SetStepPositionTo (true,stepEnd.foot))
			AddToRecord ();
	}

	void AddToRecord ()
	{
		StepsDuo newPattern = GetStepsPattern (isLeftOnGround, isRightOnGround);
		walkStepsRecord.Add((int) newPattern);
	}

	bool SetStepPositionTo (bool onGround, Foot foot)
	{
		bool switchDetected = false;
		if (foot == Foot.Left) {
			switchDetected = isLeftOnGround!=onGround;
			isLeftOnGround = onGround;
		} else if (foot == Foot.Right) {
			switchDetected	= isRightOnGround!=onGround;
			isRightOnGround = onGround;
		}
		return switchDetected;
	}

	
	public static StepsDuo GetStepsPattern(bool leftOnGround, bool rightOnGround){
		StepsDuo duo = StepsDuo.OnGround;
		if (leftOnGround && rightOnGround)
			duo = StepsDuo.OnGround;
		else if (!leftOnGround && rightOnGround)
			duo = StepsDuo.OnRight;
		else if (leftOnGround && ! rightOnGround)
			duo = StepsDuo.OnLeft;
		else if (! leftOnGround && ! rightOnGround)
			duo = StepsDuo.InAir;
		return duo;
	}

	}}
