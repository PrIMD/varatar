﻿using UnityEngine;
using System.Collections;

public class RestartScript  : MonoBehaviour {
	
	public YesNoDetector yesNoDetector;
	public Menu3D menu;
	// Use this for initialization
	void Start () {
		
		
		yesNoDetector.OnNo += No;
		yesNoDetector.OnYes += Yes;
	}
	
	public void No(object obj, NoDetectedEventArgs no)
	{
		if (menu != null) {
			menu.SetOn(false);		
		}
		
	}
	
	
	public void Yes(object obj, YesDetectedEventArgs yes)
	{
		//Debug.Break ();
		Application.LoadLevel (Application.loadedLevel);
	}
	
}