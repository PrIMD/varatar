﻿using UnityEngine;
using System.Collections;

public class GameOverTimer : MonoBehaviour {

	private bool isGameOver;
	public float countDownTime=10f;
	public float countDown=-1f;
	public float whenStartGameOver;
	void Start () {
		LevelManager.OnGameOver += OnGameOver;
	}


	public float GetCountDown(){
		return countDown;
	}
	public void Update(){
		if (isGameOver) {
			 countDown = countDownTime -(  Time.realtimeSinceStartup - whenStartGameOver);
	
			if(countDown<0){
				Time.timeScale=1f;
				Restart();
			}
		}

	}

	public virtual  void OnGameOver(object obj, GameOverEventArgs gameover)
	{
		if ( gameover !=null ) {
			countDownTime=gameover.timeBeforeNextLevel;
			isGameOver=true;
			whenStartGameOver= Time.realtimeSinceStartup;
			Time.timeScale=0;
			countDownTime = gameover.timeBeforeNextLevel+1f;
		
				
		}
	}
	public void Restart(){Application.LoadLevel (Application.loadedLevel);}

}
