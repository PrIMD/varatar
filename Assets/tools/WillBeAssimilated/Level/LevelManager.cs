﻿using UnityEngine;
using System.Collections;
using System;
public class LevelManager {
	public static EventHandler<StartLevelEventArgs> OnLevelStart;
	/**Warn every listener that the next level will be call*/
	public static EventHandler<EndOfLevelEventArgs> OnLevelEnd;
	public static EventHandler<GameOverEventArgs> OnGameOver;

	/*Ask to return to the main menu and warn everyone before*/
	public static void ReturnToMainMenu ()
	{
		NotifyNextLevel(Application.loadedLevelName, ""+0 );
		Application.LoadLevel (0);

	}
	
	/* Ask to go at the next level and warn everyone before*/
	public static void NextLevel()
	{
		NotifyNextLevel(Application.loadedLevelName,""+(Application.loadedLevel+1));
		Application.LoadLevel (Application.loadedLevel+1);
	}
	
	/* Ask to go at the next level and warn everyone before*/
	public static void NextLevel(string nextLevelName)
	{
		if (nextLevelName != null && nextLevelName.Length > 0) {
						NotifyNextLevel (Application.loadedLevelName, nextLevelName);
						Application.LoadLevel (nextLevelName);
	} else
			ReturnToMainMenu ();
	}

	public static void NotifyNextLevel ( string from, string to, float timeBeforeChange=0)
	{
//		Debug.Log("END OF LEVEL -"+Application.loadedLevelName+"-"); 
		if (OnLevelEnd != null)
			OnLevelEnd (null, new EndOfLevelEventArgs (from,to,timeBeforeChange));
	}


	public static void GameOver (float s)
	{
		if (OnGameOver != null)
			OnGameOver (null, new GameOverEventArgs (s));
	}

	public static void Restart ()
	{
		//Improve later
		Application.LoadLevel (Application.loadedLevel);
	}

	public static void Start(){
		if (OnLevelStart != null)
			OnLevelStart (null, new StartLevelEventArgs ());
	}
}

public class EndOfLevelEventArgs :  EventArgs
{
	public string from, to; 
	public float timeBeforeNextLevel;
	public EndOfLevelEventArgs(string from,string to, float timeBeforeNext=0, bool isGameOver=false){
		this.from = from;
		this.to = to;
		this.timeBeforeNextLevel = timeBeforeNext;
	}

}
public class GameOverEventArgs : EventArgs
{
	public float timeBeforeNextLevel;
	public GameOverEventArgs (float timeBeforeNextLevel)
	{
		this.timeBeforeNextLevel = timeBeforeNextLevel;
	}
}	

public class StartLevelEventArgs : EventArgs
{

}	