﻿using UnityEngine;
using System.Collections;

public class ActivateWhenPlay : MonoBehaviour {


	public GameObject [] stuffsToActivate;

	void Start () {
		foreach (GameObject gamo in stuffsToActivate) {
			gamo.SetActive(true);		
		}
		Destroy (this);
	}
	
}
