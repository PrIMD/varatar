﻿using UnityEngine;
using System.Collections;

public class StartPoint : MonoBehaviour {

	public Transform point ;
	public GameObject playerPrefab;
	void Start () {
	
		if (point == null) 
						point = this.transform;

		if (playerPrefab == null)
						Debug.LogError ("Player prefab of Start Point should not be null", this.gameObject);
		else GameObject.Instantiate (playerPrefab,point.position,point.rotation);

		Destroy (this);
	}

}
