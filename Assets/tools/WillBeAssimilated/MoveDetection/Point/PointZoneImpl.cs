using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PointZoneImpl : PointZone {
	/**True give a quick overview of the point when he is near to be use*/
	public bool display =true;
	public Material matActivated;
	public Material matNear;
	
	public float lastTimeActivated=0;
	public float lastTimeDeactivated=0;


	public float lastRefresh;
	public float refreshFrequence = 0.5f;


	void Update () {
		float time = Time.time;
		if (time - lastRefresh > refreshFrequence) {
			lastRefresh=time;
			Display ();
		}
	}


	protected void Display()
	{
		Renderer render = GetComponent<Renderer> () as Renderer;
		if (render != null)
			render.enabled = display;
		if (display) {
			transform.localScale= new Vector3(1,1,1)*activateZone;
			
			if(IsActivate())
			{
				
				transform.renderer.enabled=true;
				transform.renderer.material=matActivated;
			}
			else if (HasNearActivator())
			{
				transform.renderer.enabled=true;
				transform.renderer.material=matNear;
			}
			else {
				transform.renderer.enabled=false;
				transform.renderer.material=null;
			}
		}

	}
}
