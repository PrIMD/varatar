﻿using UnityEngine;
using System.Collections.Generic;

public abstract class PointZoneObserver : MonoBehaviour {


	public List<PointZone> observedPoints= new List<PointZone>();
	private bool actif;

	public void Update()
	{
		foreach (PointZone pt in observedPoints) {
			if( pt!=null)
			{

				if(pt.IsActivate())
				{
					if(actif==true){
						OnActivate(pt);
					}else
					{
						actif=true;
						OnActivateEnter(pt);
					}
				}
				else if (actif==true)
				{
					actif=false;
					OnActivatedExit(pt);
				}

				if(pt.HasNearActivator())
				{
					OnNear(pt);

				}

			}
		}

	}
	
	public abstract void OnNear(PointZone zone);
	public abstract void OnActivateEnter(PointZone zone);
	public abstract void OnActivate(PointZone zone);
	public abstract void OnActivatedExit(PointZone zone);
}

