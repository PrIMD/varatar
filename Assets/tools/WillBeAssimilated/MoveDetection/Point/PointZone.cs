﻿using UnityEngine;
using System.Collections.Generic;

public abstract class PointZone : MonoBehaviour {

	public string purpose ="Just blink";

	public float activateZone=1f;
	public float nearZone=2f;
	
	public bool isActivate;
	public bool isNearActivate;
	public float lastActivation;
	public float lastDeactivation;
	public float lastNearZoneActivation;
	public float bestNearDistance;

	public void ResetStatisticData(){
		  lastActivation 			=0f;
		  lastDeactivation			=0f;
		  lastNearZoneActivation	=0f;
		  bestNearDistance			=0f;
	}

	public List<GameObject> trackedPoints = new List<GameObject>();




	public  List<GameObject> GetPointsInActivationZone()
	{
		List<GameObject> activator = new List<GameObject> ();
		foreach (GameObject gamo in trackedPoints) {
			if(IsInActivationZone(gamo))
				activator.Add (gamo);		
		}
		return activator;
	}
	public  List<GameObject> GetPointsInNearZone()
	{
		List<GameObject> activator = new List<GameObject> ();
		foreach (GameObject gamo in trackedPoints) {
			if(IsInNearZone(gamo))
				activator.Add (gamo);		
		}
		return activator;
	}






	
	public bool IsInActivationZone(GameObject gamo){
		return gamo != null && Vector3.Distance (transform.position, gamo.transform.position) <= activateZone;
	}
	public bool IsInNearZone(GameObject gamo){
		return gamo != null && Vector3.Distance (transform.position, gamo.transform.position) <= nearZone;
	}
	public bool IsActivate()
	{
		foreach (GameObject gamo in trackedPoints) {
			if(IsInActivationZone(gamo)){
				isActivate=true;
				lastActivation= GetTime();
				return true;}		
		}
		if (isActivate) {
			lastDeactivation= GetTime();
		}
		isActivate=false;

		return false;
	}
	public bool HasNearActivator()
	{
		foreach (GameObject gamo in trackedPoints) {
			if(IsInNearZone (gamo)){
				lastNearZoneActivation = GetTime();
				isNearActivate=true;
				GameObject nearestPt = GetNearestPoint();
				bestNearDistance = Vector3.Distance(transform.position, nearestPt.transform.position);
				return true;		
			}
		}
		isNearActivate = false;
		return false;

	}
	public GameObject GetNearestPoint(bool inValideZone=true)
	{
		GameObject nearest=null;
		float distance = 10000f;
		foreach (GameObject gamo in trackedPoints) {
			if(IsInNearZone (gamo)){
				float d = Vector3.Distance(transform.position, gamo.transform.position);
				if(	distance > d)
				{
					if(d<nearZone)
					{
						distance= d;
						nearest=gamo;
					}
				}
			}
		}
		return nearest;
		
	}

	public float GetBestPourcentActivated(){
		if (lastActivation >0 )
			return 1f;
		float pct  =(bestNearDistance - activateZone) / nearZone;
		Mathf.Clamp (pct, 0f, 1f);
		return pct;
	}  



	public float GetTime(){
		return Time.timeSinceLevelLoad ;
	}

	///dpublic List<PointZoneListener> listeners= new List<PointZoneListener>();	


}
