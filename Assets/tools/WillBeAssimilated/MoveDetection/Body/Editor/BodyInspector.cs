﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;

[CustomEditor(typeof(BodyImpl))]
public class BodyInspector : Editor {
	public static   bool showMoreInfo;
	public static   bool showRotation = false;
	public static   bool showPoint = true;

	public static  bool showPointerPart = true;
	
	public static   bool showRotationInPlayMode = false;
	public static   bool showPointInPlayMode = false;

	public override void OnInspectorGUI()
	{
		BodyImpl body = (BodyImpl) target;	
		
		base.OnInspectorGUI ();
		if(! Application.isPlaying)
			showPoint = EditorGUILayout.Foldout(showPoint, "Point ");
		else 
			showPointInPlayMode = EditorGUILayout.Foldout(showPointInPlayMode, "Point (PM)");
		if((showPoint &&  ! Application.isPlaying) || (showPointInPlayMode &&   Application.isPlaying))
		{
			body.Root= EditorGUILayout.ObjectField("Root Position",body.Root, typeof(GameObject), true) as GameObject;
			body.HipCenter= EditorGUILayout.ObjectField("Hip Center",body.HipCenter, typeof(GameObject), true) as GameObject;
			body.Spine= EditorGUILayout.ObjectField("Spine ",body.Spine, typeof(GameObject), true) as GameObject;
			body.ShoulderCenter= EditorGUILayout.ObjectField("Shoulder Center",body.ShoulderCenter, typeof(GameObject), true) as GameObject;
			body.Head= EditorGUILayout.ObjectField("Head",body.Head, typeof(GameObject), true) as GameObject;
			body.CollarLeft= EditorGUILayout.ObjectField("Collar Left",body.CollarLeft, typeof(GameObject), true) as GameObject;
			body.ShoulderLeft= EditorGUILayout.ObjectField("Shoulder Left",body.ShoulderLeft, typeof(GameObject), true) as GameObject;
			body.ElbowLeft= EditorGUILayout.ObjectField("Elbow Left",body.ElbowLeft, typeof(GameObject), true) as GameObject;
			body.WristLeft= EditorGUILayout.ObjectField("Wrist Left",body.WristLeft, typeof(GameObject), true) as GameObject;
			body.HandLeft= EditorGUILayout.ObjectField("Hand Left",body.HandLeft, typeof(GameObject), true) as GameObject;
			body.FingerLeft= EditorGUILayout.ObjectField("Finger Left",body.FingerLeft, typeof(GameObject), true) as GameObject;
			body.CollarRight= EditorGUILayout.ObjectField("Collar Right",body.CollarRight, typeof(GameObject), true) as GameObject;
			body.ShoulderRight= EditorGUILayout.ObjectField("Shoulder Right",body.ShoulderRight, typeof(GameObject), true) as GameObject;
			body.ElbowRight= EditorGUILayout.ObjectField("Elbow Right",body.ElbowRight, typeof(GameObject), true) as GameObject;
			body.WristRight= EditorGUILayout.ObjectField("Wrist Right",body.WristRight, typeof(GameObject), true) as GameObject;
			body.HandRight= EditorGUILayout.ObjectField("Hand Right",body.HandRight, typeof(GameObject), true) as GameObject;
			body.FingerRight= EditorGUILayout.ObjectField("Finger Right",body.FingerRight, typeof(GameObject), true) as GameObject;
			body.HipLeft= EditorGUILayout.ObjectField("Hip Left",body.HipLeft, typeof(GameObject), true) as GameObject;
			body.ThighLeft= EditorGUILayout.ObjectField("Thigh Left",body.ThighLeft, typeof(GameObject), true) as GameObject;
			body.KneeLeft= EditorGUILayout.ObjectField("Knee Left",body.KneeLeft, typeof(GameObject), true) as GameObject;
			body.AnkleLeft= EditorGUILayout.ObjectField("Ankle Left",body.AnkleLeft, typeof(GameObject), true) as GameObject;
			body.FootLeft= EditorGUILayout.ObjectField("Foot Left",body.FootLeft, typeof(GameObject), true) as GameObject;
			body.HipRight= EditorGUILayout.ObjectField("Hip Right",body.HipRight, typeof(GameObject), true) as GameObject;
			body.ThighRight= EditorGUILayout.ObjectField("Thigh Right",body.ThighRight, typeof(GameObject), true) as GameObject;
			body.KneeRight= EditorGUILayout.ObjectField("Knee Right",body.KneeRight, typeof(GameObject), true) as GameObject;
			body.AnkleRight= EditorGUILayout.ObjectField("Ankle Right",body.AnkleRight, typeof(GameObject), true) as GameObject;
			body.FootRight= EditorGUILayout.ObjectField("Foot Right",body.FootRight, typeof(GameObject), true) as GameObject;


			if(body.HipLeft==null && body.ThighLeft==null && body.HipRight==null &&body.ThighRight==null  )
			{

				EditorGUILayout.HelpBox("Thight == Hip (mose of the time), " +
					"but thigh is general use in skeleton for the rotation." +
					" Be sure to set up the one you realy want.", MessageType.Warning);
			}

		}
		if(! Application.isPlaying)
		showRotation = EditorGUILayout.Foldout(showRotation, "Rotation");
		else 
			showRotationInPlayMode = EditorGUILayout.Foldout(showRotationInPlayMode, "Rotation  (PM)");

		if((showRotation &&  ! Application.isPlaying) || (showRotationInPlayMode &&   Application.isPlaying))
		{

			body.RotRootPosition= EditorGUILayout.ObjectField("Root Position",body.RotRootPosition, typeof(GameObject), true) as GameObject;
			body.RotHipCenter= EditorGUILayout.ObjectField("Hip Center",body.RotHipCenter, typeof(GameObject), true) as GameObject;
			body.RotSpine= EditorGUILayout.ObjectField("Spine ",body.RotSpine, typeof(GameObject), true) as GameObject;
			body.RotShoulderCenter= EditorGUILayout.ObjectField("Shoulder Center",body.RotShoulderCenter, typeof(GameObject), true) as GameObject;
			body.RotHead= EditorGUILayout.ObjectField("Head",body.RotHead, typeof(GameObject), true) as GameObject;
			body.RotCollarLeft= EditorGUILayout.ObjectField("Collar Left",body.RotCollarLeft, typeof(GameObject), true) as GameObject;
			body.RotShoulderLeft= EditorGUILayout.ObjectField("Shoulder Left",body.RotShoulderLeft, typeof(GameObject), true) as GameObject;
			body.RotElbowLeft= EditorGUILayout.ObjectField("Elbow Left",body.RotElbowLeft, typeof(GameObject), true) as GameObject;
			body.RotWristLeft= EditorGUILayout.ObjectField("Wrist Left",body.RotWristLeft, typeof(GameObject), true) as GameObject;
			body.RotHandLeft= EditorGUILayout.ObjectField("Hand Left",body.RotHandLeft, typeof(GameObject), true) as GameObject;
			body.RotFingerLeft= EditorGUILayout.ObjectField("Finger Left",body.RotFingerLeft, typeof(GameObject), true) as GameObject;
			body.RotCollarRight= EditorGUILayout.ObjectField("Collar Right",body.RotCollarRight, typeof(GameObject), true) as GameObject;
			body.RotShoulderRight= EditorGUILayout.ObjectField("Shoulder Right",body.RotShoulderRight, typeof(GameObject), true) as GameObject;
			body.RotElbowRight= EditorGUILayout.ObjectField("Elbow Right",body.RotElbowRight, typeof(GameObject), true) as GameObject;
			body.RotWristRight= EditorGUILayout.ObjectField("Wrist Right",body.RotWristRight, typeof(GameObject), true) as GameObject;
			body.RotHandRight= EditorGUILayout.ObjectField("Hand Right",body.RotHandRight, typeof(GameObject), true) as GameObject;
			body.RotFingerRight= EditorGUILayout.ObjectField("Finger Right",body.RotFingerRight, typeof(GameObject), true) as GameObject;
			body.RotHipLeft= EditorGUILayout.ObjectField("Hip Left",body.RotHipLeft, typeof(GameObject), true) as GameObject;
			body.RotThighLeft= EditorGUILayout.ObjectField("Thigh Left",body.RotThighLeft, typeof(GameObject), true) as GameObject;
			body.RotKneeLeft= EditorGUILayout.ObjectField("Knee Left",body.RotKneeLeft, typeof(GameObject), true) as GameObject;
			body.RotAnkleLeft= EditorGUILayout.ObjectField("Ankle Left",body.RotAnkleLeft, typeof(GameObject), true) as GameObject;
			body.RotFootLeft= EditorGUILayout.ObjectField("Foot Left",body.RotFootLeft, typeof(GameObject), true) as GameObject;
			body.RotHipRight= EditorGUILayout.ObjectField("Hip Right",body.RotHipRight, typeof(GameObject), true) as GameObject;
			body.RotThighRight= EditorGUILayout.ObjectField("Thigh Right",body.RotThighRight, typeof(GameObject), true) as GameObject;
			body.RotKneeRight= EditorGUILayout.ObjectField("Knee Right",body.RotKneeRight, typeof(GameObject), true) as GameObject;
			body.RotAnkleRight= EditorGUILayout.ObjectField("Ankle Right",body.RotAnkleRight, typeof(GameObject), true) as GameObject;
			body.RotFootRight= EditorGUILayout.ObjectField("Foot Right",body.RotFootRight, typeof(GameObject), true) as GameObject;
			
			if(body.RotHipLeft==null && body.RotThighLeft==null && body.RotHipRight==null &&body.RotThighRight==null  )
			{
				
				EditorGUILayout.HelpBox( "Thigh is general use in skeleton for the rotation." +
				                        " If not in your skeleton, just do not use it.", MessageType.Warning);
			}
		}
		showMoreInfo = EditorGUILayout.Foldout(showMoreInfo, "Plus");
		if(showMoreInfo) {
			body.DrawDebugMan = EditorGUILayout.Toggle("Draw Debug Man" , body.DrawDebugMan);
			if (GUILayout.Button ("Auto Complete")) 
			{
			AutoCompleted(body, body.Get (be.primd.toolkit.bodytracker.v1.BodyPart.Root));
			}

		}

		showPointerPart = EditorGUILayout.Foldout (showPointerPart, "Hand & Head");
		if (showPointerPart) {
		
			body.trackedHead= EditorGUILayout.ObjectField("Head", body.trackedHead, typeof(Body.BodyHead),true) as Body.BodyHead;
			body.trackedLeftHand= EditorGUILayout.ObjectField("Left hand", body.trackedLeftHand, typeof(Body.BodyHand),true) as Body.BodyHand;
			body.trackedRightHand= EditorGUILayout.ObjectField("Right hand", body.trackedRightHand, typeof(Body.BodyHand),true) as Body.BodyHand;
		}


	}



	public void AutoCompleted(Body body, GameObject root)
	{
		Debug.Log ("Try to auto complete");



	}

}
