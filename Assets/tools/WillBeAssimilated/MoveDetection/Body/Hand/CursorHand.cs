using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class CursorHand : Body.BodyHand {

	public Cursor cursor;

    /**If you are using a hydra , a sony mover or else, that give you the opportunity to have a precise rotation of the hand attache to a empty point.
     you can attach this poin to hand rotator to have a mixe between precise rotation and cursor advantage*/
    public Transform handRotator;


	public void Start(){
		if (cursor == null) {
			Debug.Log("The cursor hand ("+this.gameObject+") has not cursor reference. >> Auto Destroy.");
			Destroy(this);
		}
	}

	public override HandState GetState ()
	{
        if (cursor.IsHitting()) return HandState.PointingAt;
		return HandState.Open;
	}

	public override Vector3 GetDirection ()
	{
        if (this.handRotator != null) return this.handRotator.rotation.eulerAngles;
		return cursor.GetLastDirection ();

	}

	public override Vector3 GetPosition ()
	{
        if (handRotator != null) return handRotator.position;
		return cursor.GetOrigine ();
	}

	public override bool IsPointingAtSomething ()
	{
		if (cursor == null)
						return false;
		return cursor.GetSelected ()!=null;
	}

	public override GameObject GetPointingObject ()
	{
		if (cursor == null)
			return null;
		return cursor.GetSelected ();
	}

	public override Vector3 GetPointingPosition ()
	{
		return cursor.GetLastPosition ();
	}
	}
}
