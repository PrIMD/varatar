using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class OculusHead : Body.BodyHead {

    public Transform cameraLeft;
    public Transform cameraRight;
    public Cursor headPointer;

    public bool IsValide(bool withWarningMessage =false) {
        bool isOk =  cameraLeft != null && cameraRight != null ;
        if(!isOk && withWarningMessage)
        Debug.Log("You try to use a not correctly initilised OculusHead");
        return isOk;
    }


    public override Vector3 GetDirection()
    {
        if (!IsValide(true)) { return Vector3.zero; }
        return (cameraLeft.position + cameraRight.position) / 2f- transform.position;
    }

    public override Vector3 GetPosition()
    {
       return  transform.position;
    }

    public override bool IsPointingAtSomething()
    {
        if (headPointer == null) return false;
        return headPointer.IsHitting();

    }

    public override GameObject GetPointingObject()
    {
        if (headPointer == null) return null;
        return headPointer.GetSelected(); ;

    }

    public override Vector3 GetPointingPosition()
    {
        return headPointer.GetLastPosition();
 
		}
}
}