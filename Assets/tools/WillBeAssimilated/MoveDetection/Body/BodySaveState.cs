﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace be.primd.toolkit.bodytracker.v1{


public class BodySaveState {

	public readonly string savePurpose;
	public readonly Body cameFromBody;
	public readonly Dictionary <BodyPart, Vector3> bodyPosition ;
	public readonly Dictionary <BodyPart, Vector3> bodyEulerRotation;

	public BodySaveState( Body bodyToSave, string savePurpose)
	{
		if (bodyToSave == null || savePurpose == null)
						throw new NullReferenceException ();

		this.cameFromBody = bodyToSave;
		this.savePurpose = savePurpose;
		
		  bodyPosition = new Dictionary < BodyPart, Vector3 >();
		bodyEulerRotation = new Dictionary < BodyPart, Vector3 >();
		Dictionary< BodyPart,GameObject> pos = bodyToSave.GetBodyPartPosition ();
		Dictionary< BodyPart,GameObject> rot = bodyToSave.GetBodyPartRotation ();
		foreach(KeyValuePair< BodyPart,GameObject> entry in pos)
		{
			bodyPosition.Add(entry.Key,entry.Value.transform.position);


		}
		foreach(KeyValuePair< BodyPart,GameObject> entry in rot)
		{	
			bodyEulerRotation.Add(entry.Key,entry.Value.transform.localEulerAngles);
		}

	}

	public Vector3 GetLocalEulerRotation(BodyPart bp)
	{ Vector3 erv = new Vector3 ();

		bodyEulerRotation.TryGetValue (bp, out erv);
		return erv;
	}
}
}