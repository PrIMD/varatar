﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace be.primd.toolkit.bodytracker.v1{
public abstract class Body : MonoBehaviour {

	
	
	[HideInInspector]
	public bool DrawDebugMan = true;
	private bool hasBeenInitialize;
	
	[HideInInspector]
	public GameObject Root;
	
	[HideInInspector]
	public GameObject HipCenter;
   [HideInInspector]
	public GameObject Spine;
   [HideInInspector]
	public GameObject ShoulderCenter;
   [HideInInspector]
	public GameObject Head;
   [HideInInspector]
	public GameObject CollarLeft;
   [HideInInspector]
	public GameObject ShoulderLeft;
   [HideInInspector]
	public GameObject ElbowLeft;
   [HideInInspector]
	public GameObject WristLeft;
   [HideInInspector]
	public GameObject HandLeft;
   [HideInInspector]
	public GameObject FingerLeft;
   [HideInInspector]
	public GameObject CollarRight;
   [HideInInspector]
	public GameObject ShoulderRight;
   [HideInInspector]
	public GameObject ElbowRight;
   [HideInInspector]
	public GameObject WristRight;
   [HideInInspector]
	public GameObject HandRight;
   [HideInInspector]
	public GameObject FingerRight;
   [HideInInspector]
	public GameObject HipLeft;
	[HideInInspector]
	public GameObject ThighLeft;
   [HideInInspector]
	public GameObject KneeLeft;
   [HideInInspector]
	public GameObject AnkleLeft;
   [HideInInspector]
	public GameObject FootLeft;
   [HideInInspector]
	public GameObject HipRight;
	[HideInInspector]
	public GameObject ThighRight;
   [HideInInspector]
	public GameObject KneeRight;
   [HideInInspector]
	public GameObject AnkleRight;
   [HideInInspector]
	public GameObject FootRight;
	
	[HideInInspector]
	public GameObject RotRootPosition;
	[HideInInspector]
	public GameObject RotHipCenter;
	[HideInInspector]
	public GameObject RotSpine;
	[HideInInspector]
	public GameObject RotShoulderCenter;
	[HideInInspector]
	public GameObject RotHead;
	[HideInInspector]
	public GameObject RotCollarLeft;
	[HideInInspector]
	public GameObject RotShoulderLeft;
	[HideInInspector]
	public GameObject RotElbowLeft;
	[HideInInspector]
	public GameObject RotWristLeft;
	[HideInInspector]
	public GameObject RotHandLeft;
	[HideInInspector]
	public GameObject RotFingerLeft;
	[HideInInspector]
	public GameObject RotCollarRight;
	[HideInInspector]
	public GameObject RotShoulderRight;
	[HideInInspector]
	public GameObject RotElbowRight;
	[HideInInspector]
	public GameObject RotWristRight;
	[HideInInspector]
	public GameObject RotHandRight;
	[HideInInspector]
	public GameObject RotFingerRight;
	[HideInInspector]
	public GameObject RotHipLeft;
	[HideInInspector]
	public GameObject RotThighLeft;
	[HideInInspector]
	public GameObject RotKneeLeft;
	[HideInInspector]
	public GameObject RotAnkleLeft;
	[HideInInspector]
	public GameObject RotFootLeft;
	[HideInInspector]
	public GameObject RotHipRight;
	[HideInInspector]
	public GameObject RotThighRight;
	[HideInInspector]
	public GameObject RotKneeRight;
	[HideInInspector]
	public GameObject RotAnkleRight;
	[HideInInspector]
	public GameObject RotFootRight;

	/*Quick access to the body part O(1) (must not contain null data*/
	private Dictionary <BodyPart,GameObject> quickAccessBodyParts = new Dictionary<BodyPart,GameObject> ();
	/*Quick access to the body part O(1) (must not contain null data*/
	private Dictionary <BodyPart,GameObject> quickAccessRotationBodyParts = new Dictionary<BodyPart,GameObject> ();

	public BodyHead trackedHead;
	public BodyHand trackedLeftHand;
	public BodyHand trackedRightHand;




	#pragma warning disable 414
	private BodySaveState initialSave;

	/**Take the game oject set up in the public member and add them to the lists in aim to be use in the body class*/
	public void InitFormUnityToTheClass(){
		quickAccessBodyParts.Clear ();
		quickAccessRotationBodyParts.Clear ();

		initialSave = new BodySaveState (this,"Initial Position");

		//add game object of the body to quick access bodyParts that is O(1) access table.
	
		if( Root !=null)
		quickAccessBodyParts.Add( BodyPart.Root,Root);
		if( Head !=null)
		quickAccessBodyParts.Add( BodyPart.Head, Head);
		if( ShoulderCenter !=null)
		quickAccessBodyParts.Add( BodyPart.CenterShoulders, ShoulderCenter);
		if( Spine !=null)
		quickAccessBodyParts.Add( BodyPart.Spine, Spine);
		if( HipCenter !=null)
		quickAccessBodyParts.Add( BodyPart.CenterHips, HipCenter);
		if( CollarLeft !=null)
		quickAccessBodyParts.Add( BodyPart.CollarLeft, CollarLeft);
		if( ShoulderLeft !=null)
		quickAccessBodyParts.Add( BodyPart.ShoulderLeft, ShoulderLeft);
		if( ElbowLeft !=null)
		quickAccessBodyParts.Add( BodyPart.ElbowLeft, ElbowLeft);
		if( WristLeft !=null)
		quickAccessBodyParts.Add( BodyPart.WristLeft, WristLeft);
		if( HandLeft !=null)
		quickAccessBodyParts.Add( BodyPart.HandLeft, HandLeft);
		if( FingerLeft !=null)
		quickAccessBodyParts.Add( BodyPart.FingerLeft, FingerLeft);
		if( CollarRight !=null)
		quickAccessBodyParts.Add( BodyPart.CollarRight, CollarRight);
		if( ShoulderRight !=null)
		quickAccessBodyParts.Add( BodyPart.ShoulderRight, ShoulderRight);
		if( ElbowRight !=null)
		quickAccessBodyParts.Add( BodyPart.ElbowRight, ElbowRight);
		if( WristRight !=null)
		quickAccessBodyParts.Add( BodyPart.WristRight, WristRight);
		if( HandRight !=null)
		quickAccessBodyParts.Add( BodyPart.HandRight, HandRight);
		if( FingerRight !=null)
		quickAccessBodyParts.Add( BodyPart.FingerRight, FingerRight);
		
		if( ThighLeft !=null)
			quickAccessBodyParts.Add( BodyPart.ThighLeft, ThighLeft);
		if( HipLeft !=null)
		quickAccessBodyParts.Add( BodyPart.HipLeft, HipLeft);
		if( KneeLeft !=null)
		quickAccessBodyParts.Add( BodyPart.KneeLeft, KneeLeft);
		if( AnkleLeft !=null)
		quickAccessBodyParts.Add( BodyPart.AnkleLeft, AnkleLeft);
		if( FootLeft !=null)
			quickAccessBodyParts.Add( BodyPart.FootLeft, FootLeft);
		if( ThighRight !=null)
			quickAccessBodyParts.Add( BodyPart.ThighRight, ThighRight);
		if( HipRight !=null)
			quickAccessBodyParts.Add( BodyPart.HipRight, HipRight);
		if( KneeRight !=null)
		quickAccessBodyParts.Add( BodyPart.KneeRight, KneeRight);
		if( AnkleRight !=null)
		quickAccessBodyParts.Add( BodyPart.AnkleRight, AnkleRight);
		if( FootRight !=null)
		quickAccessBodyParts.Add( BodyPart.FootRight, FootRight);

		
		if( RotRootPosition !=null)
			quickAccessRotationBodyParts.Add( BodyPart.Root,RotRootPosition	);
		if( RotHead !=null)
		quickAccessRotationBodyParts.Add( BodyPart.Head, RotHead);
		if( RotShoulderCenter !=null)
		quickAccessRotationBodyParts.Add( BodyPart.CenterShoulders, RotShoulderCenter);
		if( RotSpine !=null)
		quickAccessRotationBodyParts.Add( BodyPart.Spine, RotSpine);
		if( RotHipCenter !=null)
		quickAccessRotationBodyParts.Add( BodyPart.CenterHips, RotHipCenter);
		if( RotCollarLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.CollarLeft, RotCollarLeft);
		if( RotShoulderLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.ShoulderLeft, RotShoulderLeft);
		if( RotElbowLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.ElbowLeft, RotElbowLeft);
		if( RotWristLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.WristLeft, RotWristLeft);
		if( RotHandLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.HandLeft, RotHandLeft);
		if( RotFingerLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.FingerLeft, RotFingerLeft);
		if( RotCollarRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.CollarRight, RotCollarRight);
		if( RotShoulderRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.ShoulderRight, RotShoulderRight);
		if( RotElbowRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.ElbowRight, RotElbowRight);
		if( RotWristRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.WristRight, RotWristRight);
		if( RotHandRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.HandRight, RotHandRight);
		if( RotFingerRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.FingerRight, RotFingerRight);
		if( RotThighLeft !=null)
			quickAccessRotationBodyParts.Add( BodyPart.ThighLeft, RotThighLeft);
		if( RotHipLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.HipLeft, RotHipLeft);
		if( RotKneeLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.KneeLeft, RotKneeLeft);
		if( RotAnkleLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.AnkleLeft, RotAnkleLeft);
		if( RotFootLeft !=null)
		quickAccessRotationBodyParts.Add( BodyPart.FootLeft, RotFootLeft);
		if( RotThighRight !=null)
			quickAccessRotationBodyParts.Add( BodyPart.ThighRight, RotThighRight);
		if( RotHipRight !=null)
			quickAccessRotationBodyParts.Add( BodyPart.HipRight, RotHipRight);
		if( RotKneeRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.KneeRight, RotKneeRight);
		if( RotAnkleRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.AnkleRight, RotAnkleRight);
		if( RotFootRight !=null)
		quickAccessRotationBodyParts.Add( BodyPart.FootRight, RotFootRight);
		
		hasBeenInitialize = true;
	}

	public bool IsExisting(BodyPart bp, bool thePositionOne=true, bool theRotationOne=true){

		bool isOkForThePosition=false;
		bool isOkForTheRotation=false;
		if (thePositionOne)
			isOkForThePosition = quickAccessBodyParts.ContainsKey (bp);
		if (theRotationOne)
			isOkForTheRotation = quickAccessRotationBodyParts.ContainsKey (bp);

		if (theRotationOne && thePositionOne)
			return isOkForThePosition && isOkForTheRotation;
		else if (thePositionOne)
			return isOkForThePosition;
		else if (theRotationOne)
			return isOkForTheRotation;

		return false;

	}
	/**Return the game object corresponding at the body part in the params*/
	public GameObject Get(BodyPart bp)
	{
		return GetPositionGameObjectOf(bp);
	}
	
	/**Return the game object corresponding at the body part in the params*/
	public GameObject GetPositionGameObjectOf(BodyPart bp)
	{
		GameObject value=null;
			quickAccessBodyParts.TryGetValue (bp,out value);

		return value;
		
	}
	/**Return the game object corresponding at the body part in the params*/
	public GameObject GetRotation(BodyPart bp )
	{
		GameObject value=null;
			quickAccessRotationBodyParts.TryGetValue (bp,out value);
		
		return value;
		
	}

	public Vector3 GetEulerRotation(BodyPart bp, bool local=false){
		
		GameObject value=GetRotation (bp);
		if (value == null)
						throw new BodyPartNotDefineException (bp);

			if(local)
				return value.transform.localRotation.eulerAngles;	
			else 
				return value.transform.rotation.eulerAngles;
	
	
	}

	/**Return the distance between the two body parts*/
	public float GetDistanceBetween(BodyPart bodyPartOne,BodyPart bodyPartTwo,CompareTo compareTo=CompareTo.World, bool ignoreAxeX=false, bool ignoreAxeY=false, bool ignoreAxeZ=false)
	{
		GameObject gameoOne = Get (bodyPartOne);
		GameObject gameoTwo = Get (bodyPartTwo);
		if (gameoOne != null && gameoTwo != null) {
						Vector3 one = GetPosition (gameoOne);
						Vector3 two = GetPosition (gameoTwo);

					if(CompareTo.Root.Equals(compareTo) || CompareTo.Body.Equals(compareTo))
					{
						GameObject root = null;
						Quaternion rot ;
						Vector3 pos;
						if(CompareTo.Root.Equals(compareTo)){
							root =	Get (BodyPart.Root);
							rot = root.transform.rotation;
							pos = root.transform.position;
						}
							
						else {
							root = GetRotation (BodyPart.Spine);
							rot = root.transform.rotation;
							pos = root.transform.position;
						}

							if(root==null) throw new BodyPartNotDefineException("Root not define, GetDistanceBetween with body compare, need to have a root, but the root is not define.");
							one = RelocateOnCartesianAxe(one,pos,rot);
							two = RelocateOnCartesianAxe(two,pos,rot);
						}

						if(ignoreAxeX)
						{
							one.x=0f;two.x=0f;
						}
						if(ignoreAxeY)
						{
							one.y=0f;two.y=0f;
						}
						if(ignoreAxeZ)
						{
							one.z=0f;two.z=0f;
						}




						return Vector3.Distance (one, two);
				} else {
						Debug.Log("Warning !!! \"NullPointerException\": You try to use a BodyPart you do not define");
						return 0;
				}
	}
	



	/**Return the distance between the two body parts*/
	public float GetDistanceRatioBetween(BodyPart bodyPartOne,BodyPart bodyPartTwo, BodyMajorBone compartor,CompareTo compareTo=CompareTo.World, bool ignoreAxeX=false, bool ignoreAxeY=false, bool ignoreAxeZ=false)
	{
		float dComparetor  =	GetDistance(compartor);
		float dDistance =	GetDistanceBetween(bodyPartOne,bodyPartTwo, compareTo, ignoreAxeX, ignoreAxeY,ignoreAxeZ);
		if (dComparetor == 0 && !IsInitialize()) {
			Debug.LogError("Distance of comparator, should not be null, do the body has been initiliaze");
						return 0;
				}
		return dDistance / dComparetor;

	}
	/**Return the angle (in degree) made boy the triangle association of three body parts*/
	public float GetAngleBetween(BodyPart shoulder, BodyPart elbow,BodyPart wrist)
	{
		if (! IsInitialize ()) {
						Debug.LogWarning ("Can be use until the class is initiliaze, plus check it IsInitialize() before use GetAngleBetween");	
			return 0;
				} else {
						try {
								Vector3 vShoulder = GetPosition (shoulder);
								Vector3 vElbow = GetPosition (elbow);
								Vector3 vWrist = GetPosition (wrist);
								Vector3 ptOne = vShoulder - vElbow;
								Vector3 ptTwo = vWrist - vElbow;
			
								return Vector3.Angle (ptOne, ptTwo);

						} catch (BodyPartNotDefineException e) {
								throw new Exception ("Warning, to be able to use AngleBetween methode, the 3 bodyparts have to be set(a,root,b). Missing at least : " + e.bodyPart);
						}
				}
	}

	public Vector3 GetDirectionBasedOnRoot( bool ignoreAxeX=false, bool ignoreAxeY=false, bool ignoreAxeZ=false){
		Vector3 direction;
		GameObject root = Get (BodyPart.Root);
		GameObject spine= Get (BodyPart.Spine);

		if (root != null && spine != null) {

			direction = spine.transform.position-root.transform.position;
			Ignore(ref direction, ignoreAxeX,ignoreAxeY , ignoreAxeZ);

		} else {
				direction = Vector3.zero;
		}

		return direction;
	}

	public void Ignore(ref Vector3 vect,  bool ignoreAxeX, bool ignoreAxeY, bool ignoreAxeZ){

				if (ignoreAxeX) {
						vect.x = 0f;
				}
				if (ignoreAxeY) {
						vect.y = 0f;
						;
				}
				if (ignoreAxeZ) {
						vect.z = 0f;
				}
		}


	/**Return the enum of the body part that is the less far of the given params vector*/
	public BodyPart GetTheCloserBodyPartOf(Vector3 worldspacepoint)
	{
		BodyPart closer = BodyPart.Spine;
		float dist = 100000f; 
		foreach(KeyValuePair<BodyPart, GameObject> entry in quickAccessBodyParts)
		{
			if(entry.Key!=BodyPart.Root){

				Vector3 gamoPosition = entry.Value.transform.position;
				float distBetween = Vector3.Distance(worldspacepoint, gamoPosition);

				if(distBetween<dist)
				{
					closer	= entry.Key;
					dist	= distBetween; 
				}
			}
		}

		return closer;
	}

	public BodyPart GetTheFarestBodyPartFrom(BodyPart bp =  BodyPart.Spine)
	{
		
		BodyPart farest = bp;
		float dist = 0f; 
		foreach(KeyValuePair<BodyPart, GameObject> entry in quickAccessBodyParts)
		{
			if(entry.Key!=BodyPart.Root){
				float distBetween = GetDistanceBetween(bp,entry.Key);
				if(distBetween>dist)
				{
					farest	= entry.Key;
					dist	= distBetween; 
				}
			}
		}
		
		return farest;
	}








	
	protected static Body INSTANCE;
	protected static List<Body> OTHERINSTANCES;
	public static List<Body> OtherInstances
	{
		get{
			if(OTHERINSTANCES==null){OTHERINSTANCES = new List<Body>();}
			return OTHERINSTANCES;
		}
	}
	public static Body Instance
	{
		get {return INSTANCE;}
		set {
			if(INSTANCE==null)
			INSTANCE=value;
			else
			{
				if(OTHERINSTANCES==null){OTHERINSTANCES = new List<Body>();}
				OTHERINSTANCES.Add(value);
				
			} 
		}
	}

	public void Awake()
	{
		Instance = this;
		InitFormUnityToTheClass ();
	}



	protected void DrawDebugBody(float during = 0f)
	{

		/**if (IsExisting(BodyPart.Head,true,true) ){
			Transform  tr = Get (BodyPart.WristLeft).transform;

			DrawPoint(tr,GetEulerRotation(BodyPart.WristLeft,true,true),5);
		}*/
		DrawLineBetween (BodyPart.Head,BodyPart.CenterShoulders);
		DrawLineBetween (BodyPart.CenterShoulders,BodyPart.Spine);
		DrawLineBetween (BodyPart.Spine,BodyPart.CenterHips);
		DrawLineBetween (BodyPart.Spine,BodyPart.CollarLeft);
		DrawLineBetween (BodyPart.Spine,BodyPart.CollarRight);
		DrawLineBetween (BodyPart.CenterHips,BodyPart.HipLeft);
		DrawLineBetween (BodyPart.CenterHips,BodyPart.HipRight);

		//Left Leg
		DrawLineBetween (BodyPart.HipLeft,BodyPart.KneeLeft);
		DrawLineBetween (BodyPart.KneeLeft,BodyPart.AnkleLeft);
		DrawLineBetween (BodyPart.AnkleLeft,BodyPart.FootLeft);
		//Right leg
		DrawLineBetween (BodyPart.HipRight,BodyPart.KneeRight);
		DrawLineBetween (BodyPart.KneeRight,BodyPart.AnkleRight);
		DrawLineBetween (BodyPart.AnkleRight,BodyPart.FootRight);

		//Left arm
		DrawLineBetween (BodyPart.CollarLeft,BodyPart.ShoulderLeft);
		DrawLineBetween (BodyPart.ShoulderLeft,BodyPart.ElbowLeft);
		DrawLineBetween (BodyPart.ElbowLeft,BodyPart.WristLeft);
		DrawLineBetween (BodyPart.WristLeft,BodyPart.HandLeft);
		DrawLineBetween (BodyPart.HandLeft,BodyPart.FingerLeft);
		//Right arm
		DrawLineBetween (BodyPart.CollarRight,BodyPart.ShoulderRight);
		DrawLineBetween (BodyPart.ShoulderRight,BodyPart.ElbowRight);
		DrawLineBetween (BodyPart.ElbowRight,BodyPart.WristRight);
		DrawLineBetween (BodyPart.WristRight,BodyPart.HandRight);
		DrawLineBetween (BodyPart.HandRight,BodyPart.FingerRight);

		DrawLineBetween (BodyPart.ShoulderLeft, BodyPart.ShoulderRight, Color.green);
		DrawLineBetween (BodyPart.ShoulderLeft, BodyPart.HipLeft, Color.green);
		DrawLineBetween (BodyPart.ShoulderRight, BodyPart.HipRight, Color.green);
		DrawLineBetween (BodyPart.HipRight, BodyPart.HipLeft, Color.green);


	}
	/**Draw a line between two body part if the body part are not null*/
	public void DrawLineBetween(BodyPart ptOne, BodyPart ptTwo, float time=0){
		DrawLineBetween (ptOne, ptTwo, Color.white, time);
	}

	/**Draw a line between two body part if the body part are not null*/
	public void DrawLineBetween(BodyPart ptOne, BodyPart ptTwo, Color color, float time=0)
	{
		GameObject firstGameObject =Get ( ptOne ) ;
		GameObject secondGameObject =Get ( ptTwo ) ;
		if(firstGameObject!=null && secondGameObject!=null)
		{
			Debug.DrawLine (GetPosition(firstGameObject),GetPosition(secondGameObject)
			                ,color,time);
		}
	}

	public void DrawPoint(Transform point,Vector3 eulerRotation, float distance){
	
		if (point != null) {
			Vector3 ptOne, ptTwo;
			
			ptOne = point.position;
			ptOne.x -= distance;
			ptTwo = point.position;
			ptTwo.x += distance;
			Debug.DrawLine (ptOne, ptTwo, Color.red);
			
			ptOne = point.position;
			ptOne.y -= distance;
			ptTwo = point.position;
			ptTwo.y += distance;
			Debug.DrawLine (ptOne, ptTwo, Color.red);
			
			
			ptOne = point.position;
			ptOne.z -= distance;
			ptTwo = point.position;
			ptTwo.z += distance;
			Debug.DrawLine (ptOne, ptTwo, Color.red);

				ptOne = point.position;
				ptTwo = point.position;
				ptTwo += eulerRotation * (distance * 1.4f);
				Debug.DrawLine (ptOne, ptTwo, Color.magenta);




		}

	}

	/**Shortcut that Return the position of the game object*/
	protected Vector3 GetPosition(GameObject gamo){
		if (gamo == null)
			throw new NullReferenceException ();
		return gamo.transform.position;}
	
	/**Shortcut that Return the position of the game object*/
	protected Vector3 GetPosition(BodyPart part){
		GameObject obj = Get (part);
		if (obj == null)
			throw new BodyPartNotDefineException (part);

		return GetPosition(Get(part));}




	
	public bool IsFowardOf(BodyPart bodyPart,BodyPart target){
		Vector3 answer = GetBodyPartDirectionOverTarget (bodyPart, target);
		if (answer.z > 0f)
			return true;
		return false;}
	public bool IsBackOf(BodyPart bodyPart,BodyPart target){
		Vector3 answer = GetBodyPartDirectionOverTarget (bodyPart, target);
		if (answer.z < 0f)
			return true;
		return false;}
	public bool IsUpOf(BodyPart bodyPart,BodyPart target){
		Vector3 answer = GetBodyPartDirectionOverTarget (bodyPart, target);
		if (answer.y > 0f)
			return true;
		return false;}
	public bool IsDownOf(BodyPart bodyPart,BodyPart target){
		Vector3 answer = GetBodyPartDirectionOverTarget (bodyPart, target);
		if (answer.y < 0f)
			return true;
		return false;}
	public bool IsAtLeftOf(BodyPart bodyPart,BodyPart target){
		Vector3 answer = GetBodyPartDirectionOverTarget (bodyPart, target);
		if (answer.x < 0f)
			return true;
		return false;}
	public bool IsAtRightOf(BodyPart bodyPart,BodyPart target){
		Vector3 answer = GetBodyPartDirectionOverTarget (bodyPart, target);
		if (answer.x > 0f)
			return true;
		return false;
	}
	/**Return a vector3 with the direction of body part compare to the target.
	 * Exemple: bodypart is at left at the bottom in front  of target -> Vector(-1,-1,1)*/
	public Vector3 GetBodyPartDirectionOverTarget(BodyPart bodyPart,BodyPart target){
		GameObject rootLocal = Get (BodyPart.Root);
		GameObject a = Get (bodyPart);
		GameObject b = Get (target);
		if (rootLocal == null)
			throw new BodyPartNotDefineException (BodyPart.Root, "The root is required in "+this.gameObject+"("+this +") and is not define in aim to use 'GetBodyPartDirectionOverTarget(...)' :" + this.gameObject);
		if (a == null )
			throw new BodyPartNotDefineException (bodyPart,"One of the bodypart is not define !"); 
		if ( b == null)
			throw new BodyPartNotDefineException (target,"One of the bodypart is not define !"); 
		return GetDirectionOf_A_ComareTo_B (a.transform.position, b.transform.position, rootLocal.transform); 
	}
	/**Return a vector3 with the direction of body part compare to the target.
	 * Exemple: bodypart is at left at the bottom in front  of target -> Vector(-1,-1,1)*/
	protected Vector3 GetDirectionOf_A_ComareTo_B(Vector3 a, Vector3 b, Transform root ){
		return GetDirectionOf_A_ComareTo_B (a, b, root.position, root.rotation);
	}
	/**Return a vector3 with the direction of body part compare to the target.
	 * Exemple: bodypart is at left at the bottom in front  of target -> Vector(-1,-1,1)*/

	protected Vector3 GetDirectionOf_A_ComareTo_B(Vector3 pointA, Vector3 pointB, Vector3 rootPosition, Quaternion rootRotation, bool withErease =true ){
		Vector3 direction = Vector3.zero;
		
		if (pointA == pointB) return Vector3.zero;
		//Recenter the point on cartesions axes
		pointA -= rootPosition;
		pointB -= rootPosition;
		
		//Rotate points, based on the root rotation, in aim to be front of cartesians axes
		//No need to be rotate if the root is already in front of cartesians axes
		if (rootRotation.eulerAngles != Vector3.zero) {
			Quaternion rootRotationInverse = Quaternion.Inverse (rootRotation);
			pointA = RotateAroundPoint (pointA, Vector3.zero, rootRotationInverse);
			pointB = RotateAroundPoint (pointB, Vector3.zero, rootRotationInverse);
		}
		if (withErease) {
						direction.x = pointA.x < pointB.x ? -1 : (pointA.x == pointB.x ? 0 : 1);
						direction.y = pointA.y < pointB.y ? -1 : (pointA.y == pointB.y ? 0 : 1);
						direction.z = pointA.z < pointB.z ? -1 : (pointA.z == pointB.z ? 0 : 1);
				} else {
				
			
			direction.x = pointA.x - pointB.x ;
			direction.y = pointA.y - pointB.y ;
			direction.z = pointA.z - pointB.z ;
		}
		
		return direction;
	}


	protected Vector3 RelocateOnCartesianAxe(Vector3 point, Vector3 rootPosition, Quaternion rootRotation ){

		//Recenter the point on cartesions axes
		point -= rootPosition;

		//Rotate points, based on the root rotation, in aim to be front of cartesians axes
		//No need to be rotate if the root is already in front of cartesians axes
		if (rootRotation.eulerAngles != Vector3.zero) {
			Quaternion rootRotationInverse = Quaternion.Inverse (rootRotation);
			point = RotateAroundPoint (point, Vector3.zero, rootRotationInverse);
		}

		return point;

	}


	protected static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}


	public float GetDistance(BodyMajorBone bone){
		return GetSizeOf (bone);
	}
	/* Return an average of the size between  the bones asked*/
	public float GetSizeOf(BodyMajorBone bone)
	{
		float distance = 0f;
		int boneNumber = 0;

		switch (bone) {
		case BodyMajorBone.Arm: case BodyMajorBone.Arms:
			distance = GetDistanceBetween(BodyPart.ElbowLeft, BodyPart.WristLeft );
			distance += GetDistanceBetween(BodyPart.ElbowLeft, BodyPart.ShoulderLeft );
			distance += GetDistanceBetween(BodyPart.ElbowRight, BodyPart.WristRight );
			distance += GetDistanceBetween(BodyPart.ElbowRight, BodyPart.ShoulderRight );
			boneNumber=BodyMajorBone.Arms.Equals(bone)?2:4;break;
		case BodyMajorBone.Leg: case BodyMajorBone.Legs:
			distance = GetDistanceBetween(BodyPart.KneeLeft, BodyPart.HipLeft);
			distance += GetDistanceBetween(BodyPart.KneeLeft, BodyPart.AnkleLeft);
			distance += GetDistanceBetween(BodyPart.KneeRight, BodyPart.HipRight);
			distance += GetDistanceBetween(BodyPart.KneeRight, BodyPart.AnkleRight);
			boneNumber=BodyMajorBone.Legs.Equals(bone)?2:4;break;

		case BodyMajorBone.Spine:
			distance = GetDistanceBetween(BodyPart.CenterHips, BodyPart.CenterShoulders);
			boneNumber=1;break;
		case BodyMajorBone.Hips:
			distance = GetDistanceBetween(BodyPart.HipLeft, BodyPart.HipRight);
			boneNumber=1; break;
		case BodyMajorBone.Shoulders:
			distance = GetDistanceBetween(BodyPart.ShoulderRight, BodyPart.ShoulderLeft);
			boneNumber=1; break;
		case BodyMajorBone.Neck:
			distance = GetDistanceBetween(BodyPart.CenterShoulders, BodyPart.Head);
			boneNumber=1; break;
		case BodyMajorBone.Body:
			distance= GetDistanceBetween(BodyPart.HipRight, BodyPart.ShoulderRight);
			distance += GetDistanceBetween(BodyPart.HipLeft, BodyPart.ShoulderLeft);
			boneNumber=2; break;
		}

		
		if (boneNumber > 0)
			return distance / ((float)boneNumber);
		return 0f;
	}
	/**Return the distence between two BodyPart that represent the MajorBone.*/
	public float GetSizeOf(BodyMajorBone bone,  bool leftOne, bool botOne=true)
	{
		float distance = 0f;
		int boneNumber = 1;

		switch (bone) {
		case BodyMajorBone.Arm: case BodyMajorBone.Arms:
			if(leftOne){
				if(botOne){
					distance = GetDistanceBetween(BodyPart.ElbowLeft, BodyPart.WristLeft );
				}else{ 
					distance = GetDistanceBetween(BodyPart.ElbowLeft, BodyPart.ShoulderLeft );
				}
			}
			else{
				if(botOne)
				{
					distance = GetDistanceBetween(BodyPart.ElbowRight, BodyPart.WristRight );
				}else{ 
					distance = GetDistanceBetween(BodyPart.ElbowRight, BodyPart.ShoulderRight );
				}
			}
			boneNumber=1;break;
		case BodyMajorBone.Leg: case BodyMajorBone.Legs:
			if(leftOne){
				if(botOne){
					distance = GetDistanceBetween(BodyPart.KneeLeft, BodyPart.AnkleLeft);
				}else{ 
					distance = GetDistanceBetween(BodyPart.KneeLeft, BodyPart.HipLeft);
				}
			}
			else{
				if(botOne)
				{
					distance = GetDistanceBetween(BodyPart.KneeRight, BodyPart.AnkleRight);
				}else{ 
					distance = GetDistanceBetween(BodyPart.KneeRight, BodyPart.HipRight);
				}
			}
			boneNumber=1; break;
		case BodyMajorBone.Spine:
			distance = GetDistanceBetween(BodyPart.CenterHips, BodyPart.CenterShoulders);
			boneNumber=1; break;
		case BodyMajorBone.Hips:
			distance = GetDistanceBetween(BodyPart.HipLeft, BodyPart.HipRight);
			boneNumber=1; break;
		case BodyMajorBone.Shoulders:
			distance = GetDistanceBetween(BodyPart.ShoulderRight, BodyPart.ShoulderLeft);
			boneNumber=1; break;
		case BodyMajorBone.Neck:
			distance = GetDistanceBetween(BodyPart.CenterShoulders, BodyPart.Head);
			boneNumber=1; break;
		case BodyMajorBone.Body:
			distance= GetDistanceBetween(BodyPart.HipRight, BodyPart.ShoulderRight);
			distance += GetDistanceBetween(BodyPart.HipLeft, BodyPart.ShoulderLeft);
			boneNumber=2; break;
		}
		if (boneNumber > 0)
			return distance / ((float)boneNumber);
		return 0f;
	}
	
	public Dictionary< BodyPart,GameObject> GetBodyPartPosition (){return quickAccessBodyParts;}
	public Dictionary< BodyPart,GameObject> GetBodyPartRotation (){return quickAccessRotationBodyParts;}
	public bool IsInitialize(){return hasBeenInitialize;}


	
	public Vector3 GetDirectionNextExtremity (BodyPart bp){
		GameObject gamo  = Get(GetNextExtremity (bp));
		if (gamo != null)
						return gamo.transform.position;
		return Vector3.zero;
	}
	public Vector3 GetDirectionNextBodyPart (BodyPart bp){
		GameObject gamo  = Get(GetNextBodyPart (bp));
		if (gamo != null)
			return gamo.transform.position;
		return Vector3.zero;
	}


	public BodyPart GetNextExtremity(BodyPart bp){

		switch(bp)
		{case BodyPart.ThighLeft:case BodyPart.HipLeft:case BodyPart.KneeLeft:case BodyPart.AnkleLeft:case BodyPart.FootLeft:
			
			if(Get (BodyPart.FootLeft)!=null)	return BodyPart.FootLeft;
			if(Get (BodyPart.AnkleLeft)!=null)	return BodyPart.AnkleLeft;
			if(Get (BodyPart.KneeLeft)!=null)	return BodyPart.KneeLeft;
			if(Get (BodyPart.HipLeft)!=null)	return BodyPart.HipLeft;
			if(Get (BodyPart.ThighLeft)!=null)	return BodyPart.ThighLeft;

			break;

		case BodyPart.ThighRight:case BodyPart.HipRight:case BodyPart.KneeRight:case BodyPart.AnkleRight:case BodyPart.FootRight:
			
			if(Get (BodyPart.FootRight)!=null)	return BodyPart.FootRight;
			if(Get (BodyPart.AnkleRight)!=null)	return BodyPart.AnkleRight;
			if(Get (BodyPart.KneeRight)!=null)	return BodyPart.KneeRight;
			if(Get (BodyPart.HipRight)!=null)	return BodyPart.HipRight;
			if(Get (BodyPart.ThighRight)!=null)	return BodyPart.ThighRight;
			
			break;

		case BodyPart.CollarLeft:case BodyPart.ShoulderLeft:case BodyPart.ElbowLeft:case BodyPart.WristLeft:case BodyPart.HandLeft:
			
			if(Get (BodyPart.CollarLeft)!=null)		return BodyPart.CollarLeft;
			if(Get (BodyPart.ShoulderLeft)!=null)	return BodyPart.ShoulderLeft;
			if(Get (BodyPart.ElbowLeft)!=null)		return BodyPart.ElbowLeft;
			if(Get (BodyPart.WristLeft)!=null)		return BodyPart.WristLeft;
			if(Get (BodyPart.HandLeft)!=null)		return BodyPart.HandLeft;
			
			break;

			
		case BodyPart.CollarRight:case BodyPart.ShoulderRight:case BodyPart.ElbowRight:case BodyPart.WristRight:case BodyPart.HandRight:
			
			if(Get (BodyPart.CollarRight)!=null)		return BodyPart.CollarRight;
			if(Get (BodyPart.ShoulderRight)!=null)	return BodyPart.ShoulderRight;
			if(Get (BodyPart.ElbowRight)!=null)		return BodyPart.ElbowRight;
			if(Get (BodyPart.WristRight)!=null)		return BodyPart.WristRight;
			if(Get (BodyPart.HandRight)!=null)		return BodyPart.HandRight;
			
			break;

		default:break;

		}
		if(Get (BodyPart.Head)!=null)	return BodyPart.Head;
		if(Get (BodyPart.CenterShoulders)!=null)	return BodyPart.CenterShoulders;
		if(Get (BodyPart.Spine)!=null)	return BodyPart.Spine;
		if(Get (BodyPart.Root)!=null)	return BodyPart.Root;
		if(Get (BodyPart.CenterHips)!=null)	return BodyPart.CenterHips;

		return BodyPart.Root;


	}
	
	public static BodyPart GetNextBodyPart(){
		return GetNextBodyPart (BodyPart.Root);
	}
	public static BodyPart GetNextBodyPart(BodyPart bp)
	{
		switch(bp)
		{
		case BodyPart.HandLeft:return BodyPart.HandLeft;
		case BodyPart.HandRight:return BodyPart.HandRight;
		case BodyPart.FootLeft:return BodyPart.FootLeft;
		case BodyPart.FootRight:return BodyPart.FootRight;

		case BodyPart.Head:return BodyPart.Head;
		case BodyPart.CenterShoulders: return BodyPart.Head;
		case BodyPart.Spine: return BodyPart.CenterShoulders;
		case BodyPart.CenterHips:return BodyPart.Spine;
		case BodyPart.Root:return BodyPart.Spine;

		}
		switch(bp)
		{
		case BodyPart.WristLeft:return BodyPart.HandLeft;
		case BodyPart.WristRight:return BodyPart.HandRight;
		case BodyPart.AnkleLeft:return BodyPart.FootLeft;
		case BodyPart.AnkleRight:return BodyPart.FootRight;
		}
		
		switch(bp)
		{
		case BodyPart.ElbowLeft:return BodyPart.WristLeft;
		case BodyPart.ElbowRight:return BodyPart.WristRight;
		case BodyPart.KneeLeft:return BodyPart.AnkleLeft;
		case BodyPart.KneeRight:return BodyPart.AnkleRight;
		}

		switch(bp)
		{
		case BodyPart.ShoulderLeft:return BodyPart.ElbowLeft;
		case BodyPart.ShoulderRight:return BodyPart.ElbowRight;
		case BodyPart.HipLeft:return BodyPart.KneeLeft;
		case BodyPart.HipRight:return BodyPart.KneeRight;
		}
		switch(bp)
		{
		case BodyPart.CollarLeft:return BodyPart.ShoulderLeft;
		case BodyPart.CollarRight:return BodyPart.ShoulderRight;
		case BodyPart.ThighLeft:return BodyPart.HipLeft;
		case BodyPart.ThighRight:return BodyPart.HipRight;
		}

		return BodyPart.Root;
	}

	public Vector3 GetDirectionOf(BodyPart bpLeft ,BodyPart bpRight, BodyPart bpOther)
	{
		Vector3 direction = Vector3.zero;
		
		GameObject gamoLeft=		Get (bpLeft);
		GameObject gamoRight=		Get (bpRight);
		GameObject gamoThirdEdge= 	Get (bpOther);
		
		if (gamoLeft == null || gamoRight == null || gamoThirdEdge == null) {
			throw new BodyPartNotDefineException(new BodyPart[]{bpLeft,bpRight,bpOther});		
		}
		
		Vector3 left=gamoLeft.transform.position;
		Vector3 right=gamoRight.transform.position;
		Vector3 thirdEdge=gamoThirdEdge.transform.position;
		
		Vector3 triCenter = (left + right + thirdEdge * 2f) / 4f;
		
		Vector3 vect1 = left-triCenter;
		Vector3 vect2 = right-triCenter;
		direction = Vector3.Cross( vect2,vect1).normalized;
		
		return direction;
	}

	public Vector3 GetPlayerDirection(BodyPart bpLeft=BodyPart.ShoulderLeft ,BodyPart bpRight= BodyPart.ShoulderRight, BodyPart bpOther= BodyPart.Spine)
	{

		return GetDirectionOf (bpLeft,bpRight,bpOther);
	}


	public abstract class BodyHead : MonoBehaviour, IHead
	{
		
		public abstract  Vector3 GetDirection ();
		public abstract  Vector3 GetPosition ();
		
		public abstract  bool IsPointingAtSomething ();
		
		public abstract  GameObject GetPointingObject ();
		
		public abstract  Vector3 GetPointingPosition ();
		
	}
	public abstract class BodyHand : MonoBehaviour, IHand
	{


		public abstract  HandState GetState ();

		public abstract  Vector3 GetDirection ();
		public abstract  Vector3 GetPosition ();

		public abstract  bool IsPointingAtSomething ();

		public abstract  GameObject GetPointingObject ();

		public abstract  Vector3 GetPointingPosition ();
	}


}
public interface IHead
{
	
	Vector3 GetDirection ();
	Vector3 GetPosition ();
	
	bool IsPointingAtSomething ();
	GameObject GetPointingObject();
	Vector3 GetPointingPosition();

}
public interface IHand
{
	HandState GetState();

	Vector3 GetDirection ();
	Vector3 GetPosition ();

	bool IsPointingAtSomething ();
	GameObject GetPointingObject();
	Vector3 GetPointingPosition();

}
public enum HandState{Open, Close, PointingAt}


public class  BodyPartNotDefineException: Exception

{
		public BodyPart bodyPart;

	public BodyPartNotDefineException (string message ): base(message){
		
	}
	public BodyPartNotDefineException (BodyPart bp ,string message ): base(message +"  (The bodypart : "+bp+")"){
			bodyPart = bp;
	}
	public BodyPartNotDefineException (BodyPart bp ): base("There is a body part not define !  (The bodypart : "+bp+")"){
		bodyPart = bp;
	}
	
	public BodyPartNotDefineException (BodyPart [] bp ): base("There is a body part not define !"){
		string names = "";
		foreach (BodyPart b in bp) {
			names+= " "+b+" ";	
		}
		Debug.LogWarning ("One of those is used by you but not define in the Body class: " + names);
	}
}
public enum BodyPart { Root, Head,CenterShoulders, Spine, CenterHips,
	CollarLeft,ShoulderLeft,ElbowLeft,WristLeft,HandLeft,FingerLeft,
	CollarRight,ShoulderRight,ElbowRight,WristRight,HandRight,FingerRight,
	HipLeft,ThighRight, KneeLeft,AnkleLeft,FootLeft,
	HipRight,ThighLeft ,KneeRight,AnkleRight,FootRight}

public enum BodyMajorBone { Arm, Leg, Arms, Legs, Spine, Hips, Shoulders, Neck, Body  }
public enum CompareTo{World,Root, Body}

}