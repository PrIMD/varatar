﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class LocalisationCondition : BodyCondition {
	
	public BodyPart doA;
	public enum Direction {Left, Right, Top, Down, Front, Back}
	public Direction [ ] at = new Direction[]{Direction.Top};
	public BodyPart ofB;


	public override bool IsConditionComplete ()
	{
		bool isInGoodDirection = true;
		if(CheckAndAutoCompleteValidity() && linkedBody.IsInitialize()){
			Vector3 dir = linkedBody.GetBodyPartDirectionOverTarget (doA,ofB);
			foreach (Direction direction in at) {
				if(direction== Direction.Left && !(dir.x<0f) ) 		isInGoodDirection= false;
				else if(direction== Direction.Right && !(dir.x>0f) ) 	isInGoodDirection=  false;
				else if(direction== Direction.Top && !(dir.y>0f) ) 		isInGoodDirection=  false;
				else if(direction== Direction.Down && !(dir.y<0f) ) 		isInGoodDirection=  false;
				else if(direction== Direction.Front && !(dir.z>0f) ) 	isInGoodDirection=  false;
				else if(direction== Direction.Back && !(dir.z<0f) ) 		isInGoodDirection=  false;
			}
		}

		return isInGoodDirection;
	}
	}}