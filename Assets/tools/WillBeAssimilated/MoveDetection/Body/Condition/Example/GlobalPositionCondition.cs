﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class GlobalPositionCondition : BodyConditionWithLimit {
	
	public BodyPart bodyParts;
	public Transform globalPosition;

	protected override float GetConditionValue ()
	{
		if (linkedBody != null) {
			GameObject bp = linkedBody.Get (bodyParts);
			if(bp!=null && globalPosition!=null)
			{
				return Vector3.Distance(bp.transform.position, globalPosition.position);
			}
		}
		return 10000f;
	}

}
}