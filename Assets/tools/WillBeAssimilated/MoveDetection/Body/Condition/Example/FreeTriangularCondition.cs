﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public  class FreeTriangularCondition : BodyCondition {
	
	public BodyMajorBone  comparator = BodyMajorBone.Shoulders;
	public float approximation=0.3f;


	public BodyPart firstPoint = BodyPart.ShoulderRight;
	public float distanceFirst=1f;
	public BodyPart secondPoint = BodyPart.ShoulderLeft;
	public float distanceSecond=1f;
	public BodyPart thirdPoint = BodyPart.CenterHips;
	public float distanceThrid=1f;

	//public bool withDirection = true;
	//public bool inInverseDirection = false;
	//public BodyPart directionalPoint = BodyPart.HandRight;
	public BodyPart target = BodyPart.HandRight;

	
	public override bool IsConditionComplete ()
	{
		bool isOk;

		if (linkedBody == null)
						return false;

		GameObject first,second, third;
		GameObject /*direction, */target;

		float compDistance = linkedBody.GetDistance (comparator);
		if (compDistance == 0)
						return false;

		//Is all initializate
		first = linkedBody.Get (firstPoint);
		if (first == null)
			return false;
		second = linkedBody.Get (secondPoint);
		if (second == null)
			return false;
		third = linkedBody.Get (thirdPoint);
		if (third == null)
			return false;
//		direction = linkedBody.Get (this.directionalPoint);
//		if ( withDirection && direction == null   )
//			return false;
		target = linkedBody.Get (this.target);
		if (target == null)
			return false;


		isOk = IsDistanceValide (GetDistance (first, target, compDistance), distanceFirst) 
			&& IsDistanceValide (GetDistance (second, target, compDistance), distanceSecond)  
			&& IsDistanceValide (GetDistance (third, target, compDistance), distanceThrid) 
				;
		if (!isOk)
			return false;

	/*	if( withDirection && direction!=null ){

			Vector3 f, s,t ;
			f= first.transform.position;
			s = second.transform.position;
			t = third.transform.position;

			Vector3 triDirection = linkedBody.GetDirectionOf (firstPoint, secondPoint, thirdPoint);
			Vector3 center = (f + s + 2f * t) / 4f;
			Vector3 directionOfDirPoint = direction.transform.position - center;
			float angle = Vector3.Angle (triDirection.normalized, directionOfDirPoint.normalized);
			
			Debug.DrawLine(center, center+triDirection*200f,Color.blue,0.5f);
			Debug.DrawLine(center, center+directionOfDirPoint*200f,Color.cyan,0.5f);

			bool goodDirection = angle>=0f && angle<90f;
			Debug.Log("angle: "+angle+"   good ? "+goodDirection);
			if(inInverseDirection) goodDirection = ! goodDirection;
				
			isOk = isOk && goodDirection;

		}	*/	
		return isOk;
		
	}
	protected float GetDistance( GameObject a, GameObject b, float comparator)
	{
		return Vector3.Distance (a.transform.position, b.transform.position)/comparator;

	}
	
	protected bool IsDistanceValide( float dist, float distNeeded)
	{
		return dist>distNeeded-approximation && dist<distNeeded+approximation;
	}

	public bool IsInGoodDirection()
	{
		
		return true;
	}
	
}

}