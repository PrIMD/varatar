﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class AngleCondition : BodyConditionWithLimit {

	public BodyPart firstEdge;
	public BodyPart pivot;
	public BodyPart secondEdge;


	protected override float GetConditionValue ()
	{
			   return linkedBody.GetAngleBetween(firstEdge,pivot,secondEdge); 
	
	}



}
}