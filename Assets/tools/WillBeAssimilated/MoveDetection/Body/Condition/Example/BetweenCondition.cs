﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class BetweenCondition : BodyCondition {
	
	
	public BodyPart trackedBodyPart = BodyPart.HandRight;
	public BodyPart firstBodyPart= BodyPart.HandLeft;
	public BodyPart secondBodyPart=BodyPart.ElbowLeft;
	public BodyMajorBone comparator = BodyMajorBone.Shoulders;
	public float radius=0.5f;
	public float lastRadius;
	protected override void Start ()
	{
		base.Start ();
	}
	
	public override bool IsConditionComplete ()
	{	bool isComplete = false;
		
		if (linkedBody!=null &&  linkedBody.IsInitialize ()) {
			GameObject first = linkedBody.Get (firstBodyPart);
			GameObject second = linkedBody.Get (secondBodyPart);
			GameObject tracked = linkedBody.Get (trackedBodyPart);
			float comparator = linkedBody.GetDistance(this.comparator);
			if (first != null && second != null && tracked != null) {

				Quaternion firstRotation = new Quaternion();
				Vector3 dir = (second.transform.position-first.transform.position).normalized;
				if(dir!= Vector3.zero)
					firstRotation= Quaternion.LookRotation( dir, Vector3.forward);
				
				Vector3 firstPosition = first.transform.position;
				
				Vector3 vTrackedOnXY = Utility.BasedOnPosition.Relocated (tracked.transform.position, firstPosition, firstRotation, false, false, true);
				
				float pointRadius = Mathf.Sqrt (vTrackedOnXY.x * vTrackedOnXY.x + vTrackedOnXY.y * vTrackedOnXY.y) /comparator;
				lastRadius=pointRadius;
				bool isInRadius = pointRadius < radius;

				if(!isInRadius)return false;

				Vector3 vSecond = Vector3.zero;
				Vector3 vTracked = Vector3.zero;
				//if (isInRadius) {
					vSecond = Utility.BasedOnPosition.Relocated (second.transform.position, firstPosition, firstRotation);
					vTracked = Utility.BasedOnPosition.Relocated (tracked.transform.position, firstPosition, firstRotation);
					
					isComplete = vTracked.z < vSecond.z && vTracked.z>=0;
					
					
				//}
				
				
				if (withDebugDraw) {
					
					Debug.DrawLine (transform.position, transform.position+vSecond, Color.cyan);
					Debug.DrawLine (transform.position, transform.position+vTracked, Color.green);
					Debug.DrawLine (transform.position, transform.position+vTrackedOnXY, Color.yellow);
				}
				
			}
			
		}
		return isComplete;
		
	}
	
	public override void DebugDraw ()
	{
		base.DebugDraw ();
	}
}
}
