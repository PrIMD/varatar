﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public class DistanceBetweenCondition	: BodyConditionWithLimit {
	
	public BodyPart bodyPartOne;
	public BodyPart bodyPartTwo;
	public BodyMajorBone measureComparator = BodyMajorBone.Arm;
	public CompareTo compareTo = CompareTo.Root;
	public bool ignoreX;
	public bool ignoreY;
	public bool ignoreZ;


	protected override float GetConditionValue ()
	{
	
				return linkedBody.GetDistanceRatioBetween(bodyPartOne,bodyPartTwo, measureComparator,compareTo,ignoreX,ignoreY,ignoreZ);


	}
}
}