﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public  class TriangulareCondition : BodyCondition {



	public float hipCenterDistance=2f;
	public float shoulderLeftDistance=1f;
	public float shoulderRightDistance=1f;
	public float approximation=0.3f;
	public BodyMajorBone  comparator = BodyMajorBone.Shoulders;
	public bool front=true;

	public BodyPart point= BodyPart.HandRight;



	//TODO improve the triangulation by using transform too. 
	//Exemple, to store a virual sword.
	//	public bool baseOnTransform;
	//	public Transform transfPoint;

	public override bool IsConditionComplete ()
	{
		bool isOk;

		//TODO, check that the point is in front of the player and add the methode in body.
		isOk = IsDistanceOk( point, BodyPart.CenterHips, hipCenterDistance) 
			&& IsDistanceOk( point, BodyPart.ShoulderLeft, shoulderLeftDistance) 
			&& IsDistanceOk( point, BodyPart.ShoulderRight, shoulderRightDistance) 
				;

		return isOk;

	}

	public bool IsDistanceOk(BodyPart point , BodyPart bp, float distance)
	{
		if (linkedBody == null || !linkedBody.IsInitialize())
						return false;
			float d = linkedBody.GetDistanceRatioBetween(point, bp, comparator);
			return distance>d-approximation && distance<d+approximation;

	}

}
}

