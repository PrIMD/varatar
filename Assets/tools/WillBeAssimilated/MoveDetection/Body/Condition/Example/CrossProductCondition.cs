﻿using UnityEngine;
using System.Collections;
using System;
namespace be.primd.toolkit.bodytracker.v1{

public class CrossProductCondition : BodyConditionWithLimit {
	
	public BodyPart left;
	public BodyPart right;
	public BodyPart other;


	public Vector3 directionAdjustement;



	//To delete
	public Vector3 directionWithAdj;
	public Vector3 directionActuelle;

	public bool inserseAxe;

	 protected override void Start(){
		base.Start ();

	}
	
	protected override float GetConditionValue ()
	{
		float angle=0f;
		LimitControl ();

		Vector3 dir = linkedBody.GetDirectionOf (left, right, other);
		directionActuelle = dir;

		directionWithAdj = Quaternion.Euler(directionAdjustement) * linkedBody.GetPlayerDirection();

		angle =Vector3.Angle(dir.normalized, directionWithAdj.normalized);
		return angle;

	}
	public void LimitControl(){

		float limit = 360f;
		if (directionAdjustement.x >= limit)
			directionAdjustement.x = limit;
		else if (directionAdjustement.x <= -limit)
			directionAdjustement.x = -limit;
		
		if (directionAdjustement.y >= limit)
			directionAdjustement.y = limit;
		else if (directionAdjustement.y <= -limit)
			directionAdjustement.y = -limit;
		
		if (directionAdjustement.z >= limit)
			directionAdjustement.z = limit;
		else if (directionAdjustement.z <= -limit)
			directionAdjustement.z = -limit;

	}
	public override void DebugDraw ()
	{
		base.DebugDraw ();
		Vector3 p1 = linkedBody.Get (left).transform.position;
		Vector3 p2 = linkedBody.Get (right).transform.position;
		Vector3 p3 = linkedBody.Get (other).transform.position; 
		Vector3 center = (p1 + p2 + 2f * p3) / 4f;
		Debug.DrawLine (center, center + directionActuelle*20f, Color.blue);
		Debug.DrawLine (center, center + directionWithAdj*20f, Color.cyan);
	}

}
}