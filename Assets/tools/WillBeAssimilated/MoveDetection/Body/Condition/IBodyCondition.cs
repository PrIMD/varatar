public interface IBodyCondition {
	
	bool IsConditionComplete ();
	string GetDescription();

     }
