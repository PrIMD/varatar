﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public abstract class BodyCondition : MonoBehaviour , IBodyCondition{

	public Body linkedBody;
	public string conditionDescription= "";
	public bool withDebugDraw;
	
	public bool checkInUpdate ;
	public bool lastValideCheck;
	protected void Update()
	{
		if (checkInUpdate) {
						lastValideCheck = IsConditionComplete ();
			if(withDebugDraw)
			{
				DebugDraw();
			}		
		}
	}

	protected virtual void Start(){CheckAndAutoCompleteValidity ();
	}
	protected virtual bool CheckAndAutoCompleteValidity()
	{
		if (linkedBody == null)
			linkedBody = Body.Instance;
		if (linkedBody == null)
						return false;
		return true;
	}
	public virtual bool IsConditionComplete (){
		return CheckAndAutoCompleteValidity ();
	}
	public string GetDescription ()
	{
		return conditionDescription;
	}

	public virtual void DebugDraw(){}
}
}