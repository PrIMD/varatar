﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;
[CustomEditor(typeof(LinkedMovement))]
public class LinkedMovementInspector : Editor {


	public bool plus=true;
	public bool historic=true;
		public override void OnInspectorGUI()
		{
		base.OnInspectorGUI (); EditorUtility.SetDirty( target );
		LinkedMovement lm = (LinkedMovement)target;
		plus = EditorGUILayout.Foldout(plus,"Plus");
		if (plus && Application.isPlaying) {
						EditorGUILayout.LabelField ("Observed positoin\t:" + lm.GetCurrentObserveredPosition ());
						EditorGUILayout.LabelField ("State position\t:" + lm.GetCurrentObserveredPosition ());
						EditorGUILayout.LabelField ("Activate:" + lm.IsActive ());
						EditorGUILayout.LabelField ("");
						EditorGUILayout.LabelField ("Start:" + lm.HasStart ());
						EditorGUILayout.LabelField ("In:" + lm.HasEnterPosition ());
						EditorGUILayout.LabelField ("Out:" + lm.HasExitPosition ());
						EditorGUILayout.LabelField ("End:" + lm.HasEnd ());
						EditorGUILayout.LabelField ("Cancel:" + lm.HasCancel ());


						historic = EditorGUILayout.Foldout (historic, "Historic");
			if(historic){
				EditorGUILayout.LabelField ("Time:" + lm.times.GetDuration()+" /" + lm.maxTime);
						float start = lm.times.GetWhenStarted();
						foreach (MovementHistoric.Step step in lm.times.GetSteps()) {

					EditorGUILayout.LabelField ("> " +(  step.when-start) + "\t:" + step.type + "\t:" + step.position);

				}
			}

		}

	}

}
