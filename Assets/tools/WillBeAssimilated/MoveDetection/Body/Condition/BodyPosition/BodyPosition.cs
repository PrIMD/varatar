﻿using UnityEngine;
using System.Collections.Generic;
namespace be.primd.toolkit.bodytracker.v1{

public class BodyPosition : MonoBehaviour {


	public static Dictionary <string, BodyPosition> positions = new Dictionary<string, BodyPosition> ();
	void AddAvailablePosition (string keyname)
	{
		if (keyname != null && keyname.Length > 0 ) {
			
			if(positions.ContainsKey (keyname))
			{
				positions.Remove(keyname);
			}
			
			positions.Add (keyname, this);
		}
	}

	public string keyName;
	public string positionDescription="";
	public bool autoCompleteIfNull = true;
	public GameObject [] gamObjConditions;
	public List<BodyCondition> conditions= new List<BodyCondition>();
	public float pourcentCorresponding ;

	public float refreshFrequence =0.3f;
	private float lastTimeRefresh;

	public void Start()
	{
		SetUpConditions (gamObjConditions);
		if (autoCompleteIfNull && gamObjConditions!=null && gamObjConditions.Length==0) {
			BodyCondition [] conditions = GetComponents<BodyCondition>() as BodyCondition [];
			SetUpConditions(conditions);
		}
		AddAvailablePosition (keyName);
	}


	public void Update(){
		float time = Time.timeSinceLevelLoad;
		if (time - lastTimeRefresh > refreshFrequence) {
			lastTimeRefresh=time;
			pourcentCorresponding= GetPourcentCorrespondingPosition();

		}
	}

	public float GetPourcentCorrespondingPosition(){
		int i = 0;
		foreach( BodyCondition cond in conditions)
		{
			if(cond.IsConditionComplete())i++;
		}
		return ((float) i)/((float) conditions.Count);
	}

	public bool IsActive() {
		return GetPourcentCorrespondingPosition () >= 1f;
	}
	public void SetUpConditions( GameObject [] sources)
	{
		foreach (GameObject  gamo in sources) {
			BodyCondition []  condi = gamo.GetComponents<BodyCondition>() as BodyCondition [];
			SetUpConditions(condi);

		}
	}
	public void SetUpConditions( BodyCondition [] cond)
	{
		foreach (BodyCondition c  in cond)
		{
			if(c.enabled)
				this.conditions.Add(c);
		}
	}

	public string GetMessage(){return positionDescription;}

	public static BodyPosition Get (string keyname)
	{
		BodyPosition pos=null;
		positions.TryGetValue (keyname, out pos);
		return pos;
	}

	public static string[] GetKeywordAvailable ()
	{
		string [] keys = new string[positions.Count]; 
		positions.Keys.CopyTo (keys, 0);
		return keys;
	}
}
}