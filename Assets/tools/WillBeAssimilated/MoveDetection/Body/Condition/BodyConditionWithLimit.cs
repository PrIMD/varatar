﻿using UnityEngine;
using System.Collections;
namespace be.primd.toolkit.bodytracker.v1{

public  abstract class BodyConditionWithLimit : BodyCondition {

	public ValideLimiteType limiteType ;
	public float minValue=0f;
	public float maxValue=90f;
	public float nearValue=5f;
	public float lastCalculatedValue;


	protected abstract float GetConditionValue ();

	public override bool IsConditionComplete ()
	{
		bool isOk =base.IsConditionComplete ();
		if (!isOk)return false;
		try{
			if (linkedBody != null && linkedBody.IsInitialize()) {

				float value = GetConditionValue();
				lastCalculatedValue= value;
				switch(limiteType)
				{
				case ValideLimiteType.In: 			return IsIn(value);
				case ValideLimiteType.Out: 			return IsOut(value);
				case ValideLimiteType.NearOfMax:	return IsNear(value);
				}
			}
				return false;
		}catch(BodyPartNotDefineException){return false;}

	}
	
	
	public bool IsIn(float value){
		return value > minValue && value < maxValue;
	}
	
	public bool IsOut(float value){
		return value < minValue || value > maxValue;
	}
	
	public bool IsNear(float value){
		return value > maxValue - nearValue && value < maxValue + nearValue;
	}
}

public enum ValideLimiteType {In, Out,NearOfMax }
}