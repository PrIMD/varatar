﻿using UnityEngine;
using System.Collections.Generic;
using System;
namespace be.primd.toolkit.bodytracker.v1{
public class MovementHistoric {
	
	public float startTime=-1f;
	public float endTime=-1f;
	public float cancelTime=-1f;
	public float lastTimeRecorded=-1;
	public readonly List<Step> steps = new List<Step>();

	public float GetWhenLastEnd ()
	{
		return endTime;
	}

	public float GetWhenLastStart ()
	{
		return startTime;
	}

	public float GetWhenLastCancel ()
	{
		return cancelTime;	}

	public void Add(BodyPosition position, float time, Step.Type type)
	{
		if (Step.Type.StartMovement.Equals (type))
			startTime = time;
		else if (Step.Type.EndMovement.Equals (type))
			endTime = time;
		else if (Step.Type.Cancel.Equals (type))
			cancelTime = time;

		steps.Add (new Step(position,time,type));
		lastTimeRecorded = time;
	} 

	public void Clear(){
		
		startTime=-1f;
		endTime=-1f;
		cancelTime=-1f;
		lastTimeRecorded = -1f;
		steps.Clear ();
	}

	
	public float GetLastTimeRecorded(){ return lastTimeRecorded;}
	public float GetDuration(){ 
		if (startTime >= 0 && endTime >= 0 && endTime>=startTime)
			return endTime - startTime;
		else if (startTime >= 0 && lastTimeRecorded >= 0 && lastTimeRecorded>= startTime)
			return lastTimeRecorded - startTime;

		return 0;
	}
	public float GetDuration(float time){
		if (startTime >= 0 && time >= 0 && time>=startTime)
			return time - startTime;
		return 0;
	}

	
	public float GetWhenStarted(){
		return startTime;
	}
	public float GetWhenEnded(){
		return endTime;
	}

	public List<Step> GetSteps(){
		//TODO should be a copie of step, but lazy to do it. 
		return steps;
	}


	public  class Step
	{
		public BodyPosition position;
		public float when;
		public enum Type {StartMovement, EnterPosition, ExitPosition, EndMovement, Cancel}
		public Type type;

		public Step(BodyPosition pos, float when, Type type)
		{
			this.position=pos;
			this.when =when;
			this.type =type;
		}
	}	
}
}