using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
namespace be.primd.toolkit.bodytracker.v1{

public abstract class Movement : MonoBehaviour {
	
	public event EventHandler <MouvementStartArgs> OnStartMoving;
	
	public event EventHandler <MouvementEnterPositionArgs> OnEnterPositionDetected;
	public event EventHandler <MouvementExitPositionArgs> OnExitPositionDetected;

	public event EventHandler <MouvementCancelArgs> OnMovementCancelled;

	public event EventHandler <MouvementEndArgs> OnEndMoving;



	public readonly MovementHistoric times = new MovementHistoric();
	
	public static Dictionary <string, Movement> movements = new Dictionary<string, Movement> ();
	public static Movement Get (string keyname)
	{
		Movement pos=null;
		movements.TryGetValue (keyname, out pos);
		return pos;
	}void AddAvailableMovement ()
	{
		if (keyName != null && keyName.Length > 0 ) {
			
			if(movements.ContainsKey (keyName))
			{
				movements.Remove(keyName);
			}
			
			movements.Add (keyName, this);
		}
	}

	public string keyName;
	public bool autoReset=true;
	public float autoResetTime =6f;
	public bool autoResetEnd;
	public bool autoResetCancel;	
	public bool endAtEnterLast=true;

	private float lastTimeStart;
	private bool hasBeenReset;
	
	protected bool isReset=true;


	protected virtual void Start(){
		AddAvailableMovement ();
	}


	protected virtual void Update(){

		bool askToStopUsing =  ! HasEventListener();

		// If there is not more listener, reset the move a zero and wait to be reuse
		if (askToStopUsing && ! hasBeenReset ) {

						Reset ();	
						hasBeenReset = true;
						//no need to warn someone, as nobody is listening
					//	Debug.Log ("The is no more listener");
				} else {

						if (autoReset && ! hasBeenReset) {
				
								float t = GetTime ();
								if (t - lastTimeStart > autoResetTime) {
										hasBeenReset = true;
										Cancel ();	
								}
						}
				}


	}


	protected void NotifyMovementStart()
	{
		lastTimeStart = GetTime ();
		hasBeenReset = false;
		times.Add (GetCurrentObserveredPosition (), GetTime (), MovementHistoric.Step.Type.StartMovement);
		if(OnStartMoving!=null)
		OnStartMoving (this, new MouvementStartArgs (Time.timeSinceLevelLoad, GetCurrentObserveredPosition() ));
	}
	
	protected void NotifyMovementEnd()
	{
		
		times.Add (GetCurrentObserveredPosition (), GetTime (), MovementHistoric.Step.Type.EndMovement);
		if(OnEndMoving!=null)
		OnEndMoving (this, new MouvementEndArgs  (Time.timeSinceLevelLoad, GetCurrentObserveredPosition() ));
		if (autoResetEnd)
						Reset ();
	}
	
	protected void NotifyBodyPositionEnterHasBeenDetected()
	{
		
		times.Add (GetCurrentObserveredPosition (), GetTime (), MovementHistoric.Step.Type.EnterPosition);
		if(OnEnterPositionDetected!=null)
		OnEnterPositionDetected (this, new MouvementEnterPositionArgs  (Time.timeSinceLevelLoad, GetCurrentObserveredPosition() ));
		if (endAtEnterLast  && IsEnd() ) {
			NotifyMovementEnd();
		}
	}

	protected void NotifyBodyPositionExitHasBeenDetected()
	{
		
		times.Add (GetCurrentObserveredPosition (), GetTime (), MovementHistoric.Step.Type.ExitPosition);
		if(OnExitPositionDetected!=null)
		OnExitPositionDetected (this, new MouvementExitPositionArgs  (Time.timeSinceLevelLoad, GetCurrentObserveredPosition() ));
	}

	protected void NotifyMovementCancel()
	{
		times.Add (GetCurrentObserveredPosition (), GetTime (), MovementHistoric.Step.Type.Cancel);
		if(OnMovementCancelled!=null)
		OnMovementCancelled (this, new MouvementCancelArgs  (Time.timeSinceLevelLoad, GetCurrentObserveredPosition() ));
		if (autoResetCancel)
			Reset ();
	}

	/**Is there something that are listening the event*/
	public virtual bool HasEventListener ()
	{	
		return OnStartMoving!=null ||
				OnEnterPositionDetected!=null ||
				OnExitPositionDetected!=null ||
				OnMovementCancelled!=null ||
				OnEndMoving!=null;

	}

	/**Return the main body position observed (cursor)*/
	public abstract BodyPosition GetCurrentObserveredPosition();

	public abstract void NextPositionDetection();

	/**Reset the members parms to be ready for the next move*/
	public abstract void Reset ();

	/**return true if the movement is considerer as end*/
	public abstract bool IsEnd();

	public abstract float GetTime (); 

	/** Does the movement always respect the condition of the move.
	 * Exemple if each move has a timing, do the time is respected.*/
	public virtual bool IsMovementAlwaysValide()
	{return true;}

	/**Stop immediately the move and reset it to listen the next*/
	public void Cancel(){
		NotifyMovementCancel ();
		Reset ();
	}

	public virtual void StartDetectingMove()
	{
		Reset ();

	}

	public static string[] GetKeywordAvailable ()
	{
		string [] keys = new string[movements.Count]; 
		movements.Keys.CopyTo (keys, 0);
		return keys;
	}
	
	public float WhenLastEndDetected(){return times==null?0f:times.GetWhenLastEnd();}
	public float WhenLastStartDetected(){return times==null?0f:times.GetWhenLastStart();}
	public float WhenLastCancelDetected(){return times==null?0f:times.GetWhenLastCancel();}
}


public class MouvementArgs : EventArgs
{

	public MouvementArgs(float when, BodyPosition bodyPosition)
	{
		this.when = when;
		this.bodyPosition = bodyPosition;

	}
	public readonly float when;
	public readonly BodyPosition bodyPosition;

}

public class MouvementStartArgs  : MouvementArgs
{	
	public MouvementStartArgs(float when, BodyPosition bodyPosition): base(when, bodyPosition){}
}
public class MouvementEndArgs  : MouvementArgs
{	public MouvementEndArgs(float when, BodyPosition bodyPosition): base(when, bodyPosition){}
}
public class MouvementCancelArgs : MouvementArgs
{	public MouvementCancelArgs(float when, BodyPosition bodyPosition): base(when, bodyPosition){}
}
public class MouvementEnterPositionArgs  : MouvementArgs
{	public MouvementEnterPositionArgs(float when, BodyPosition bodyPosition): base(when, bodyPosition){}
}
public class MouvementExitPositionArgs  : MouvementArgs
{	public MouvementExitPositionArgs(float when, BodyPosition bodyPosition): base(when, bodyPosition){}
}
}