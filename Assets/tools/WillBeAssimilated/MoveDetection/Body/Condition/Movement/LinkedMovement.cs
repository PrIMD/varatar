using UnityEngine;
using System.Collections.Generic;
namespace be.primd.toolkit.bodytracker.v1{
public  class LinkedMovement : Movement {
	

	public BodyPosition[] linkedBodyPositions ;
	public int cursor;

	public float refreshFrequence =0.1f;
	public float maxTime =5f; 

	private float lastTimeRefresh;



	//Current
	private int currentCursor;
	private BodyPosition currentPosition;
	private bool currentIsActive;
	private bool currentEnterNotifySend;
	private bool currentExitNotifySend;

	//Movement
	private bool StartNotifySend;
	private bool EndNotifySend;
	private bool CancelNotifySend;

	//to delete, just here to test
	public bool isThereListener;


	protected override void Update(){
		//If there are some mouvement
		base.Update ();
		bool askToStopUsing = ! HasEventListener();
		isThereListener =  ! askToStopUsing;
		if (askToStopUsing)
						return;

		if (linkedBodyPositions != null && linkedBodyPositions.Length > 1) {
						//refresh regulary
						float time = Time.timeSinceLevelLoad;
						if (time - lastTimeRefresh > refreshFrequence) {
								lastTimeRefresh = time;
								if (! CancelNotifySend) {

										CheckAndSend_MaxTime_CancelNotification();
										bool IsMoveTotalyFinished = EndNotifySend && currentExitNotifySend;	
										if (! IsMoveTotalyFinished) {

												bool isCurrentActive = IsActive ();
												bool startHasNotBeenSeendYet = ! StartNotifySend; 
												bool isNextDetected = false;


												//Detect the start of the movement
												if (isCurrentActive && startHasNotBeenSeendYet) {
														CheckAndSend_Start_Notification ();
				
												}

												//if the movement is in progress
												if (StartNotifySend) {
														if (CheckAndSend_In_Notification (BodyPositionObserved, isCurrentActive)) {
																currentIsActive = true;
															//Notify has been activated by by Movement; 
															if(IsEnd()) EndNotifySend=true;
													
														}
														CheckAndSend_End_Notification ();

														isNextDetected = CheckAndSend_Out_Notification (BodyPositionObserved, isCurrentActive) ;


												}

												//if I am at the end move and the notification of ending the move is send, the move is totaly finished;
												IsMoveTotalyFinished = EndNotifySend && currentExitNotifySend;	


												if (StartNotifySend && isNextDetected && ! IsMoveTotalyFinished) {

														Next (isCurrentActive);
												}
										}
								}
						}
				}
	}




	public BodyPosition BodyPositionObserved
	{
		get {
			BodyPosition bp = null;
			//if( cursor=>0 && cursor < linkedBodyPositions.Length )
				bp =  linkedBodyPositions [cursor];
			return bp;
		}
	}
	public BodyPosition GetNextObserved ()
	{
		
		int nextCursor = cursor + 1;
		if (nextCursor>=linkedBodyPositions.Length)
			return null;
		return linkedBodyPositions [nextCursor];
	}

	public bool IsActive ()
	{
		return BodyPositionObserved.IsActive ();
	}
	

	
	public BodyPosition GetPrevious ()
	{
		int previousCursor = currentCursor - 1;
		if (previousCursor<0)
		return null;
		return linkedBodyPositions [previousCursor];
	}
	
	public BodyPosition GetCurrent ()
	{
		
		return currentPosition;
	}

	
	public BodyPosition GetNext ()
	{
		
		int nextCursor = currentCursor + 1;
		if (nextCursor>=linkedBodyPositions.Length)
			return null;
		return linkedBodyPositions [nextCursor];
	}

	protected bool Next(bool isActif = true)
	{
		SetCurrent (cursor, BodyPositionObserved, isActif);
		
		currentEnterNotifySend = false;
		currentExitNotifySend = false;

		bool hasNext = cursor < linkedBodyPositions.Length - 1;

		if (hasNext) {
			cursor++;
		}
		return hasNext;
	}

	public override void Reset()
	{
		//Debug.Break ();
		cursor = 0;
		currentCursor 	= 0;
		currentPosition = null;
		currentIsActive = false;
		currentEnterNotifySend = false;
		currentExitNotifySend = false;
		StartNotifySend = false;
		EndNotifySend = false; 
		CancelNotifySend = false;
		times.Clear ();

	}

	public float SetCurrent(int newCursor, BodyPosition newBodyPosition, bool newIsActive){

		currentCursor 	= newCursor;
		currentPosition = newBodyPosition;
		currentIsActive = newIsActive;
		return GetTime ();
	}

	public bool CheckAndSend_MaxTime_CancelNotification(){

		if (StartNotifySend  && ! EndNotifySend  && times.GetDuration (GetTime()) > maxTime) {
			CancelNotifySend=true;
			//USELESSCODE: TodoWhenCancelDetected();
			base.NotifyMovementCancel();
			return true;

		}
		return false;
	}

	public bool CheckAndSend_Start_Notification(){
	
			if (!StartNotifySend && GetPrevious()==null)
			{
				StartNotifySend=true;
				//USELESSCODE: TodoWhenStartDetected();
				base.NotifyMovementStart();
				return true;
			}
		return false;
			
	}

	public bool CheckAndSend_End_Notification(){
		
		if (!EndNotifySend && ! HasEnterPosition() && GetNext()==null)
		{//Debug.Break();
			NotifyEnd();
			return true;
		}
		return false;
	}
	protected void NotifyEnd(){
		EndNotifySend=true;
		//USELESSCODE: TodoWhenEndDetected();
		base.NotifyMovementEnd();

	}

	public bool CheckAndSend_In_Notification(BodyPosition newBodyPosition, bool newIsActive){
		
		//If the new one is different
		if (newBodyPosition != currentPosition) {
			
			//And is going to be active
			if (newIsActive && currentIsActive == false) {
				//Signal that a new position is active
				if (currentEnterNotifySend == false) {

					currentEnterNotifySend = true;
					//USELESSCODE: TodoWhenEnterDetected();
					base.NotifyBodyPositionEnterHasBeenDetected ();
					
					return true;
				}
				
			}
		}
		
		return false;
	}

	public bool CheckAndSend_Out_Notification(BodyPosition newBodyPosition, bool newIsActive){
	  
		//If the new one is different
		//if (newBodyPosition != currentPosition) {
			// If it is out of the body position zone previousely actif
			if( currentIsActive && ! newIsActive  )
			{	
					//Signal that a new position is desactivated
					if(currentExitNotifySend==false){
						NotifyExit();
					return true;
					}
			}

		return false;
	}

	protected void NotifyExit(){
		currentExitNotifySend=true;
		//USELESSCODE: TodoWhenExitDetected();
		base.NotifyBodyPositionExitHasBeenDetected();

	}
	
	/*//USELESSCODE: 
	protected virtual void TodoWhenEnterDetected(){
		
	}
	protected virtual void TodoWhenExitDetected(){
		
	}
	
	protected virtual void TodoWhenStartDetected(){
		
	}

	
	protected virtual void TodoWhenEndDetected(){
	}

	protected virtual void TodoWhenCancelDetected(){
	}
	USELESS_CODE//*/
	public override float GetTime()
	{
		return Time.timeSinceLevelLoad;
	}


	public override BodyPosition GetCurrentObserveredPosition()
	{
		return BodyPositionObserved;
	}

	
	public override  void NextPositionDetection()
	{
		Next (false);
	}
	public bool IsTheLastObservedPosition(){
		return GetNextObserved () == null;
	}
	public override bool IsEnd (){return IsTheLastObservedPosition ();}

	public bool HasStart(){	return StartNotifySend;}
	public bool HasEnd(){	return EndNotifySend;}
	public bool HasCancel(){	return CancelNotifySend;}
	public bool HasEnterPosition(){return currentEnterNotifySend;}
	public bool HasExitPosition(){return currentExitNotifySend;}




}
}