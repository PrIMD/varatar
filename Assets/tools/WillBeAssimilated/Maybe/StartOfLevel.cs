﻿using UnityEngine;
using System.Collections;
using System;
public abstract class StartOfLevel : MonoBehaviour {


	public  EventHandler<HardWareReadyEventsArgs> OnHardwareReady;

	public bool hasHardWareReady;
	public bool hasStarted;

	public virtual void Update () {
	
		if (hasHardWareReady && hasStarted)
			Destroy (this);

		if (! hasHardWareReady && IsHardwareReadyConditionOk ()) {
			if(OnHardwareReady!=null){
			
				OnHardwareReady(null,new HardWareReadyEventsArgs(Time.timeSinceLevelLoad));
				hasHardWareReady=true;
			}

		}
		if (! hasStarted && IsStartConditionOk ()) {
							LevelManager.Start();
							hasStarted=true;
						
				}

	}
	
	public abstract bool IsStartConditionOk ();
	public abstract bool IsHardwareReadyConditionOk ();
}




public class HardWareReadyEventsArgs : EventArgs
{
	
	public HardWareReadyEventsArgs (float timeSinceLevelLoad)
	{
		when =timeSinceLevelLoad;
	}
	
	public float when;
	
}


