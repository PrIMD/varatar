﻿using UnityEngine;
using System.Collections;

public class Slider3D : MonoBehaviour {


	public float sliderValue;
	public TextMesh displayValue;
	
	public Transform slider;
	public Transform minPosition;
	public Transform maxPosition;
	public GameObject activateOnHighlight;

	
	public bool withUpdateCheck;
	public float checkValue;

	void Start(){
		if (minPosition == null || maxPosition == null) {
			Debug.Log("A slider is invalide, please set a min and max position",this.gameObject);
			Destroy(this);
			return;
		}
		
		if (slider== null) {
			Debug.Log("A slider is invalide, please set the slider Transform",this.gameObject);
			Destroy(this);
			return;
		}
		SetHighLight (false);
		SetValue (sliderValue);
	}

	public void Update(){
		if (withUpdateCheck) {
			if(checkValue!=sliderValue)
			{
				SetValue(sliderValue);
			}		
		}
	}
	public bool IsValide(){
		return minPosition != null && maxPosition != null && slider!=null;
	}

	void SetSliderAt (float value)
	{
		if (IsValide ()) {
						Vector3 positionFirstpoint = minPosition.position;
						Vector3 dirSecondPoint = maxPosition.position - minPosition.position;

						slider.position = positionFirstpoint + (dirSecondPoint * value);
			if(displayValue!=null) displayValue.text=""+(int)(value*100f);
		}
	
	}

	public void SetValue(float _value)
	{
		_value = Mathf.Clamp (_value,0f,1f);
		this.sliderValue = _value;
		checkValue = _value;
		SetSliderAt (_value);
	}
	public float GetValue()
	{
		return this.sliderValue;
	}

	public void SetHighLight (bool onOff)
	{
		if (activateOnHighlight != null) {
			activateOnHighlight.SetActive(onOff);		
		}
	}
}
