﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;
public class StartGameWhenCalibrationOk : MonoBehaviour {

	public BodyPosition position ;

	
	// Update is called once per frame
	void Update () {
	
		if (position!=null &&  position.IsActive ()) {
			LevelManager.NextLevel("Floor1");
		}
	}
}
