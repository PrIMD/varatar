﻿using UnityEngine;
using System.Collections;

public class KinectAutoLink : MonoBehaviour {


	// Use this for initialization
	void Start () {
		SkeletonWrapper sw = GameObject.FindObjectOfType<SkeletonWrapper> () as SkeletonWrapper;

		if (sw == null) {
			Destroy(this);	
			return ;
		}
		KinectModelControllerV2 [] kinectControllers = GameObject.FindObjectsOfType <KinectModelControllerV2>() as KinectModelControllerV2[];
		foreach (KinectModelControllerV2 rigSkelete in kinectControllers) {
			if(rigSkelete.sw==null) rigSkelete.sw=sw;		
		}
		
		KinectPointController [] kinectPointController = GameObject.FindObjectsOfType <KinectPointController>() as KinectPointController[];
		foreach (KinectPointController rigSkelete in kinectPointController) {
			if(rigSkelete.sw==null) rigSkelete.sw=sw;		
		}

	}
	

}
