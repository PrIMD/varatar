﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;

public class StartWhenBodyReady : StartOfLevel {

	public BodyPosition readyToPlay;
	public string readyToPlayKeyword="ReadyToPlay";



	// Update is called once per frame
	public override void Update () {
		base.Update ();

	}

	public override bool IsStartConditionOk ()
	{
		return IsHardwareReadyConditionOk ();
	}

	void CheckValidity ()
	{
		if (readyToPlay == null)
			readyToPlay = BodyPosition.Get (readyToPlayKeyword);
	}

	public override bool IsHardwareReadyConditionOk ()
	{
		Body body = Body.Instance;
		if (body != null && body.IsInitialize ()) {
			CheckValidity();
			if(readyToPlay==null) return true;
			else
			{
				if(readyToPlay.IsActive())
				{
					Destroy(this);
					return true;
				}
			}
		}
		return false;
	}
}
