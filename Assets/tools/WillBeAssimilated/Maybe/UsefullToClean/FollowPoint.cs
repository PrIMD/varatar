﻿using UnityEngine;
using System.Collections;

public class FollowPoint : MonoBehaviour {

	public Transform point;
	public float speed=0.5f;

	void Update () {
		if (point != null) {
			if(speed<=0f)
			this.transform.position= point.transform.position;
			else transform.position = Vector3.Lerp(transform.position, point.position,Time.deltaTime*speed);
		}
	}

	public void SetPoint (Transform point)
	{
		this.point = point;
	}

	public void SetSpeed (float speed)
	{
		this.speed = speed;
	}
}
