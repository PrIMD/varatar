﻿using UnityEngine;
using System.Collections;

public class GrowWithTime : MonoBehaviour {
	
	private float startTime;
	public float growTime=2f;
	public float pctPerSecond=0.5f;
	void Start () {
		startTime = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad-startTime>growTime)
		{Destroy(this);}
	
		transform.localScale +=transform.localScale*pctPerSecond*Time.deltaTime;
	}
}
