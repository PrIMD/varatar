﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {
	
	public Vector3 initialTarget =Vector3.zero;
	public Vector3 initialDirection =Vector3.zero;
	public float speed =60f;

	void Update () {
		if(initialDirection!=Vector3.zero)
			this.transform.Translate (Vector3.forward *( Time.deltaTime*speed));
	}

	public void SetDirectionAt(Vector3 targetlocalisation ){ 
		this.initialTarget = targetlocalisation;
		this.transform.LookAt(this.initialTarget);	
		this.initialDirection= transform.eulerAngles;

	}
}
