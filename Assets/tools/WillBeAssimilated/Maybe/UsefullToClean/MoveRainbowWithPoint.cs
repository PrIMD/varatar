﻿using UnityEngine;
using System.Collections;

public class MoveRainbowWithPoint : MonoBehaviour {

	public GameObject pointManRoot;
	public GameObject pointManSpine;
	public GameObject rainbowManSpineRoot;
	public Vector3 adjustment;

	void Start () {
	}

	void Update () {
		if (rainbowManSpineRoot != null && pointManSpine != null && pointManRoot !=null) {
			Vector3 newPos =rainbowManSpineRoot.transform.position;
 			newPos = pointManRoot.transform.position + (pointManSpine.transform.position-pointManRoot.transform.position);
			newPos += adjustment;		
			rainbowManSpineRoot.transform.position = newPos;

		}
	}
}
