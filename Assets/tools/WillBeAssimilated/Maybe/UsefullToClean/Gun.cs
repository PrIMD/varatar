﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {
	/*
	
	public float lastShoot;
	public float timeBetweenShot=0.5f;

	public float dommage =10f;
	public float range = 200f;

	public int radiusOfShoot=200;
	public LayerMask shootAtLayout;
	public LayerMask wallAndStuff;
	public Transform origine;
	public Transform direction;


	public Transform gunExplosion;
	public GameObject gunExploPrefab;
	public GameObject gunSound;

	public bool canFireWithBody=true;
	public BodyPosition fireWithBody;



	public void Update(){
		float time = Time.timeSinceLevelLoad;
		if (time - lastShoot > timeBetweenShot) {

						if (X360.IsLb () ||  Input.GetMouseButton(0) || (canFireWithBody && fireWithBody!=null && fireWithBody.IsActive() ) ) {
								lastShoot=time;
								bool hasAmmo = WolfPlayer.HasAmmo();
								if(hasAmmo){
					
									Sound.Play("GunFire");
									Shoot (origine.position, direction.position - origine.position, dommage);
									if (gunExplosion != null && gunExploPrefab != null) {
											GameObject pref = GameObject.Instantiate (gunExploPrefab, gunExplosion.position, gunExplosion.rotation)as GameObject;
											pref.transform.parent = gunExplosion.transform;
											Destroy (pref, 5f);
									}
									if (gunSound != null) {
											GameObject sound = GameObject.Instantiate (gunSound) as GameObject;
											Destroy (sound, 3f);
									}
									WolfPlayer.UseAmmo(1);
								}
							else{
								Sound.Play("NoAmmo");
							}
						}
				}
	}
	public void Shoot(Vector3 position, Vector3 direction, float dommage)
	{
		position -= direction.normalized * radiusOfShoot;
		RaycastHit [] hits=null;
		hits = Physics.SphereCastAll (position, radiusOfShoot, direction.normalized, 1000f, shootAtLayout);	
		
		foreach (RaycastHit h in hits) {
//			bool isEnemy=false;
			WolfSoldier enemy = h.collider.gameObject.GetComponent(typeof(WolfSoldier)) as WolfSoldier;
			if(enemy!=null)
			{
				//Debug.DrawLine (position, h.point,Color.yellow, 15f);
				//isEnemy=true;
				if(! enemy.isDeath && !IsThereObjectBetween(position, h.collider.gameObject.transform.position)){

					float dist = Vector3.Distance(position,h.collider.gameObject.transform.position);
					float dmg=dommage;
					if(range!=0)
					{
						dmg = dommage + (dommage*(1-dist/range));
						
					}


					enemy.TakeShoot((int)dmg);

					Debug.DrawLine(position, h.point, Color.red,10f);
					return ;
				}
			}

		}
	}
	protected bool IsThereObjectBetween (Vector3 position, Vector3 target)
	{
	
		RaycastHit hit ;
		bool wallBetween =  Physics.Raycast (position, target-position, out hit, Vector3.Distance(position,target)*1.1f, wallAndStuff);

//		Debug.DrawLine(position, hit.point, Color.cyan, 10f);
		return wallBetween;

	}*/	
}
