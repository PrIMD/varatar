﻿using UnityEngine;
using System.Collections;

public class BetweenPoint : MonoBehaviour {
    public Transform    firstPoint;
    public Transform    secondPoint;

    public float smoothVelocity=7f;

    public Vector3 actualPosition = new Vector3();
    public Vector3 destinationPosition = new Vector3();

    public bool allowRotationOnX = true;
    public bool allowRotationOnY = true;
    public bool allowRotationOnZ = false;

    void Update() {
        if (DoPointsAreInitialisated())
        {
            bool withSmooth = IsWithSmooth();

            float newX = MiddleOfTheNumbers(firstPoint.transform.position.x , secondPoint.transform.position.x)  ;
            float newY = MiddleOfTheNumbers(firstPoint.transform.position.y , secondPoint.transform.position.y)  ;
            float newZ = MiddleOfTheNumbers(firstPoint.transform.position.z , secondPoint.transform.position.z)  ;

            destinationPosition.x = newX;
            destinationPosition.y = newY;
            destinationPosition.z = newZ;

            if (withSmooth)
            {
                SetActualPositionWithSmooth( ref actualPosition, destinationPosition, smoothVelocity);
            }
            else actualPosition = destinationPosition;


            Quaternion newRotation = GetAdequateRotation(firstPoint.transform.position, actualPosition );

            ApplyNewPositionAndRotation( actualPosition,  newRotation);

        }
           
    }

    private void SetActualPositionWithSmooth(ref Vector3 actualPosition, Vector3 destinationPosition, float smoothVelocity)
    {

        actualPosition += (destinationPosition- actualPosition) * (smoothVelocity * Time.deltaTime);
    }

 
    public bool IsWithSmooth() { return smoothVelocity != 0f; }
    public float MiddleOfTheNumbers(float first, float second)
    {       return (first + second) / 2f;    }

    private void ApplyNewPositionAndRotation( Vector3 position , Quaternion  rotation)
    {
        this.transform.position = position;
        this.transform.rotation = rotation;
    }
    public Quaternion GetAdequateRotation(Vector3 first, Vector3 second) 
    {
        Vector3 lookToward = second - first;
        if (!allowRotationOnZ) { lookToward.z = 0f; }
        if (!allowRotationOnY) { lookToward.y = 0f; }
        if (!allowRotationOnX) { lookToward.x = 0f; }
        Quaternion toRotateOf = Quaternion.LookRotation(lookToward);
        return toRotateOf;
    }

    public bool DoPointsAreInitialisated() { return firstPoint != null && secondPoint != null; }

}
