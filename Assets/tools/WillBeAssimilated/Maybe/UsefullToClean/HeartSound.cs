﻿using UnityEngine;
using System.Collections;

public class HeartSound : MonoBehaviour {

	/*
	public float hearthSoundDuration = 2f;
	public AudioSource hearthSound;
	public WolfPlayer player ;

	public float startBeatLife=50;
	public float minBeatDelay=0.25f;
	public float nextBeat;

	public float stress=0f;

	public bool dying = false;
	public float pulsarDecreasePerBeat = 0.3f;
	public int decreaseCount;
	public float frequenceDyingBeat =0.30f;
	public int dyingQuickBeatCount= 9;
	public float multicatorQuickBeat=0.7f;
	public float multicatorSlowBeat=2.3f;
	public void Start(){

		if (hearthSound == null) {
						Destroy (this);
			return;}
			LevelManager.OnGameOver += GameOver;
		}

		public void GameOver(object obj, GameOverEventArgs args){
			dying = true;
			stress=1f;
		}

	public bool IsValide(){
	
		if (player == null)
			player = WolfPlayer.instance;

		if(player==null) return false;
		 return true;
	}

	public float lastUpdate ;
	public void Update()
	{

		float deltaTime = 0;
		if (dying) {
			if(lastUpdate==0){ lastUpdate= Time.realtimeSinceStartup;
				return;}
			float time = Time.realtimeSinceStartup;
			deltaTime=time-lastUpdate;
			lastUpdate=time;

		} else {
			deltaTime = Time.deltaTime;
				}
		if (IsValide ()) {


			if(! dying){
				if (nextBeat <= 0 && player != null && player.Health < startBeatLife) {
					float pct = 1f;
					pct = ((float)player.Health) / startBeatLife;
					nextBeat = hearthSoundDuration * (minBeatDelay + pct / 2f);
					hearthSound.volume = (1f - pct) * 2f;
				}
			}
			else
			{
				if (nextBeat <= 0)
				{
					float soundBeat =frequenceDyingBeat;
					float decreaseRatio =decreaseCount*pulsarDecreasePerBeat;
					float stressMalus = 1f- Mathf.Clamp(stress,0f,1f)/2f;
					nextBeat = soundBeat + (soundBeat *decreaseRatio *stressMalus);

					hearthSound.volume=100;
					decreaseCount++;
				}

			}	


				if (nextBeat > 0) {
					nextBeat -= deltaTime;
					if (nextBeat <= 0) {
						nextBeat = 0;
						hearthSound.Play ();
					}
				}

		}
	}*/
}
