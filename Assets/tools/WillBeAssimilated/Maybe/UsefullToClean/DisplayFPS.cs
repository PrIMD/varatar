using UnityEngine;
using System.Collections;

public class DisplayFPS : MonoBehaviour {

	float frameCount = 0;
	float deltaTime = 0.0f;
	float fps = 0.0f;
	float updateRate = 4.0f; // 4 updates per sec.
	public bool displayOnOff;
	public TextMesh fpsTextDisplay; 


	public void Start(){
		if (fpsTextDisplay == null)
						Destroy (this);
	}
	private void Update()
	{

		if (Input.GetKeyDown (KeyCode.F))
			displayOnOff=!displayOnOff;

		if (fpsTextDisplay != null){
				frameCount++;
				deltaTime += Time.deltaTime;
				if (deltaTime > 1.0f / updateRate) {
						fps = frameCount / deltaTime;
						frameCount = 0f;
						deltaTime -= 1.0f / updateRate;
					fpsTextDisplay.renderer.enabled = displayOnOff;
					if (displayOnOff) {
						
						fpsTextDisplay.text = "" + fps;
					}
				}

				
		}
	}
}