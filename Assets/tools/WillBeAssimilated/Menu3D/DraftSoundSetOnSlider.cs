﻿using UnityEngine;
using System.Collections;

public class DraftSoundSetOnSlider : MonoBehaviour {

	public Slider3D sliderSound ;
	public float lastValue;

	public void Start()
	{
		if (sliderSound == null) {
						Destroy (this);
						return;
				}
	}

	public void Update()
	{
		float newValue = sliderSound.GetValue ();
		if (sliderSound != null && newValue  != lastValue) {
			lastValue=newValue;
			AudioListener.volume=newValue;
		}
	}
}
