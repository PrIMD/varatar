﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;
public class MenuSoundInput : MonoBehaviour {

	public int cursor;
	public Slider3D [ ] slider;
	
	public BodyPosition next;
	public BodyPosition previous;
	public BodyPosition more;
	public BodyPosition less;

	public float pourcentPerIncrement=0.01f;
	public float selectRefresh;
	public float whenSelectChanged;

	public float valueRefresh;
	public float whenValueChanged;


	public void Start(){
		SliderChange (0);
	}

	public void Update()
	{
		if (slider != null && slider.Length > 0) {

			CheckForSelectionRequest();
			CheckForChangeRequest();
		
		}



	}

	void SliderChange (int incrementValue=1)
	{
		
		if (slider.Length > 0) {

			slider [cursor].SetHighLight (false);


				if (cursor + incrementValue < 0) 
					//cursor = slider.Length-1;
					cursor = 0;
				else if(cursor+incrementValue>=slider.Length) cursor=slider.Length-1;
				else cursor+=incrementValue;

			//cursor= cursor% slider.Length;
			slider [cursor].SetHighLight (true);
		}
	}

	void SlideSelectedValueChange (float pctIncr)
	{
		float pctValue = slider [cursor].GetValue () + pctIncr;
		slider [cursor].SetValue (pctValue);
	}

	void CheckForSelectionRequest ()
	{
		float time = Time.realtimeSinceStartup;
		if (time - whenSelectChanged > selectRefresh) {
			
			if(next !=null && next.IsActive())
			{
				SliderChange(1);
				whenSelectChanged=time;
			}else
			if(previous !=null && previous.IsActive())
			{
				SliderChange(-1);
				whenSelectChanged=time;
			}
		}
	}

	void CheckForChangeRequest ()
	{
		float time = Time.realtimeSinceStartup;
		if (time - whenValueChanged > valueRefresh) {
			
			if(more !=null && more.IsActive())
			{
				SlideSelectedValueChange(pourcentPerIncrement);
				whenValueChanged=time;
			}else
				if(less !=null && less.IsActive())
			{
				SlideSelectedValueChange(-pourcentPerIncrement);
				whenValueChanged=time;
			}
		}
	}
}
