﻿using UnityEngine;
using System.Collections;

public class MainMenuFirstTime : MonoBehaviour {

	public Menu3D menu;
	public void Start () {

		menu.SetOn (true);
		menu.TryToActivate("Help");
	}

}
