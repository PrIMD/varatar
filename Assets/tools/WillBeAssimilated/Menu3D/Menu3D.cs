using UnityEngine;
using System.Collections;
using System;
using be.primd.toolkit.bodytracker.v1;
public class Menu3D: MonoBehaviour
{
	public bool onOff;

	public BodyPosition open;
	public BodyPosition close;

	public GameObject menuGameObject;
	public Button3D [] buttons;
	
	public bool freezeMoveWhenOpen;
	public bool autoReadjustationWhenOpen;
	public MoveZoneActivator [] movers;
	
	public bool freezeRotationWhenOpen;
	public RotationZoneActivator [] rotators;




	public void Start(){
		if (menuGameObject == null || menuGameObject == this.gameObject) {
						Debug.Log ("The menu must have a link to an other game object", this.gameObject);
						return;
				}
		SetOn (false);

	}
	public void Update()
	{

		if (onOff) {
			if (close.IsActive ()) {
				DeactivateAllButton();
				SetOn (false);

			}
		} else {
			if(open.IsActive())
			{
				SetOn(true);
			}	
		}

		if (onOff) {
			foreach (Button3D button in buttons) {
				if(button !=null && button.positionToActivate!=null && button.objectToActivate!=null)
				{
					if(button.positionToActivate.IsActive())
					{
						DeactivateAllButton();
						button.objectToActivate.SetActive(true);
					}
				}		
			}

		}

	}
	
	public void DeactivateAllButton()
	{
		foreach (Button3D button in buttons) {
			if(button !=null &&  button.objectToActivate!=null)
			{
					button.objectToActivate.SetActive(false);
			}		
		}
	}

	public void SetOn (bool onOff)
	{
		if (! this.onOff  && onOff) {
						ActivationOfMenu ();
				} else
						DeactivationOfMenu ();

		this.onOff = onOff;
		if (menuGameObject != null)
			menuGameObject.SetActive(onOff);
	}

	public void FreezeMovement(bool onOff = true)
	{

			foreach(MoveZoneActivator move  in movers)
			{
				move.SetActive(!onOff);
			}		

	}
	public void FreezeRotation(bool onOff = true)
	{
		
		foreach(RotationZoneActivator rotate in rotators)
		{
			rotate.SetActive(!onOff);
		}		
		
	}
	public void AutoCalibrateMovement(){
		
		foreach(MoveZoneActivator move  in movers)
		{
			move.RootAutoAdjustement();
		}		
	}

	public void ActivationOfMenu(){
		if(freezeMoveWhenOpen)
			FreezeMovement (true);
		if (freezeRotationWhenOpen) 
			FreezeRotation (true);

	}
	public void DeactivationOfMenu(){

		if (autoReadjustationWhenOpen) {
			AutoCalibrateMovement();
		}
		if(freezeMoveWhenOpen)
			FreezeMovement (false);
		if (freezeRotationWhenOpen) 
			FreezeRotation (false);

	}

	public void TryToActivate (string elementName)
	{
		if(elementName!=null && elementName.Length>0)
		foreach (Button3D b in buttons) {
			if(b !=null && elementName.Equals(b.name))
			{
				DeactivateAllButton();
			   b.Activate();
			}

		}
		//DoWeHaveTheElement
		//If yes SetOn and activate the element to be seen
	}
}

[Serializable]
public class Button3D 	
{
	public string name;
	public BodyPosition positionToActivate;
	public GameObject objectToActivate;

	public void Activate ()
	{
		if (objectToActivate != null)
						objectToActivate.SetActive (true);
		OnActivate ();
	}
	protected virtual void OnActivate()
	{

	}
}

