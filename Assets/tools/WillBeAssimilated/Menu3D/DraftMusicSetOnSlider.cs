﻿using UnityEngine;
using System.Collections;

public class DraftMusicSetOnSlider : MonoBehaviour {
	
	public Slider3D sliderSound ;
	public float lastValue;
	public string musicTagName = "Music";
	public AudioSource musicSource;

	public string prefKeyToSave="MusicPreference";
	public void Start()
	{
		if (sliderSound == null) {
			Destroy (this);
			return;
		}
		else{
			sliderSound.SetValue( PlayerPrefs.GetFloat(prefKeyToSave));
		}
	}
	
	public void Update()
	{
		float newValue = sliderSound.GetValue ();
		if (sliderSound != null && newValue  != lastValue) {
			if(musicSource==null)
			{
				GameObject gamo = GameObject.FindWithTag(musicTagName) as GameObject;
				if(gamo!=null)
				{
					musicSource= gamo.GetComponent<AudioSource>() as AudioSource;
					//if(musicSource!=null)
						//sliderSound.SetValue(musicSource.volume);
				}
			}
			lastValue=newValue;
			PlayerPrefs.SetFloat(prefKeyToSave, newValue);
			if(musicSource!=null)
			musicSource.volume=newValue;
		}
	}
}
