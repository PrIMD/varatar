﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v1;

public class DisplayMenu : MonoBehaviour {

	public BodyPosition openMenu;
	public GameObject menuObject;

	public bool menuIsOpen;

	public float checkDelay = 1f;
	private float lastTimeCheck;

	// Use this for initialization
	void Start () {
	
		if (menuObject == null || openMenu == null)
						Destroy (this);

	}
	
	// Update is called once per frame
	void Update () {
		float time = Time.timeSinceLevelLoad;
		if (time - lastTimeCheck > checkDelay) {

			MenuActifConfiguration ();

			if(menuIsOpen)
			{

			}




			lastTimeCheck=time;	
		}
	
	}

	void MenuActifConfiguration ()
	{
		bool actif = openMenu.IsActive ();
		if (menuIsOpen == false && actif && menuObject != null) {
			menuIsOpen = true;
			menuObject.SetActive (true);
		}
		else
			if (menuIsOpen && !actif && menuObject != null) {
				menuIsOpen = false;
				menuObject.SetActive (false);
			}
	}
}
