﻿using UnityEngine;
using System.Collections;

public class YesNoQuickView : MonoBehaviour {


	public YesNoDetector yesNo;
	public GameObject targetToColor;
	// Use this for initialization
	void Start () {
		if (yesNo != null) {
						yesNo.OnYes = DoOnYes;
						yesNo.OnNo = DoOnNo;
				}
	}
	
	public void DoOnYes(object obj, YesDetectedEventArgs yes)
	{
		if (targetToColor != null && targetToColor.renderer != null && targetToColor.renderer.material != null) {
			targetToColor.renderer.material.color = Color.green;
		}
		
	}
	
	public void DoOnNo(object obj, NoDetectedEventArgs no)
	{
		if (targetToColor != null && targetToColor.renderer != null && targetToColor.renderer.material != null) {
			targetToColor.renderer.material.color = Color.red;
		}

	}
}
