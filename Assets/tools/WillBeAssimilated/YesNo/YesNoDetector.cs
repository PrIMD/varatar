﻿/** Code by Stree Eloi the 07 may 2014. */

using UnityEngine;
using System.Collections;
using System;
public class YesNoDetector : MonoBehaviour {

	//NEED RESOURCE
	public Transform observeRotatorPoint;

	//LISTENER
	public  EventHandler<YesDetectedEventArgs> OnYes;
	public  EventHandler<NoDetectedEventArgs> OnNo;
	//CONFIG
	public bool inverse;
	public int minNoChangeCount=4;
	public int minYesChangeCount=4;

	public float sensibility=0.03f;
	public float maxTimeBetweenChange =0.5f;

	public float refreshTime=0.1f;


	//TMP
	private float horizontalChangeDirCount=0f;
	private float verticalChangeDirCount=0f;


	private float lastRefresh;
	
	private Quaternion lastRotationRecorded;

	private float whenLastHorizontalChange;
	public LateralDirection lastHorizontalDirection;
	private LateralDirection lastSignificatifHorizontalDirection;
	
	private float whenLastVerticalChange;
	public VerticalDirection lastVerticalDirection;
	private VerticalDirection lastSignificatifVerticalDirection;
	private bool autoCompleteIfObservedNull=true;

	public void Awake(){
		if (observeRotatorPoint == null && autoCompleteIfObservedNull) {
			observeRotatorPoint=this.transform;
		}
	}

	void Update () {

		bool isListening = OnYes != null || OnNo != null; 
		if (isListening) {
						float time = Time.realtimeSinceStartup;
						if (time - lastRefresh > refreshTime) {
								lastRefresh = time;
								Quaternion rotation = observeRotatorPoint.transform.rotation;
								Vector3 origine = observeRotatorPoint.transform.position;
								Vector3 forward = origine + rotation * Vector3.forward * 1f;
								Vector3 direction = forward - origine;
								Vector3 directionRelocated = Relocated (direction, Vector3.zero, lastRotationRecorded);
			
								LateralDirection horizontalDirection = LateralDirection.NotMoving;
								VerticalDirection verticalDirection = VerticalDirection.NotMoving;

								SetDirection (directionRelocated, ref horizontalDirection, ref verticalDirection);
	
								LateralDirection horizontalSignificatifDirection = LateralDirection.NotDefine;
								VerticalDirection verticalSignificatifDirection = VerticalDirection.NotDefine;
								SetSignificatifDirection (horizontalDirection, ref horizontalSignificatifDirection);
								SetSignificatifDirection (verticalDirection, ref verticalSignificatifDirection);

								if (HasChangeDirection (horizontalSignificatifDirection, lastSignificatifHorizontalDirection)) {
										whenLastHorizontalChange = time;
										horizontalChangeDirCount++;
								}
								if (HasChangeDirection (verticalSignificatifDirection, lastSignificatifVerticalDirection)) {
										whenLastVerticalChange = time;
										verticalChangeDirCount++;
				
								}
								if (time - whenLastHorizontalChange > maxTimeBetweenChange) {
										horizontalChangeDirCount = 0;
								}
								if (time - whenLastVerticalChange > maxTimeBetweenChange) {
										verticalChangeDirCount = 0;
								}
			
								if (horizontalChangeDirCount >= (inverse ? minYesChangeCount : minNoChangeCount)) {
										if (inverse)
												NotifyYesDetected ();
										else
												NotifyNoDetected ();
								}
			
								if (verticalChangeDirCount >= (inverse ? minNoChangeCount : minYesChangeCount)) {
										if (inverse)
												NotifyNoDetected ();
										else
												NotifyYesDetected ();
								}


								lastHorizontalDirection = horizontalDirection;
								lastVerticalDirection = verticalDirection;

								lastSignificatifHorizontalDirection = horizontalSignificatifDirection;
								lastSignificatifVerticalDirection = verticalSignificatifDirection;

								lastRotationRecorded = rotation;
						}

				}
	}
	
	public void NotifyNoDetected(){
		if(OnNo!=null)
		OnNo (this.gameObject, new NoDetectedEventArgs ());
	}
	
	public void NotifyYesDetected(){
		if(OnYes!=null)
		OnYes (this.gameObject, new YesDetectedEventArgs ());

	}
	void SetDirection (Vector3 directionRelocated, ref LateralDirection latHorizDir, ref VerticalDirection latVertDir)
	{
		if (directionRelocated.x > sensibility) {
			latHorizDir = LateralDirection.Right;
		}
		else
			if (directionRelocated.x < -sensibility) {
				latHorizDir = LateralDirection.Left;
			}
		if (directionRelocated.y > sensibility) {
			latVertDir = VerticalDirection.Up;
		}
		else
			if (directionRelocated.y < -sensibility) {
				latVertDir = VerticalDirection.Down;
			}
	}
	
	void SetSignificatifDirection(LateralDirection from, ref LateralDirection to)
	{
		if(from == LateralDirection.NotMoving ||  from==LateralDirection.NotDefine)
			to =LateralDirection.Left;
		else to=from;
		
	}
	void SetSignificatifDirection(VerticalDirection from, ref VerticalDirection to)
	{
		if(from == VerticalDirection.NotMoving ||  from==VerticalDirection.NotDefine)
			to =VerticalDirection.Down;
		else to=from;
		
	}


	bool HasChangeDirection (LateralDirection first, LateralDirection second)
	{
		return (first == LateralDirection.Left && second == LateralDirection.Right) 
			|| (first == LateralDirection.Right && second == LateralDirection.Left);
	}
	
	bool HasChangeDirection (VerticalDirection first, VerticalDirection second)
	{
		return (first == VerticalDirection.Up && second == VerticalDirection.Down) 
			|| (first == VerticalDirection.Down && second == VerticalDirection.Up);
	}
	
	public enum LateralDirection{Left, Right,NotMoving,NotDefine}
	public enum VerticalDirection{Up, Down,NotMoving,NotDefine}

	private static Vector3 Relocated(Vector3 point,Vector3 rootPosition ,Quaternion rootRotation, bool withIgnoreX=false, bool withIgnoreY=false, bool withIgnoreZ=false){
		Vector3 p = point- rootPosition;	
		if (rootRotation.eulerAngles != Vector3.zero) {
			Quaternion rootRotationInverse = Quaternion.Inverse (rootRotation);
			p = RotateAroundPoint (p, Vector3.zero, rootRotationInverse);
		}
		if (withIgnoreX)
			p.x = 0;
		if (withIgnoreY)
			p.y = 0;
		if (withIgnoreZ)
			p.z = 0;
		
		return p;
	}
	
	private static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}
}
public class YesDetectedEventArgs : EventArgs
{//public ValidationSpeedType speed = ValidationSpeedType.Quick //[Normal,Slow] 
}
public class NoDetectedEventArgs : EventArgs
{
}
