﻿/*
 * --------------------------BEER-WARE LICENSE--------------------------------
 * PrIMD42@gmail.com wrote this file. As long as you retain this notice you
 * can do whatever you want with this code. If you think
 * this stuff is worth it, you can buy me a beer in return, 
 *  S. E.
 * Donate a beer: http://www.primd.be/donate/ 
 * Contact: http://www.primd.be/
 * ----------------------------------------------------------------------------
 */

#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

public class AddMeasure : EditorWindow {

	public int  i =0;
/*	
	[MenuItem("Tools/Measure/2D Zone %#z")]
	private static void AddMeasureZoneInCameraFront()
	{
		GameObject Instance = Instantiate(Resources.Load("Zone2D", typeof(GameObject))) as GameObject;
		Instance.name="Zone2D";
		if (Camera.current != null) {
						Camera cam = Camera.current.camera;
						if (cam != null) {
			
								Instance.transform.position = new Vector3 (cam.transform.position.x, cam.transform.position.y, 0f);
			
						}
				}
	}
*/
	[MenuItem("Tools/Measure/Point To Point %#o")]
	private static void AddMeasurePointInCameraFront()
	{
		GameObject Instance = Instantiate(Resources.Load("PointToPoint", typeof(GameObject))) as GameObject;
		Instance.name="PointToPoint";
		if (Camera.current != null) {
						Camera cam = Camera.current.camera;
						if (cam != null) {
			
								Instance.transform.position = new Vector3 (cam.transform.position.x, cam.transform.position.y, 0f);
			
						}
				}
	}

	[MenuItem("Tools/PrIMD/1. Documentation")]
	private static void OpenWiki ()
	{
		System.Diagnostics.Process.Start ("https://bitbucket.org/PrIMD/toolbox/wiki");
	}
	[MenuItem("Tools/PrIMD/2. Source")]
	private static void Source ()
	{
		System.Diagnostics.Process.Start ("https://bitbucket.org/PrIMD/toolbox/src");
	}


	[MenuItem("Tools/PrIMD/3. Donation")]
	private static void Donation ()
	{
		System.Diagnostics.Process.Start ("http://www.primd.be/donate");
	}


	
	[MenuItem("Tools/PrIMD/4. About")]
	private static void OpenWebSite ()
	{
		System.Diagnostics.Process.Start ("http://www.primd.be/");
	}

}
#endif