﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(CharacterController))]
public class DEL_CharactereControlerInput : MonoBehaviour {

	public CharacterController controler;
	public MultiInput left;
	public MultiInput right;
	public MultiInput front;
	public MultiInput back;
	public float speed =1f;

	public Vector3 direction;
	public float refresh=0.1f;
	public float lastRefresh;

	void Start () {
		if(controler==null)
		controler = GetComponent<CharacterController> () as CharacterController;
	}
	
	void Update () {

		if (controler == null)
						return;

		float time = Time.timeSinceLevelLoad;
		if(time-lastRefresh>refresh){

			float moveLeft = left!=null? left.IsActiveInPourcent():0f;
			float moveRight = right!=null ? right.IsActiveInPourcent ():0f;
			float moveFront = front!=null ? front.IsActiveInPourcent ():0f;
			float moveBack = back!=null ?  back.IsActiveInPourcent ():0f;

			if(moveLeft>0f && ! (moveRight>0f)) direction.x = -Mathf.Pow(moveLeft,2f);
			else if( ! (moveLeft>0f) && moveRight>0f) direction.x = Mathf.Pow(moveRight,2f);
			else direction.x=0f;
			
			if(moveFront>0f && ! (moveBack>0f)) direction.z = Mathf.Pow(moveFront,2f);
			else if( ! (moveFront>0f) && moveBack>0f) direction.z = -Mathf.Pow(moveBack,2f);
			else direction.z=0f;
			if(direction!= Vector3.zero) lastRefresh=time;
		}
		direction.y = 0f;
		controler.Move (transform.rotation*direction*(Time.deltaTime*speed));

	}
}
