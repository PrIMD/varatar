﻿using UnityEngine;
using System.Collections;

public class T_Inclination : MethodToText {

	#region implemented abstract members of MethodToText
	public override float GetFloatValue ()
	{
		return skeleton.GetSkeletonInclination ();
	}
	public override bool GetBoolValue ()
	{
		throw new System.NotImplementedException ();
	}
	#endregion

	void Start () {
		floatValue = true;
	}

}
