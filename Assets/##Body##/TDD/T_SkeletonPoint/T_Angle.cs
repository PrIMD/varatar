﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v2;
public class T_Angle : MethodToText {
	
	public Skeleton.Point pt1;
	public Skeleton.Point pivot;
	public Skeleton.Point pt2;

	#region implemented abstract members of MethodToText
	public override float GetFloatValue ()
	{
		return skeleton.GetAngle (pt1, pt2, pivot,new AxisUsed(){basedOn=this.basedOn,ignore_x_axe=ignoreX, ignore_y_axe=ignoreY,ignore_z_axe=ignoreZ});
	}
	public override bool GetBoolValue ()
	{
		throw new System.NotImplementedException ();
	}
	#endregion

	// Use this for initialization
	void Start () {
		floatValue = true;
	}

}
