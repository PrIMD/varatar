﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v2;
public class T_Direction : MethodToText {
	#region implemented abstract members of MethodToText
	public override float GetFloatValue ()
	{
		throw new System.NotImplementedException ();
	}
	public override bool GetBoolValue ()
	{
		throw new System.NotImplementedException ();
	}
	#endregion

	// Use this for initialization
	void Start () {
	
	}
	 void Update () {
		if (textView != null && skeleton!= null) {
			
			Vector3 dir = skeleton.GetSkeletonDirection(ignoreX,ignoreY,ignoreZ,true);
			lastValue=""+dir;
			textView.text=""+dir;

		}
	}

}
