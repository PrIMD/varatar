﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v2;
public abstract class MethodToText : MonoBehaviour {

	public string lastValue;

	public string purpuse="";
	public TextMesh textView;
	public SkeletonAccess skeleton;


	public bool floatValue;
	public bool boolValue;
	public AxisUsed.BasedOn basedOn ;
	public bool ignoreX;
	public bool ignoreY;
	public bool ignoreZ;

	void Update () {
		if (textView != null && skeleton!= null) {

			if(floatValue){
				float value = GetFloatValue();
				textView.text =purpuse+": "+ value ;
				lastValue=""+value;
			}
			else if(boolValue){
				bool value= GetBoolValue();
				textView.text =purpuse+": "+value;
				lastValue=""+value;
			}

		}
	}
	
	public abstract float GetFloatValue();
	public abstract bool GetBoolValue();
}
