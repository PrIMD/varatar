﻿using UnityEngine;
using System.Collections.Generic;
using System;
namespace be.primd.toolkit.bodytracker.v2{
public class DefaultBody : Body
	{
		public string keyword="Player 1";
		public SkeletonAccess skeletonAccess;
		public SkeletonVelocity skeletonVelocity;
		public BodyFeet feet;

		public override string GetKeyword(){
			return keyword;		
		}

	public override bool IsPointTracked (Skeleton.Point point)
	{IsSkeletonOk ();return  skeletonAccess.IsPointExist (point);}

	public override bool IsBoneTracked (Skeleton.Bone bone)
	{IsSkeletonOk ();return  skeletonAccess.IsBoneExist (bone);}

	public override TrackedPoint Get (Skeleton.Point point)
		{IsSkeletonOk ();return  skeletonAccess.Get (point);}

	public override TrackedBone Get (Skeleton.Bone bone)
		{IsSkeletonOk ();return  skeletonAccess.Get (bone);}

		public override TrackedPoint[] Get (Skeleton.Point [] point)
		{IsSkeletonOk ();return  skeletonAccess.Get (point);}

		public override TrackedBone[] Get (Skeleton.Bone [] bone)
		{IsSkeletonOk ();return  skeletonAccess.Get (bone);}

	public override Transform GetGround ()
		{IsSkeletonOk ();return  skeletonAccess.GetGround ();}

	public override Transform GetStartRoot ()
		{IsSkeletonOk ();return  skeletonAccess.GetStartRoot ();}

	public override float GetDistance (Skeleton.Bone bone)
		{IsSkeletonOk ();return  skeletonAccess.GetDistance (bone);}

	public override float GetDistance (Skeleton.Bone bone, AxisUsed axis)
		{IsSkeletonOk ();return  skeletonAccess.GetDistance (bone, axis);}

		public override float GetDistance (Skeleton.Bone [] bones)
		{IsSkeletonOk ();return  skeletonAccess.GetDistance (bones);}

		public override float GetDistance (Skeleton.Bone [] bones, AxisUsed axis)
		{IsSkeletonOk ();return  skeletonAccess.GetDistance (bones, axis);}

	public override float GetDistance (Skeleton.Point firstPoint, Skeleton.Point secondPoint)
		{IsSkeletonOk ();return  skeletonAccess.GetDistance (firstPoint, secondPoint);}

	public override float GetDistance (Skeleton.Point firstPoint, Skeleton.Point secondPoint, AxisUsed axis)
		{IsSkeletonOk ();return  skeletonAccess.GetDistance (firstPoint, secondPoint, axis);}

	public override float GetAngle (Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot)
		{IsSkeletonOk ();return  skeletonAccess.GetAngle (firstPoint, secondPoint, pivot);}

	public override float GetAngle (Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot, AxisUsed axis)
		{IsSkeletonOk ();return  skeletonAccess.GetAngle (firstPoint, secondPoint, pivot, axis);	}

	public override Vector3 GetDirection (Skeleton.Point from, Skeleton.Point to)
		{IsSkeletonOk ();return  skeletonAccess.GetDirection (from, to);}

	public override Vector3 GetDirection (Skeleton.Point from, Skeleton.Point to, AxisUsed axis)
		{IsSkeletonOk ();return  skeletonAccess.GetDirection (from, to, axis);}

	public override bool IsPointAtRightOf (Skeleton.Point point, Skeleton.Point of)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtRightOf (point,of);}

	public override bool IsPointAtLeftOf (Skeleton.Point point, Skeleton.Point of)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtLeftOf (point,of);}

	public override bool IsPointAtTopOf (Skeleton.Point point, Skeleton.Point of)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtTopOf (point,of);}

	public override bool IsPointAtBotOf (Skeleton.Point point, Skeleton.Point of)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtBotOf (point,of);}

	public override bool IsPointAtFrontOf (Skeleton.Point point, Skeleton.Point of)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtFrontOf (point,of);}

	public override bool IsPointAtBackOf (Skeleton.Point point, Skeleton.Point of)
		{IsSkeletonOk ();return  skeletonAccess.IsPointAtRightOf (point,of);}









	public override bool IsPointAtRightOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtRightOf (point,of,axis);}

	public override bool IsPointAtLeftOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtLeftOf (point,of,axis);}

	public override bool IsPointAtTopOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtTopOf (point,of,axis);}

	public override bool IsPointAtBotOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtBotOf (point,of,axis);}

	public override bool IsPointAtFrontOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtFrontOf (point,of,axis);	}

	public override bool IsPointAtBackOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{IsSkeletonOk ();return  skeletonAccess.IsPointAtBackOf (point,of,axis);}

	public override Quaternion GetNaturalRotation (Skeleton.Point from)
	{IsSkeletonOk ();return  skeletonAccess.GetNaturalRotation (from);}

	public override Vector3 GetNaturalDirection (Skeleton.Point from)
	{IsSkeletonOk ();return  skeletonAccess.GetNaturalDirection (from);}

	public override Vector3 GetSkeletonDirection ()
	{IsSkeletonOk ();return  skeletonAccess.GetSkeletonDirection ();}

	public override Vector3 GetSkeletonDirection (bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint)
	{IsSkeletonOk ();return  skeletonAccess.GetSkeletonDirection (ignoreX,ignoreY,ignoreZ,basedOnStartPoint);}

	public override Quaternion GetSkeletonRotation ()
	{IsSkeletonOk ();return  skeletonAccess.GetSkeletonRotation ();}

	public override Quaternion GetSkeletonRotation (bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint)
	{IsSkeletonOk ();return  skeletonAccess.GetSkeletonRotation (ignoreX,ignoreY,ignoreZ,basedOnStartPoint);}

	public override float GetSkeletonInclination ()
	{IsSkeletonOk ();return  skeletonAccess.GetSkeletonInclination();}

		public override Skeleton.Point [] GetPoints ()
	{IsSkeletonOk ();return  skeletonAccess.GetPoints ();}

	public override Skeleton.Bone [] GetBones ()
	{IsSkeletonOk ();return  skeletonAccess.GetBones ();}

	public override TrackedPoint[] GetTrackedPoints ()
	{IsSkeletonOk ();return  skeletonAccess.GetTrackedPoints ();}

	public override TrackedBone[] GetTrackedBones ()
	{IsSkeletonOk ();return skeletonAccess.GetTrackedBones ();}

	public override bool IsInitialise ()
	{
			return skeletonAccess != null && skeletonVelocity != null
				&& skeletonAccess.IsInitialise () && skeletonVelocity.IsInitialise ();
	}

	public override float GetSpeed (Skeleton.Point point)
	{IsVelocityOk ();return skeletonVelocity.GetSpeed (point);}

	public override Vector3 GetMovingDirection (Skeleton.Point point)
	{IsVelocityOk ();return skeletonVelocity.GetDirection (point);}

	public override float GetAcceleration (Skeleton.Point point)
	{IsVelocityOk ();return skeletonVelocity.GetAcceleration(point);}

	
	public override float GetSpeed (Skeleton.Point point, Skeleton.Point relativeTo)
	{IsVelocityOk ();return skeletonVelocity.GetSpeed(point,relativeTo);}

	public override Vector3 GetMovingDirection (Skeleton.Point point, Skeleton.Point relativeTo)
	{IsVelocityOk ();return skeletonVelocity.GetDirection(point,relativeTo);}

	public override float GetAcceleration (Skeleton.Point point, Skeleton.Point relativeTo)
	{IsVelocityOk ();return skeletonVelocity.GetAcceleration(point,relativeTo);}

		private void IsSkeletonOk ()
		{
			if (skeletonAccess  == null || (skeletonAccess != null && !skeletonAccess.IsInitialise ()))
				throw new IsNotInitialiseException ();
		}
		
		private void IsVelocityOk ()
		{
			if (skeletonVelocity == null || (skeletonVelocity != null && !skeletonVelocity.IsInitialise ()))
				throw new IsNotInitialiseException ();
		}
	}
	public class IsNotInitialiseException: Exception
	{
				public IsNotInitialiseException ():base(){}
				public IsNotInitialiseException (string str) : base( str){}
	}
	


}
