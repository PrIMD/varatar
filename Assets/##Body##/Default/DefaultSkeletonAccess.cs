﻿using UnityEngine;
using System.Collections.Generic;
using System;	
using be.primd.toolkit.bodytracker.v2;

public class DefaultSkeletonAccess : SkeletonAccess {

		public Transform startRoot;
		public Transform ground;
	
		public Transform hipCenter;
		public Transform spine;
		public Transform shoulderCenter;
		public Transform head;
		public Transform shoulderLeft;
		public Transform elbowLeft;
		public Transform wristLeft;
		public Transform handLeft;
		public Transform shoulderRight;
		public Transform elbowRight;
		public Transform wristRight;
		public Transform handRight;
		public Transform hipLeft;
		public Transform kneeLeft;
		public Transform ankleLeft;
		public Transform footLeft;
		public Transform hipRight;
		public Transform kneeRight;
		public Transform ankleRight;
		public Transform footRight;

		private Dictionary <Skeleton.Point  , TrackedPoint> points = new Dictionary<Skeleton.Point,TrackedPoint> ();
		private Dictionary <Skeleton.Bone   , TrackedBone> bones = new Dictionary<Skeleton.Bone,TrackedBone> ();
	
		private bool isInitialise;


	/**Return the localisation of the skeleton point*/
	public override TrackedPoint Get (Skeleton.Point point)
	{
			TrackedPoint tp=null;
			try{
				points.TryGetValue(point, out tp); 

			} catch (Exception){tp = null;}
			return tp;
	}
	
	/**Return the points localisations of the bone*/
	public override TrackedBone Get (Skeleton.Bone bone)
	{
			TrackedBone tb=null;
			try{
				bones.TryGetValue(bone, out tb); 
				
			} catch (Exception){tb = null;}
			return tb;
	}
	/**Does the skeleton point is usable*/
	public override bool IsPointExist (Skeleton.Point point)
	{
			TrackedPoint tp = Get (point);
			if (tp == null)
				return false;
			return true;
	}
	
	/**Does the skeleton bone is usable*/
	public override bool IsBoneExist (Skeleton.Bone bone)
	{ 	
			TrackedBone tb = Get (bone);
			if (tb == null)
				return false;
			return true;
	}

	public override Skeleton.Point[] GetPoints ()
	{
		Skeleton.Point[] pts=new Skeleton.Point[points.Keys.Count];
		points.Keys.CopyTo (pts, 0);
		return pts;
	}

	public override Skeleton.Bone[] GetBones ()
	{
		Skeleton.Bone [] bs=new Skeleton.Bone [bones.Keys.Count];
		bones.Keys.CopyTo (bs, 0);
		return bs;
	}

	
	public override TrackedPoint[] Get (Skeleton.Point[] pts)
	{
		TrackedPoint [] tp = new TrackedPoint[pts.Length];
		for (int i =0; i<pts.Length; i++) {
			tp[i]= Get (pts[i]);		
		}
		return tp;
	}
	public override TrackedBone[] Get (Skeleton.Bone[] _bones){
		TrackedBone [] tb = new TrackedBone[_bones.Length];
		for (int i =0; i<_bones.Length; i++) {
			tb[i]= Get (_bones[i]);		
		}
		return tb;
	}
	public override TrackedPoint[] GetTrackedPoints ()
	{
		TrackedPoint[] pts=new TrackedPoint[points.Values.Count];
		points.Values.CopyTo (pts, 0);
		return pts;
	}
	public override TrackedBone[] GetTrackedBones (){
		TrackedBone [] bs=new TrackedBone [bones.Values.Count];
		bones.Values.CopyTo (bs, 0);
		return bs;
	}
	


	/**Return the ground position and rotation of the ground*/
	public override Transform GetGround ()
	{return ground;}
	
	/**Return the ground position and rotation of the start emplacement*/
	public override Transform GetStartRoot ()
	{return startRoot;}

		/**Return the distance of skeleton bone*/
		public override float GetDistance (Skeleton.Bone bone, AxisUsed axis)
		{
				TrackedBone tb = Get (bone);
				if (tb == null) throw new SkeletonBoneNotfoundException ();
				return GetDistance (tb.firstPoint , tb.secondPoint,axis );
		}

	
		/**Return the distance of skeleton bones*/
			public override float GetDistance (Skeleton.Bone [] bones, AxisUsed axis)
		{
				float distance = 0f;
				foreach (Skeleton.Bone bone in bones) {
					distance+= GetDistance(bone,axis);
				}
				return distance;
		}


		public override float GetDistance (Skeleton.Point firstPoint, Skeleton.Point secondPoint, AxisUsed axis)
		{
			return GetDistance (Get (firstPoint), Get (secondPoint), axis);
		}
		public  float GetDistance (TrackedPoint firstPoint, TrackedPoint secondPoint, AxisUsed axis)
		{
			if (firstPoint == null)
				throw new SkeletonPointNotfoundException ();
			if (secondPoint == null)
				throw new SkeletonPointNotfoundException ();
			Vector3 f = firstPoint.transform.position;
			Vector3 s = secondPoint.transform.position;

			if (axis.basedOn == AxisUsed.BasedOn.StartRoot) {
					f = Relocated (f, startRoot.position, startRoot.rotation);
					s = Relocated (s, startRoot.position, startRoot.rotation);
			} else if (axis.basedOn == AxisUsed.BasedOn.Skeleton) {
					f = Relocated (f, f, GetSkeletonRotation());
					s = Relocated (s, f, GetSkeletonRotation());
			}
			f = Ignore (f, axis);
			s = Ignore (s, axis);
			
			return Vector3.Distance (f,s);

		}
	public override float GetAngle (Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot)
	{return GetAngle (firstPoint, secondPoint, pivot, new AxisUsed (){basedOn= AxisUsed.BasedOn.World});}
		public override float GetAngle (Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot, AxisUsed axis)
		{return GetAngle (Get (firstPoint), Get (secondPoint), Get (pivot), axis);}

		public  float GetAngle (TrackedPoint firstPoint, TrackedPoint secondPoint, TrackedPoint pivot, AxisUsed axis)
		{
			if (firstPoint == null)
				throw new SkeletonPointNotfoundException ();
			if (secondPoint == null)
				throw new SkeletonPointNotfoundException ();
			if (pivot == null)
				throw new SkeletonPointNotfoundException ();
			Vector3 a = firstPoint.transform.position;
			Vector3 b = secondPoint.transform.position;
			Vector3 p = pivot.transform.position;
			
			if (axis.basedOn == AxisUsed.BasedOn.StartRoot) {
				a = Relocated (a, startRoot.position, startRoot.rotation);
				b = Relocated (b, startRoot.position, startRoot.rotation);
				p = Relocated (p, startRoot.position, startRoot.rotation);
			} else if (axis.basedOn == AxisUsed.BasedOn.Skeleton) {
				a = Relocated (a, p, GetSkeletonRotation());
				b = Relocated (b, p, GetSkeletonRotation());
				p = Relocated (p, p, GetSkeletonRotation());
			}
			a = Ignore (a, axis);
			b = Ignore (b, axis);
			p = Ignore (p, axis);
			
			return GetAngleBetween (a, p, b);
		}

	
	public override Quaternion GetSkeletonRotation(){
		return GetQuadDirection (GetSkeletonDirection (false, true, false,true));
	}
	public override Quaternion GetSkeletonRotation(bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint){
		return GetQuadDirection (GetSkeletonDirection (ignoreX, ignoreY, ignoreZ,basedOnStartPoint));
	}

	public override Vector3 GetSkeletonDirection ()
	{return GetSkeletonDirection (false, true, false,true);}




	public override Vector3 GetSkeletonDirection (bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint)
	{
		
				TrackedPoint leftShoulder = Get (Skeleton.Point.ShoulderLeft);
				TrackedPoint rightShoulder = Get (Skeleton.Point.ShoulderRight);
				TrackedPoint spine = Get (Skeleton.Point.Spine);

				if (leftShoulder == null|| rightShoulder==null || spine==null)
					throw new SkeletonPointNotfoundException ();

				Vector3 vLeftShoulder = leftShoulder.transform.position;
				Vector3 vRightShoulder = rightShoulder.transform.position;
				Vector3 vSpine = spine.transform.position;
				
				if (basedOnStartPoint) {
						vLeftShoulder = Relocated (vLeftShoulder, startRoot.position, startRoot.rotation);
						vRightShoulder = Relocated (vRightShoulder, startRoot.position, startRoot.rotation);
						vSpine = Relocated (vSpine, startRoot.position, startRoot.rotation);
				}
				Vector3 direction = GetDirection (vLeftShoulder, vRightShoulder, vSpine);
				direction = 	Ignore (direction, ignoreX, ignoreY,ignoreZ);
				return direction;
	}

	public override float GetSkeletonInclination ()
	{
		//GET THE POINT
		TrackedPoint hipCenter = Get (Skeleton.Point.CenterHips);
		TrackedPoint shoulderCenter = Get (Skeleton.Point.CenterShoulders);
		//CHECK VALIDITY
		if (hipCenter == null|| shoulderCenter==null )
			throw new SkeletonPointNotfoundException ();
		//RELOCATE IT COMPARE TO THE ground root
		Vector3 vHipCenter = Relocated(hipCenter.transform.position, startRoot.transform.position ,startRoot.transform.rotation);
		Vector3 vShoulderCenter = Relocated(shoulderCenter.transform.position, startRoot.transform.position,startRoot.transform.rotation);
		//CREATE A POINT UNDER HIP
		Vector3 vUnderHip = vHipCenter;
		vUnderHip.y -= 1f;
		//CALCULATE ANGLE OF INCLINATION
		return  GetAngleBetween (vUnderHip, vHipCenter, vShoulderCenter);
		
		
	}
	public override Quaternion GetNaturalRotation(Skeleton.Point from){
		return GetQuadDirection (Vector3.zero, GetNaturalDirection (from));
	}
	public override Vector3 GetNaturalDirection(Skeleton.Point from){
		if (from == Skeleton.Point.Spine)
			return GetSkeletonDirection (false,false,false, false);
		else if (from == Skeleton.Point.HandLeft)
			return GetDirection(Skeleton.Point.WristLeft ,Skeleton.Point.HandLeft);
		else if (from == Skeleton.Point.HandRight)
			return GetDirection(Skeleton.Point.WristRight ,Skeleton.Point.HandRight);
		else if (from == Skeleton.Point.FootLeft)
			return GetDirection(Skeleton.Point.AnkleLeft ,Skeleton.Point.FootLeft);
		else if (from == Skeleton.Point.FootRight)
			return GetDirection(Skeleton.Point.AnkleRight ,Skeleton.Point.FootRight);
		else if (from == Skeleton.Point.Head)
			return GetDirection(Skeleton.Point.CenterShoulders ,Skeleton.Point.Head);

		Skeleton.Point [] nexts = Skeleton.GetNextPoints (from);
		if (nexts.Length == 0)
				return Vector3.zero;
		else if (nexts.Length == 1)
				return GetDirection (from, nexts [0]);
		else {
			Vector3 averageDirection = GetDirection (from, nexts [0]);
			for(int i=1; i<nexts.Length;i++)
			{
				Vector3 direction = GetDirection (from, nexts [i]);
				averageDirection = (averageDirection+direction)/2f;
			}
			return averageDirection;
		}

	}
	
	public override Vector3 GetDirection(Skeleton.Point from, Skeleton.Point to){
		return GetDirection(from,to, new AxisUsed(){basedOn = AxisUsed.BasedOn.World});
	}
	public override Vector3 GetDirection(Skeleton.Point from, Skeleton.Point to, AxisUsed axis){

		if (from == to)return Vector3.zero;

		TrackedPoint trFrom = Get(from);
		TrackedPoint trTo = Get(to);
		if (trFrom == null || trTo == null)
			throw new SkeletonPointNotfoundException ();
		Vector3 vFrom = trFrom.transform.position;
		Vector3 vTo = trTo.transform.position;

		if (axis.basedOn == AxisUsed.BasedOn.StartRoot) {
			vFrom = 	Relocated (vFrom, startRoot.position, startRoot.rotation);
			vTo = 		Relocated (vTo, startRoot.position, startRoot.rotation);
		} else if (axis.basedOn == AxisUsed.BasedOn.Skeleton) {
			vFrom = 	Relocated (vFrom, vFrom, GetSkeletonRotation());
			vTo = 		Relocated (vTo, vFrom, GetSkeletonRotation());
		}

		Vector3 direction = vTo-vFrom;
		direction = Ignore (direction, axis);
		return direction;
		
	}
	public override float GetDistance (Skeleton.Bone bone)
	{
		return GetDistance (bone, new AxisUsed (){basedOn = AxisUsed.BasedOn.World});
	}

	public override float GetDistance (Skeleton.Bone[] bones)
	{
		return GetDistance (bones, new AxisUsed (){basedOn = AxisUsed.BasedOn.World});
	}

	public override float GetDistance (Skeleton.Point firstPoint, Skeleton.Point secondPoint)
	{
		return GetDistance (firstPoint, secondPoint, new AxisUsed (){basedOn = AxisUsed.BasedOn.World});
	}


	public override bool IsPointAtRightOf (Skeleton.Point point, Skeleton.Point of)
	{
		return IsPointAtRightOf (point, of, new AxisUsed (){basedOn=AxisUsed.BasedOn.Skeleton});
	}
	
	public override bool IsPointAtLeftOf (Skeleton.Point point, Skeleton.Point of)
	{
		return IsPointAtLeftOf (point, of, new AxisUsed (){basedOn=AxisUsed.BasedOn.Skeleton});
	}
	
	public override bool IsPointAtTopOf (Skeleton.Point point, Skeleton.Point of)
	{
		return IsPointAtTopOf (point, of, new AxisUsed (){basedOn=AxisUsed.BasedOn.Skeleton});
	}
	
	public override bool IsPointAtBotOf (Skeleton.Point point, Skeleton.Point of)
	{
		return IsPointAtBotOf (point, of, new AxisUsed (){basedOn=AxisUsed.BasedOn.Skeleton});
	}
	
	public override bool IsPointAtFrontOf (Skeleton.Point point, Skeleton.Point of)
	{
		return IsPointAtFrontOf (point, of, new AxisUsed (){basedOn=AxisUsed.BasedOn.Skeleton});
	}
	
	public override bool IsPointAtBackOf (Skeleton.Point point, Skeleton.Point of)
	{
		return IsPointAtBackOf (point, of, new AxisUsed (){basedOn=AxisUsed.BasedOn.Skeleton});
	}	



	public override bool IsPointAtRightOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{
		
		Vector3 direction = GetDirection (of,point,axis);
		return direction.x > 0;
	}

	public override bool IsPointAtLeftOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{
		
		Vector3 direction = GetDirection (of,point,axis);
		return direction.x < 0;
	}

	public override bool IsPointAtTopOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{
		
		Vector3 direction = GetDirection (of,point,axis);
		return direction.y > 0;
	}

	public override bool IsPointAtBotOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{
		
		Vector3 direction = GetDirection (of,point,axis);
		return direction.y< 0;
	}

	public override bool IsPointAtFrontOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{
		
		Vector3 direction = GetDirection (of,point,axis);
		return direction.z > 0;
	}

	public override bool IsPointAtBackOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis)
	{
		Vector3 direction = GetDirection (of,point,axis);
		return direction.z < 0;
	}	
	
	
	public override bool IsInitialise ()
	{
			return isInitialise;
	}



		public void Awake(){
			InitFromInspectorToDictonary ();		
		}
		public void InitFromInspectorToDictonary()
		{
			points.Clear ();
			bones.Clear ();

			TrackedPoint hipCenterTracked=null;
			TrackedPoint spineTracked=null;
			TrackedPoint shoulderCenterTracked=null;
			TrackedPoint headTracked=null;
			TrackedPoint shoulderLeftTracked=null;
			TrackedPoint elbowLeftTracked=null;
			TrackedPoint wristLeftTracked=null;
			TrackedPoint handLeftTracked=null;
			TrackedPoint shoulderRightTracked=null;
			TrackedPoint elbowRightTracked=null;
			TrackedPoint wristRightTracked=null;
			TrackedPoint handRightTracked=null;
			TrackedPoint hipLeftTracked=null;
			TrackedPoint kneeLeftTracked=null;
			TrackedPoint ankleLeftTracked=null;
			TrackedPoint footLeftTracked=null;
			TrackedPoint hipRightTracked=null;
			TrackedPoint kneeRightTracked=null;
			TrackedPoint ankleRightTracked=null;
			TrackedPoint footRightTracked=null;

			
			//########## POINT ############
			if( hipCenter!=null){
				hipCenterTracked = new TrackedPoint(Skeleton.Point.CenterHips, hipCenter);
				points.Add(Skeleton.Point.CenterHips,hipCenterTracked);}
			if( spine!=null){
				spineTracked = new TrackedPoint(Skeleton.Point.Spine, spine);
				points.Add(Skeleton.Point.Spine,spineTracked);}
			if( shoulderCenter!=null){
				shoulderCenterTracked=new TrackedPoint(Skeleton.Point.CenterShoulders, shoulderCenter);
				points.Add(Skeleton.Point.CenterShoulders,shoulderCenterTracked);}
			if( head !=null){
				headTracked =new TrackedPoint(Skeleton.Point.Head, head);
				points.Add(Skeleton.Point.Head,headTracked);}
			if( shoulderLeft!=null){
				shoulderLeftTracked=new TrackedPoint(Skeleton.Point.ShoulderLeft, shoulderLeft);
				points.Add(Skeleton.Point.ShoulderLeft,shoulderLeftTracked);}
			if( elbowLeft!=null){
				elbowLeftTracked= new TrackedPoint(Skeleton.Point.ElbowLeft, elbowLeft);
				points.Add(Skeleton.Point.ElbowLeft,elbowLeftTracked);}
			if( wristLeft !=null){
				wristLeftTracked=new TrackedPoint(Skeleton.Point.WristLeft, wristLeft);
				points.Add(Skeleton.Point.WristLeft,wristLeftTracked);}
			if( handLeft!=null){
				handLeftTracked=new TrackedPoint(Skeleton.Point.HandLeft, handLeft);
				points.Add(Skeleton.Point.HandLeft,handLeftTracked);}
			if( shoulderRight!=null){
				shoulderRightTracked=new TrackedPoint(Skeleton.Point.ShoulderRight, shoulderRight);
				points.Add(Skeleton.Point.ShoulderRight,shoulderRightTracked);}
			if( elbowRight!=null){
				elbowRightTracked=new TrackedPoint(Skeleton.Point.ElbowRight, elbowRight);
				points.Add(Skeleton.Point.ElbowRight,elbowRightTracked);}  
			if( wristRight!=null){
				wristRightTracked=new TrackedPoint(Skeleton.Point.WristRight, wristRight);
				points.Add(Skeleton.Point.WristRight,wristRightTracked);}  
			if( handRight!=null){
				handRightTracked=new TrackedPoint(Skeleton.Point.HandRight, handRight);
				points.Add(Skeleton.Point.HandRight,handRightTracked);}  
			if( hipLeft!=null){
				hipLeftTracked=new TrackedPoint(Skeleton.Point.HipLeft, hipLeft);
				points.Add(Skeleton.Point.HipLeft,hipLeftTracked);}  
			if( kneeLeft!=null){
				kneeLeftTracked=new TrackedPoint(Skeleton.Point.KneeLeft, kneeLeft);
				points.Add(Skeleton.Point.KneeLeft,kneeLeftTracked);}  
			if( ankleLeft!=null){
				ankleLeftTracked=new TrackedPoint(Skeleton.Point.AnkleLeft, ankleLeft);
				points.Add(Skeleton.Point.AnkleLeft,ankleLeftTracked);}  
			if( footLeft!=null){
				footLeftTracked=new TrackedPoint(Skeleton.Point.FootLeft, footLeft);
				points.Add(Skeleton.Point.FootLeft,footLeftTracked );}  
			if( hipRight!=null){
				hipRightTracked=new TrackedPoint(Skeleton.Point.HipRight, hipRight);
				points.Add(Skeleton.Point.HipRight,hipRightTracked );}  
			if( kneeRight!=null){
				kneeRightTracked=new TrackedPoint(Skeleton.Point.KneeRight, kneeRight);
				points.Add(Skeleton.Point.KneeRight,kneeRightTracked);}  
			if( ankleRight!=null){
				ankleRightTracked=new TrackedPoint(Skeleton.Point.AnkleRight, ankleRight);
				points.Add(Skeleton.Point.AnkleRight,ankleRightTracked);}  
			if( footRight!=null){
				footRightTracked=new TrackedPoint(Skeleton.Point.FootRight, footRight);
				points.Add(Skeleton.Point.FootRight,footRightTracked );}

			//########## BONE ############
			//
			if (shoulderLeftTracked != null && elbowLeftTracked != null) {
				bones.Add(Skeleton.Bone.ArmLeft,
				          new TrackedBone(Skeleton.Bone.ArmLeft, shoulderLeftTracked,elbowLeftTracked));
			} 
			if (elbowLeftTracked != null && wristLeftTracked != null) {
				bones.Add(Skeleton.Bone.ForearmLeft,
				          new TrackedBone(Skeleton.Bone.ForearmLeft, elbowLeftTracked,wristLeftTracked));
			} 
			if (wristLeftTracked != null && handLeftTracked != null) {
				bones.Add(Skeleton.Bone.HandLeft,
				          new TrackedBone(Skeleton.Bone.HandLeft, wristLeftTracked,handLeftTracked));
			} 
			if (hipCenterTracked != null && hipLeftTracked != null) {
				bones.Add(Skeleton.Bone.HipLeft,
				          new TrackedBone(Skeleton.Bone.HipLeft, hipCenterTracked,hipLeftTracked));
			}
			if (hipLeftTracked != null && kneeLeftTracked != null) {
				bones.Add(Skeleton.Bone.ThighLeft,
				          new TrackedBone(Skeleton.Bone.ThighLeft, hipLeftTracked,kneeLeftTracked));
			} 
			if (kneeLeftTracked != null && ankleLeftTracked != null) {
				bones.Add(Skeleton.Bone.LegLeft,
				          new TrackedBone(Skeleton.Bone.LegLeft, kneeLeftTracked,ankleLeftTracked));
			} 
			if (ankleLeftTracked != null && footLeftTracked != null) {
				bones.Add(Skeleton.Bone.FootLeft,
				          new TrackedBone(Skeleton.Bone.FootLeft, ankleLeftTracked, footLeftTracked));
			} 
			//

			if (shoulderRightTracked != null && elbowRightTracked != null) {
				bones.Add(Skeleton.Bone.ArmRight,
				          new TrackedBone(Skeleton.Bone.ArmRight, shoulderRightTracked,elbowRightTracked));
			} 
			if (elbowRightTracked != null && wristRightTracked != null) {
				bones.Add(Skeleton.Bone.ForearmRight,
				          new TrackedBone(Skeleton.Bone.ForearmRight, elbowRightTracked,wristRightTracked));
			} 
			if (wristRightTracked != null && handRightTracked != null) {
				bones.Add(Skeleton.Bone.HandRight,
				          new TrackedBone(Skeleton.Bone.HandRight, wristRightTracked,handRightTracked));
			} 
			if (hipCenterTracked != null && hipRightTracked != null) {
				bones.Add(Skeleton.Bone.HipRight,
				          new TrackedBone(Skeleton.Bone.HipRight, hipCenterTracked,hipRightTracked));
			}
			if (hipRightTracked != null && kneeRightTracked != null) {
				bones.Add(Skeleton.Bone.ThighRight,
				          new TrackedBone(Skeleton.Bone.ThighRight, hipRightTracked,kneeRightTracked));
			} 
			if (kneeRightTracked != null && ankleRightTracked != null) {
				bones.Add(Skeleton.Bone.LegRight,
				          new TrackedBone(Skeleton.Bone.LegRight, kneeRightTracked,ankleRightTracked));
			} 
			if (ankleRightTracked != null && footRightTracked != null) {
				bones.Add(Skeleton.Bone.FootRight,
				          new TrackedBone(Skeleton.Bone.FootLeft, ankleLeftTracked, footLeftTracked));
			} 

			//

			if (shoulderCenterTracked != null && spineTracked != null) {
				bones.Add(Skeleton.Bone.TopSpine,
				          new TrackedBone(Skeleton.Bone.TopSpine, shoulderCenterTracked,spineTracked));
			} 
			if (spineTracked != null && hipCenterTracked != null) {
				bones.Add(Skeleton.Bone.BotSpine,
				          new TrackedBone(Skeleton.Bone.BotSpine,spineTracked,hipCenterTracked));
			} 
			if (spineTracked != null && shoulderLeftTracked != null) {
				bones.Add(Skeleton.Bone.ChestLeft,
				          new TrackedBone(Skeleton.Bone.ChestLeft,spineTracked,shoulderLeftTracked));
			}
			if (spineTracked != null && shoulderRightTracked != null) {
				bones.Add(Skeleton.Bone.ChestRight,
				          new TrackedBone(Skeleton.Bone.ChestRight,spineTracked,shoulderRightTracked));
			}
			//
			if (shoulderCenterTracked != null && headTracked != null) {
				bones.Add(Skeleton.Bone.Neck,
				          new TrackedBone(Skeleton.Bone.Neck,shoulderCenterTracked,headTracked));
			} 
			if (shoulderLeftTracked != null && shoulderRightTracked != null) {
				bones.Add(Skeleton.Bone.Shoulder,
				          new TrackedBone(Skeleton.Bone.Shoulder,shoulderLeftTracked,shoulderRightTracked));
			}
			isInitialise = true;
		}



		public static Vector3 GetDirection(Vector3 left, Vector3 right, Vector3 thirdEdge) {
			Vector3 direction = Vector3.zero;
			Vector3 triCenter = (left + right + thirdEdge * 2f) / 4f;
			
			Vector3 vect1 = left-triCenter;
			Vector3 vect2 = right-triCenter;
			direction = Vector3.Cross( vect2,vect1).normalized;
			
			return direction;
		}
		
	public static Quaternion GetQuadDirection(Vector3 originePoint, Vector3 directionalPoint) {
		Vector3 direction = (directionalPoint - originePoint).normalized;
		if (direction == Vector3.zero)
			return new Quaternion ();
		
		return 		Quaternion.LookRotation( direction, Vector3.forward);
		
	}
	
	public static Quaternion GetQuadDirection( Vector3 directionalPoint) {
		return GetQuadDirection (Vector3.zero, directionalPoint);
	}
		

		
		public static Vector3 Relocated(Vector3 point,Vector3 rootPosition ,Quaternion rootRotation, bool withIgnoreX=false, bool withIgnoreY=false, bool withIgnoreZ=false){
			Vector3 p = point- rootPosition;
			if (rootRotation.eulerAngles != Vector3.zero) {
				Quaternion rootRotationInverse = Quaternion.Inverse (rootRotation);
				p = RotateAroundPoint (p, Vector3.zero, rootRotationInverse);
			}
			if (withIgnoreX)
				p.x = 0;
			if (withIgnoreY)
				p.y = 0;
			if (withIgnoreZ)
				p.z = 0;
			return p;
		}
		public static Vector3 Ignore(Vector3 vector , AxisUsed axis){
			
			if (axis.ignore_x_axe)
				vector.x = 0;
			if (axis.ignore_y_axe)
				vector.y = 0;
			if (axis.ignore_z_axe)
				vector.z = 0;
			return vector;
		}
	public static Vector3 Ignore(Vector3 vector , bool ignoreX, bool ignoreY, bool ignoreZ){
			
			if (ignoreX)
				vector.x = 0;
			if (ignoreY)
				vector.y = 0;
			if (ignoreZ)
				vector.z = 0;
			return vector;
		}
		public static Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
			return angle * ( point - pivot) + pivot;
		}
		public static float GetAngleBetween(Vector3 first, Vector3 pivotCenter , Vector3 second)
		{
			Vector3 ptOne = first - pivotCenter;
			Vector3 ptTwo = second - pivotCenter;
			return Vector3.Angle (ptOne, ptTwo);
		}


		

	}

