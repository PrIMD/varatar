﻿using UnityEngine;
using System.Collections;

namespace be.primd.toolkit.bodytracker.v2{
public class DefaultFeet : BodyFeet {

		public Feet feet;

		public override bool IsStandingOnLeftFoot ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsStandingOnRightFoot ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsStandingOnFeet ()
	{
		throw new System.NotImplementedException ();
	}

	public override bool IsJumping ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetGroundFeetDistance ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetBetweenFeetDistance ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetLeftFootHeight ()
	{
		throw new System.NotImplementedException ();
	}

	public override float GetRightFootHeight ()
	{
		throw new System.NotImplementedException ();
	}

	public override Side GetHigherFoot ()
	{
		throw new System.NotImplementedException ();
	}
}
}