﻿using UnityEngine;
using System.Collections.Generic;
using System;
using be.primd.toolkit.bodytracker.v2;

public class DefaultSkeletonVelocity : SkeletonVelocity {

	public SkeletonAccess skeleton;
	private Dictionary<Skeleton.Point, TrackedVelocityData> velocityPoints = new Dictionary<Skeleton.Point, TrackedVelocityData>();
	private List <TrackedVelocityData> velocityPointsList = new List<TrackedVelocityData> ();

	public float refresh=0.1f;
	public float lastRefresh;
	public bool initialise;

	public override bool IsInitialise ()
	{	return initialise;}



		public void Update(){
		
		if (skeleton == null || ! skeleton.IsInitialise ())
						return;
				if (!initialise) {
				TrackedPoint [] skeletonPoints = skeleton.GetTrackedPoints ();
				if(skeletonPoints==null)
				{Debug.LogWarning("Skeleton is initialised but does not have tracked points !",this.gameObject);return;}
				velocityPoints.Clear();velocityPointsList.Clear();

				foreach(TrackedPoint points in skeletonPoints)
				{
					if(points!=null)
					{
						TrackedVelocityData vd = new TrackedVelocityData(points);
						velocityPoints.Add(points.type, vd);
						velocityPointsList.Add(vd);
					}

				}
				initialise=true;
				} 
					float time = GetTime ();
					if (time - lastRefresh > refresh) {
						lastRefresh=time;
						foreach(TrackedVelocityData vd in velocityPointsList)
						{
							vd.Refresh(time);
						}

					}
			
		}
	public float GetTime (){return Time.realtimeSinceStartup;}


	private TrackedVelocityData GetVelocity(Skeleton.Point point){

		try{
			TrackedVelocityData vd = null;
			velocityPoints.TryGetValue(point,out vd);
			return vd;
		}catch(Exception){
			return null;	
		}
	}


	
	public override float GetSpeed (Skeleton.Point point, Skeleton.Point relativeTo)
	{
		return GetSpeed (point, relativeTo, false);
	}
	private float GetSpeed (Skeleton.Point point, Skeleton.Point relativeTo,bool previous)
	{
		TrackedVelocityData vdPoint = GetVelocity (point);
		TrackedVelocityData vdRelative = GetVelocity (relativeTo);
		if (vdPoint == null || vdRelative == null)
			throw new SkeletonPointNotfoundException ("The velocity of a point is not tracked");

		if(previous)return Vector3.Distance(Vector3.zero,(vdPoint.Previous.Direction - vdRelative.Previous.Direction))/((vdPoint.Previous.Duration+vdRelative.Previous.Duration)/2f);
		return Vector3.Distance(Vector3.zero,(vdPoint.Direction - vdRelative.Direction))/((vdPoint.Duration+vdRelative.Duration)/2f);
	}
	
	public override Vector3 GetDirection (Skeleton.Point point, Skeleton.Point relativeTo)
	{
		TrackedVelocityData vdPoint = GetVelocity (point);
		TrackedVelocityData vdRelative = GetVelocity (relativeTo);
		if (vdPoint == null || vdRelative == null)
			throw new SkeletonPointNotfoundException ("The velocity of a point is not tracked");
		return vdPoint.Direction - vdRelative.Direction;
	}
	
	public override float GetAcceleration (Skeleton.Point point, Skeleton.Point relativeTo)
	{
		TrackedVelocityData vdPoint = GetVelocity (point);
		if (vdPoint == null )
			throw new SkeletonPointNotfoundException ("The velocity of a point is not tracked");

		float speedNow = GetSpeed (point, relativeTo, false);
		float speedBefore = GetSpeed (point, relativeTo, true);

		return (speedNow-speedBefore)/ (vdPoint.When-vdPoint.Previous.When);
	}
	


	public override float GetSpeed (Skeleton.Point point)
	{
		TrackedVelocityData vd = GetVelocity (point);
		if (vd == null)
						throw new SkeletonPointNotfoundException ("The velocity of this point is not tracked");
		return vd.Speed ;
	}

	public override Vector3 GetDirection (Skeleton.Point point)
	{
		
		TrackedVelocityData vd = GetVelocity (point);
		if (vd == null)
			throw new SkeletonPointNotfoundException ("The velocity of this point is not tracked");
		return vd.Direction ;
	}

	public override float GetAcceleration (Skeleton.Point point)
	{
		
		TrackedVelocityData vd = GetVelocity (point);
		if (vd == null)
			throw new SkeletonPointNotfoundException ("The velocity of this point is not tracked");
		return vd.Acceleration ;
	}



	public class TrackedVelocityData
	{
		public readonly TrackedPoint trackedPoint;
		private float when;
		private Vector3 lastPosition;

		private float speed;
		private float acceleration;
		private Vector3 direction;
		private float duration;
		
		private OldData previous;

		public TrackedVelocityData (TrackedPoint trackedPoint)
		{
			this.trackedPoint = trackedPoint;
		}

		public void Refresh(float time){
			SetNewPosition (trackedPoint.transform.position, time);	
		}

		public void SetNewPosition(Vector3 position,float time){

			float timeBetween = time - when;
			Vector3 direction = position - lastPosition;
			float speed = Vector3.Distance (position, lastPosition) / timeBetween;
			float acceleration = (speed - this.speed)/timeBetween;
			SetData (time, position, speed, acceleration, direction,timeBetween);

		}
		
		public void SetData(float time,Vector3 position, float speed, float acceleration, Vector3 direction, float timeBetween){
			previous.SaveAs (this.when,this.lastPosition,this.speed,this.acceleration,this.direction,this.duration);
			this.when = time;
			this.lastPosition = position;
			this.speed = speed;
			this.acceleration = acceleration;
			this.direction = direction;
			this.duration = timeBetween;
		}
		public TrackedPoint TrackedPoint {
			get {
				return this.trackedPoint;
			}

		}

		public struct OldData
		{
			private float when;
			private Vector3 lastPosition;
			
			private float speed;
			private float acceleration;
			private Vector3 direction;
			private float duration;

			public void SaveAs(float time,Vector3 position, float speed, float acceleration, Vector3 direction, float timeBetween){
				this.when = time;
				this.lastPosition = position;
				this.speed = speed;
				this.acceleration = acceleration;
				this.direction = direction;
				this.duration = timeBetween;
			}

			public float When {
				get {
					return this.when;
				}
				set {
					when = value;
				}
			}

			public Vector3 LastPosition {
				get {
					return this.lastPosition;
				}
				set {
					lastPosition = value;
				}
			}


			public float Speed {
				get {
					return this.speed;
				}
				set {
					speed = value;
				}
			}

			public float Acceleration {
				get {
					return this.acceleration;
				}
				set {
					acceleration = value;
				}
			}

			public Vector3 Direction {
				get {
					return this.direction;
				}
				set {
					direction = value;
				}
			}

			public float Duration {
				get {
					return this.duration;
				}
				set {
					duration = value;
				}
			}
		}

		public float When {
			get {
				return this.when;
			}
			set {
				when = value;
			}
		}
		public OldData Previous {
			get {
				return this.previous;
			}
			set {
				previous = value;
			}
		}
		public float Duration {
			get {
				return this.duration;
			}
			set {
				duration = value;
			}
		}
		public Vector3 LastPosition {
			get {
				return this.lastPosition;
			}
			set {
				lastPosition = value;
			}
		}

		public float Speed {
			get {
				return this.speed;
			}
			set {
				speed = value;
			}
		}

		public float Acceleration {
			get {
				return this.acceleration;
			}
			set {
				acceleration = value;
			}
		}

		public Vector3 Direction {
			get {
				return this.direction;
			}
			set {
				direction = value;
			}
		}
	}
}

