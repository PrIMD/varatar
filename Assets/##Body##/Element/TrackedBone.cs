﻿using UnityEngine;
using System;

namespace be.primd.toolkit.bodytracker.v2{
public class TrackedBone
{
	
	public readonly Skeleton.Bone type;
	public readonly TrackedPoint firstPoint;
	public readonly TrackedPoint secondPoint;
	


	public TrackedBone( Skeleton.Bone boneType ,TrackedPoint firstPoint, TrackedPoint secondPoint)
	{
			if (firstPoint == null || secondPoint==null )
				throw new NullReferenceException ("The point given at the construction of a BodyPoint can't be null");
			this.type = boneType;
		this.firstPoint = firstPoint;
		this.secondPoint = secondPoint;
	}
		public float GetDistance(){return Vector3.Distance(firstPoint.transform.position, secondPoint.transform.position);}
//		public Vector3 GetDirection(bool fromFirstToSecond=true)
//		{
//			if (fromFirstToSecond)
//				return second.transform.position-first.transform.position;
//			else
//				return first.transform.position-second.transform.position;
//		}
		public override string ToString ()
	{
			return string.Format ("[Bone={0}> {1} , {2}]", type, firstPoint.type, secondPoint.type);
	}
	
	}
}
