﻿using UnityEngine;
using System;

namespace be.primd.toolkit.bodytracker.v2{
public class TrackedPoint
{
	public readonly Skeleton.Point type;
	public readonly Transform transform;
	public readonly GameObject gameObject;
	
	
	public TrackedPoint( Skeleton.Point pointType ,Transform point)
	{
		if (point == null)
			throw new NullReferenceException ("The point given at the construction of a BodyPoint can't be null");
			this.type = pointType;
		this.transform = point;
		this.gameObject = point.gameObject;
	}
	public TrackedPoint( Skeleton.Point pointType ,GameObject point)
	{
		if (point == null)
			throw new NullReferenceException ("The point given at the construction of a BodyPoint can't be null");
			this.type = pointType;
		this.transform = point.transform;
		this.gameObject = point;
	}
	public override string ToString ()
	{
			return string.Format ("[Point: {0} -> {1}]", type, transform.name);
	}
	
}
}