﻿using UnityEngine;
using System.Collections;
using System;

namespace be.primd.toolkit.bodytracker.v2{
public class Skeleton
{

		public enum Point
		{
			Head,CenterShoulders, Spine, CenterHips,
			ShoulderLeft,ShoulderRight,
			ElbowLeft,ElbowRight,
			WristLeft,WristRight,
			HandLeft,HandRight,
			HipLeft,HipRight,
			KneeLeft,KneeRight,
			AnkleLeft,AnkleRight,
			FootLeft,FootRight
		}
		public enum Bone
		{
			Neck,
			Shoulder,
			
			TopSpine,BotSpine,
			ChestLeft,ChestRight,
			ArmLeft,ArmRight,
			
			ForearmLeft,ForearmRight,
			HandLeft,HandRight, 
			
			HipLeft,HipRight,
			ThighLeft,ThighRight,
			LegLeft,LegRight,
			FootLeft,FootRight
		}
		public static Point [] GetPointsBasedOnBone(Bone bone)
		{
			switch(bone)
			{
			case Bone.ArmLeft: return new Point[]{Point.ShoulderLeft, Point.ElbowLeft};
			case Bone.ForearmLeft: return new Point[]{Point.ElbowLeft, Point.WristLeft};
			case Bone.HandLeft: return new Point[]{Point.WristLeft, Point.HandLeft};
				
				
			case Bone.HipLeft: return new Point[]{Point.CenterHips, Point.HipLeft};
			case Bone.ThighLeft: return new Point[]{Point.HipLeft, Point.KneeLeft};
			case Bone.LegLeft: return new Point[]{Point.KneeLeft, Point.AnkleLeft};
			case Bone.FootLeft: return new Point[]{Point.AnkleLeft, Point.FootLeft};
				
				
			case Bone.ArmRight: return new Point[]{Point.ShoulderRight, Point.ElbowRight};
			case Bone.ForearmRight: return new Point[]{Point.ElbowRight, Point.WristRight};
			case Bone.HandRight: return new Point[]{Point.WristRight, Point.HandRight};
				
				
			case Bone.HipRight: return new Point[]{Point.CenterHips, Point.HipRight};
			case Bone.ThighRight: return new Point[]{Point.HipRight, Point.KneeRight};
			case Bone.LegRight: return new Point[]{Point.KneeRight, Point.AnkleRight};
			case Bone.FootRight: return new Point[]{Point.AnkleRight, Point.FootLeft};
				
			case Bone.TopSpine: return new Point[]{Point.CenterShoulders, Point.Spine};
			case Bone.BotSpine: return new Point[]{Point.Spine, Point.CenterHips};
			case Bone.ChestLeft: return new Point[]{Point.Spine, Point.ShoulderLeft};
			case Bone.ChestRight: return new Point[]{Point.Spine, Point.ShoulderRight};
				
			case Bone.Neck : return new Point[]{Point.CenterShoulders, Point.Head};
			case Bone.Shoulder: return new Point[]{Point.ShoulderLeft, Point.ShoulderRight};
			}
			return null;
		}

		public static Bone GetBoneBasedOnPoints(Point firstPoint, Point secondPoint){
			
			if ((firstPoint == Point.ShoulderLeft && secondPoint == Point.ElbowLeft)
			    || (firstPoint == Point.ElbowLeft && secondPoint == Point.ShoulderLeft))
				return Bone.ArmLeft;
			if ((firstPoint == Point.ElbowLeft && secondPoint == Point.WristLeft)
			    || (firstPoint == Point.WristLeft && secondPoint == Point.ElbowLeft))
				return Bone.ForearmLeft;
			if ((firstPoint == Point.WristLeft && secondPoint == Point.HandLeft)
			    || (firstPoint == Point.HandLeft && secondPoint == Point.WristLeft))
				return Bone.HandLeft;

				
			if ((firstPoint == Point.CenterHips && secondPoint == Point.HipLeft)
			    || (firstPoint == Point.HipLeft && secondPoint == Point.CenterHips))
				return Bone.HipLeft;
			if ((firstPoint == Point.HipLeft && secondPoint == Point.KneeLeft)
			    || (firstPoint == Point.KneeLeft && secondPoint == Point.HipLeft))
				return Bone.ThighLeft;
			if ((firstPoint == Point.KneeLeft && secondPoint == Point.AnkleLeft)
			    || (firstPoint == Point.AnkleLeft && secondPoint == Point.KneeLeft))
				return Bone.LegLeft;
			if ((firstPoint == Point.AnkleLeft && secondPoint == Point.FootLeft)
			    || (firstPoint == Point.FootLeft && secondPoint == Point.AnkleLeft))
				return Bone.FootLeft;
			
			
			if ((firstPoint == Point.ShoulderRight && secondPoint == Point.ElbowRight)
			    || (firstPoint == Point.ElbowRight && secondPoint == Point.ShoulderRight))
				return Bone.ArmRight;
			if ((firstPoint == Point.ElbowRight && secondPoint == Point.WristRight)
			    || (firstPoint == Point.WristRight && secondPoint == Point.ElbowRight))
				return Bone.ForearmRight;
			if ((firstPoint == Point.WristRight && secondPoint == Point.HandRight)
			    || (firstPoint == Point.HandRight && secondPoint == Point.WristRight))
				return Bone.HandRight;
			
			
			if ((firstPoint == Point.CenterHips && secondPoint == Point.HipRight)
			    || (firstPoint == Point.HipRight && secondPoint == Point.CenterHips))
				return Bone.HipRight;
			if ((firstPoint == Point.HipRight && secondPoint == Point.KneeRight)
			    || (firstPoint == Point.KneeRight && secondPoint == Point.HipRight))
				return Bone.ThighRight;
			if ((firstPoint == Point.KneeRight && secondPoint == Point.AnkleRight)
			    || (firstPoint == Point.AnkleRight && secondPoint == Point.KneeRight))
				return Bone.LegRight;
			if ((firstPoint == Point.AnkleRight && secondPoint == Point.FootRight)
			    || (firstPoint == Point.FootRight && secondPoint == Point.AnkleRight))
				return Bone.FootRight;


			if ((firstPoint == Point.CenterShoulders && secondPoint == Point.Spine)
			    || (firstPoint == Point.Spine && secondPoint == Point.CenterShoulders))
				return Bone.TopSpine;
			if ((firstPoint == Point.Spine && secondPoint == Point.CenterHips)
			    || (firstPoint == Point.CenterHips && secondPoint == Point.Spine))
				return Bone.BotSpine;
			if ((firstPoint == Point.Spine && secondPoint == Point.ShoulderLeft)
			    || (firstPoint == Point.ShoulderLeft && secondPoint == Point.Spine))
				return Bone.ChestLeft;
			if ((firstPoint == Point.Spine && secondPoint == Point.ShoulderRight)
			    || (firstPoint == Point.ShoulderRight && secondPoint == Point.Spine))
				return Bone.ChestRight;

			if ((firstPoint == Point.CenterShoulders && secondPoint == Point.Head)
			    || (firstPoint == Point.Head && secondPoint == Point.CenterShoulders))
				return Bone.Neck;

			if ((firstPoint == Point.ShoulderLeft && secondPoint == Point.ShoulderRight)
			    || (firstPoint == Point.ShoulderRight && secondPoint == Point.ShoulderLeft))
				return Bone.Shoulder;

			throw new SkeletonBoneNotfoundException ("You try to found a bone that is not define by the two points you gave.");
		}

		
		public static Point [] GetNextPoints(Point point){
			switch (point) {
				
			case Point.Spine: return new Point []{Point.CenterShoulders, Point.ShoulderLeft,Point.ShoulderRight, Point.CenterHips};
			case Point.CenterHips: return new Point []{Point.HipLeft, Point.HipRight};
				
			case Point.HipLeft: return new Point []{Point.KneeLeft};
			case Point.KneeLeft: return new Point []{Point.AnkleLeft};
			case Point.AnkleLeft: return new Point []{Point.FootLeft};
				
			case Point.HipRight: return new Point []{Point.KneeRight};
			case Point.KneeRight: return new Point []{Point.AnkleRight};
			case Point.AnkleRight: return new Point []{Point.FootRight};
				
			case Point.ShoulderLeft: return new Point []{Point.ElbowLeft};
			case Point.ElbowLeft: return new Point []{Point.WristLeft};
			case Point.WristLeft: return new Point []{Point.HandLeft};
				
			case Point.ShoulderRight: return new Point []{Point.ElbowRight};
			case Point.ElbowRight: return new Point []{Point.WristRight};
			case Point.WristRight: return new Point []{Point.HandRight};
				
			case Point.CenterShoulders: return new Point []{Point.Head};
				
			case Point.HandLeft: return new Point []{Point.HandLeft};
			case Point.HandRight: return new Point []{Point.HandRight};
				
			case Point.FootLeft: return new Point []{Point.FootLeft};
			case Point.FootRight: return new Point []{Point.FootRight};
				
			case Point.Head: return new Point []{Point.Head};
				
			}
			return new Point []{Point.Spine};
		}
		
		public static Point GetPreviousPoint(Point point){
			switch (point) {
				
			case Point.Spine: return Point.Spine;
			case Point.CenterHips: return Point.Spine;
				
			case Point.HipLeft: return Point.CenterHips;
			case Point.KneeLeft:  return Point.HipLeft;
			case Point.AnkleLeft:  return Point.KneeLeft;
			case Point.FootLeft:  return Point.AnkleLeft;
				
			case Point.HipRight:  return Point.CenterHips;
			case Point.KneeRight:  return Point.HipRight;
			case Point.AnkleRight:  return Point.KneeRight;
			case Point.FootRight:  return Point.AnkleRight;
				
			case Point.ShoulderLeft: return Point.Spine;
			case Point.ElbowLeft: return Point.ShoulderLeft;
			case Point.WristLeft: return Point.ElbowLeft;
			case Point.HandLeft: return Point.WristLeft;
				
			case Point.ShoulderRight: return Point.Spine;
			case Point.ElbowRight: return Point.ShoulderRight;
			case Point.WristRight: return Point.ElbowRight;
			case Point.HandRight: return Point.WristRight;
				
			case Point.Head: return Point.CenterShoulders;
			case Point.CenterShoulders:  return Point.Spine;
				
			}
			return point;
		}
		


	}
}