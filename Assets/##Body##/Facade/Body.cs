﻿using UnityEngine;
using System.Collections;
using System;
namespace be.primd.toolkit.bodytracker.v2{
	public abstract class Body : MonoBehaviour, IBody {


		public virtual void Awake()
		{
			BodyAccess.Add (this);
		}

		public abstract string GetKeyword();

		public abstract  bool IsPointTracked (Skeleton.Point point);
		public abstract  bool IsBoneTracked (Skeleton.Bone bone);
		public abstract  TrackedPoint Get (Skeleton.Point point);
		public abstract  TrackedBone Get (Skeleton.Bone bone);
		public abstract  TrackedPoint[] Get (Skeleton.Point[] point);
		public abstract  TrackedBone[] Get (Skeleton.Bone[] bone);
		public abstract  Transform GetGround ();
		public abstract  Transform GetStartRoot ();
		public abstract  float GetDistance (Skeleton.Bone bone);
		public abstract  float GetDistance (Skeleton.Bone bone, AxisUsed axis);
		public abstract  float GetDistance (Skeleton.Bone[] bones);
		public abstract  float GetDistance (Skeleton.Bone[] bones, AxisUsed axis);
		public abstract  float GetDistance (Skeleton.Point firstPoint, Skeleton.Point secondPoint);
		public abstract  float GetDistance (Skeleton.Point firstPoint, Skeleton.Point secondPoint, AxisUsed axis);
		public abstract  float GetAngle (Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot);
		public abstract  float GetAngle (Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot, AxisUsed axis);
		public abstract  Vector3 GetDirection (Skeleton.Point from, Skeleton.Point to);
		public abstract  Vector3 GetDirection (Skeleton.Point from, Skeleton.Point to, AxisUsed axis);
		public abstract  bool IsPointAtRightOf (Skeleton.Point point, Skeleton.Point of);
		public abstract  bool IsPointAtLeftOf (Skeleton.Point point, Skeleton.Point of);
		public abstract  bool IsPointAtTopOf (Skeleton.Point point, Skeleton.Point of);
		public abstract  bool IsPointAtBotOf (Skeleton.Point point, Skeleton.Point of);
		public abstract  bool IsPointAtFrontOf (Skeleton.Point point, Skeleton.Point of);
		public abstract  bool IsPointAtBackOf (Skeleton.Point point, Skeleton.Point of);
		public abstract  bool IsPointAtRightOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis);
		public abstract  bool IsPointAtLeftOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis);
		public abstract  bool IsPointAtTopOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis);
		public abstract  bool IsPointAtBotOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis);
		public abstract  bool IsPointAtFrontOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis);
		public abstract  bool IsPointAtBackOf (Skeleton.Point point, Skeleton.Point of, AxisUsed axis);
		public abstract  Quaternion GetNaturalRotation (Skeleton.Point from);
		public abstract  Vector3 GetNaturalDirection (Skeleton.Point from);
		public abstract  Vector3 GetSkeletonDirection ();
		public abstract  Vector3 GetSkeletonDirection (bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint);
		public abstract  Quaternion GetSkeletonRotation ();
		public abstract  Quaternion GetSkeletonRotation (bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint);
		public abstract  float GetSkeletonInclination ();
		public abstract  Skeleton.Point[] GetPoints ();
		public abstract  Skeleton.Bone[] GetBones ();
		public abstract  TrackedPoint[] GetTrackedPoints ();
		public abstract  TrackedBone[] GetTrackedBones ();
		public abstract  bool IsInitialise ();
		public abstract  float GetSpeed (Skeleton.Point point);
		public abstract  Vector3 GetMovingDirection (Skeleton.Point point);
		public abstract  float GetAcceleration (Skeleton.Point point);
		public abstract  float GetSpeed (Skeleton.Point point, Skeleton.Point relativeTo);
		public abstract  Vector3 GetMovingDirection (Skeleton.Point point, Skeleton.Point relativeTo);
		public abstract  float GetAcceleration (Skeleton.Point point, Skeleton.Point relativeTo);
	}

}
