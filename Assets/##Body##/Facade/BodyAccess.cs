﻿using UnityEngine;
using System.Collections.Generic;
using System;
namespace be.primd.toolkit.bodytracker.v2{
	public class BodyAccess  {

		private static readonly string defaultMainKeyword="Player 1";
		private static readonly Dictionary<string, Body> bodies = new Dictionary<string, Body>();

		public static Body Get( )
		{
			return Get (defaultMainKeyword);
		}
		public static Body Get(string keyword )
		{
			if (string.IsNullOrEmpty (keyword))
				return null;

			Body body =null;
			if (bodies.ContainsKey (keyword)) {
				try
				{
					bodies.TryGetValue(keyword,out body);
				}catch(Exception){return null;}
			}
			return body;
		}

		public static void Add (Body body)
		{
			if (body == null) {
				Debug.LogWarning ("Body not added: body null");
				return ;			
			}
			string keyword = body.GetKeyword ();
			if (string.IsNullOrEmpty(keyword)) {
				Debug.LogWarning ("Body not added:The keyword to access to the body is kind of null");
				return ;			
			}
			if ( bodies.ContainsKey(keyword) ) {
				Debug.LogWarning ("Body not added: there is already a body with the keyword "+keyword);
				return ;			
			}
			bodies.Add (keyword, body);
		}
	}
}
