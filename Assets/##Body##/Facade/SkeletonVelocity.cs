﻿using UnityEngine;
using System.Collections;

namespace be.primd.toolkit.bodytracker.v2{
public abstract class SkeletonVelocity : MonoBehaviour, ISkeletonVelocity {
		#region ISkeletonVelocity implementation

		public abstract float GetSpeed (Skeleton.Point point);
		public abstract Vector3 GetDirection (Skeleton.Point point);
		public abstract float GetAcceleration (Skeleton.Point point);
		public abstract float GetSpeed (Skeleton.Point point, Skeleton.Point relativeTo);
		public abstract Vector3 GetDirection (Skeleton.Point point, Skeleton.Point relativeTo);
		public abstract float GetAcceleration (Skeleton.Point point, Skeleton.Point relativeTo);
		public abstract bool IsInitialise();
		#endregion





	
	}
}
