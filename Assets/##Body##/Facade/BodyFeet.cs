using UnityEngine;
using System.Collections;

namespace be.primd.toolkit.bodytracker.v2{
	public abstract class  BodyFeet : MonoBehaviour,IFeet {

		public abstract bool IsStandingOnLeftFoot ();
		public abstract bool IsStandingOnRightFoot ();
		public abstract bool IsStandingOnFeet ();
		public abstract bool IsJumping ();
		public abstract float GetGroundFeetDistance ();
		public abstract float GetBetweenFeetDistance ();
		public abstract float GetLeftFootHeight ();
		public abstract float GetRightFootHeight ();
		public abstract Side GetHigherFoot ();
			
	}
}