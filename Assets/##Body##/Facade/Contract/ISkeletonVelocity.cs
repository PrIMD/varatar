using System;
using UnityEngine;

namespace be.primd.toolkit.bodytracker.v2{
	public interface ISkeletonVelocity {

		float GetSpeed (Skeleton.Point point);
		Vector3 GetDirection (Skeleton.Point point);
		float GetAcceleration (Skeleton.Point point);
		
		float GetSpeed (Skeleton.Point point, Skeleton.Point relativeTo);
		Vector3 GetDirection (Skeleton.Point point, Skeleton.Point relativeTo);
		float GetAcceleration (Skeleton.Point point, Skeleton.Point relativeTo);
     }
}