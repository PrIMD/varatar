
namespace be.primd.toolkit.bodytracker.v2{
	public interface IFeet {

		bool IsStandingOnLeftFoot();
		bool IsStandingOnRightFoot();
		bool IsStandingOnFeet();
		bool IsJumping();
		
		float GetGroundFeetDistance();
		float GetBetweenFeetDistance();
		float GetLeftFootHeight();
		float GetRightFootHeight();
		Side GetHigherFoot();
    
	}
}