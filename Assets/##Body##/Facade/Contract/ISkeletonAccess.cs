using System;
using UnityEngine;

namespace be.primd.toolkit.bodytracker.v2{
public interface ISkeletonAccess {

		bool 	IsPointExist(Skeleton.Point point);
		bool 	IsBoneExist(Skeleton.Bone bone);
		TrackedPoint 	Get(Skeleton.Point point);
		TrackedBone 	Get(Skeleton.Bone bone);
		
		TrackedPoint [] 	Get(Skeleton.Point [] point);
		TrackedBone  []  	Get(Skeleton.Bone [] bone);

		Transform		GetGround();
		Transform		GetStartRoot();

		float 			GetDistance(Skeleton.Bone bone );
		float 			GetDistance(Skeleton.Bone bone, AxisUsed axis );
		float 			GetDistance(Skeleton.Bone [] bones );
		float 			GetDistance(Skeleton.Bone [] bones, AxisUsed axis  );
		float 			GetDistance(Skeleton.Point firstPoint, Skeleton.Point secondPoint);
		float 			GetDistance(Skeleton.Point firstPoint, Skeleton.Point secondPoint, AxisUsed axis );
		float 			GetAngle(Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot );
		float 			GetAngle(Skeleton.Point firstPoint, Skeleton.Point secondPoint, Skeleton.Point pivot , AxisUsed axis );

		Vector3 GetDirection(Skeleton.Point from, Skeleton.Point to);
		Vector3 GetDirection(Skeleton.Point from, Skeleton.Point to, AxisUsed axis);

		bool IsPointAtRightOf(Skeleton.Point point ,Skeleton.Point of);
		bool IsPointAtLeftOf(Skeleton.Point point ,Skeleton.Point of);
		bool IsPointAtTopOf(Skeleton.Point point ,Skeleton.Point of);
		bool IsPointAtBotOf(Skeleton.Point point ,Skeleton.Point of);
		bool IsPointAtFrontOf(Skeleton.Point point ,Skeleton.Point of);
		bool IsPointAtBackOf(Skeleton.Point point ,Skeleton.Point of );

		bool IsPointAtRightOf(Skeleton.Point point ,Skeleton.Point of , AxisUsed axis);
		bool IsPointAtLeftOf(Skeleton.Point point ,Skeleton.Point of , AxisUsed axis);
		bool IsPointAtTopOf(Skeleton.Point point ,Skeleton.Point of , AxisUsed axis);
		bool IsPointAtBotOf(Skeleton.Point point ,Skeleton.Point of , AxisUsed axis);
		bool IsPointAtFrontOf(Skeleton.Point point ,Skeleton.Point of , AxisUsed axis);
		bool IsPointAtBackOf(Skeleton.Point point ,Skeleton.Point of , AxisUsed axis);

		Quaternion GetNaturalRotation (Skeleton.Point from);
		Vector3 GetNaturalDirection (Skeleton.Point from);

		Vector3			GetSkeletonDirection();
		Vector3			GetSkeletonDirection(bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint);
		
		Quaternion GetSkeletonRotation();
		Quaternion GetSkeletonRotation(bool ignoreX, bool ignoreY, bool ignoreZ, bool basedOnStartPoint);

		float			GetSkeletonInclination();

		
		Skeleton.Point [] GetPoints();
		Skeleton.Bone [] GetBones();
		
		TrackedPoint[] GetTrackedPoints();
		TrackedBone [] GetTrackedBones();
		bool 			IsInitialise();

	}
	public struct AxisUsed
	{
		public enum BasedOn {World, StartRoot, Skeleton}
		public BasedOn basedOn;
		public bool ignore_x_axe;
		public bool ignore_y_axe;
		public bool ignore_z_axe;
	}
	public class SkeletonBoneNotfoundException : Exception
	{
		public SkeletonBoneNotfoundException():base(){}
		public SkeletonBoneNotfoundException(string msg):base(msg){}
	}
	public class SkeletonPointNotfoundException : Exception
	{
		public SkeletonPointNotfoundException():base(){}
		public SkeletonPointNotfoundException(string msg):base(msg){}
	}
}
