﻿using UnityEngine;
using System.Collections;
using be.primd.toolkit.bodytracker.v2;
public class T_PointVelocity  :MonoBehaviour{
	
	public Skeleton.Point point;
	public bool withRelativity=true;
	public Skeleton.Point relativeTo;
	public DefaultSkeletonVelocity velocitySkeleton;
	public TextMesh textZone;

	public void Update()
	{
		if (textZone == null || velocitySkeleton == null || ! velocitySkeleton.IsInitialise ())
						return ;
		if(withRelativity)
			textZone.text = "" + velocitySkeleton.GetDirection(point,relativeTo) + " - s > " + velocitySkeleton.GetSpeed (point,relativeTo)+ " - a > " + velocitySkeleton.GetAcceleration (point,relativeTo);
		else
			textZone.text = "" + velocitySkeleton.GetDirection(point) + " - s > " + velocitySkeleton.GetSpeed (point)+ " - a > " + velocitySkeleton.GetAcceleration (point);


	}
}
