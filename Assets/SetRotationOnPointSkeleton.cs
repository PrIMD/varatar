﻿using UnityEngine;
using System.Collections;

using be.primd.toolkit.bodytracker.v2;
public class SetRotationOnPointSkeleton : MonoBehaviour {

	public SkeletonAccess skeleton;
	private TrackedPoint [] trackedPoints;

	public float refresh  = 0.05f;
	public float lastTime ; 


	public void Update()
	{
		if (skeleton == null || ! skeleton.IsInitialise())
			return;
		if (trackedPoints == null) {
			trackedPoints = skeleton.GetTrackedPoints();
				
		} else {
			float time = Time.realtimeSinceStartup;
			if (time - lastTime > refresh) {
				lastTime=time;

				for(int i =0; i<trackedPoints.Length;i++)
				{
					if(trackedPoints[i]!=null)
					{
						Quaternion rot = skeleton.GetNaturalRotation(trackedPoints[i].type);
						trackedPoints[i].transform.rotation=rot;
					}
				}
				
			}
		}




	}

}
